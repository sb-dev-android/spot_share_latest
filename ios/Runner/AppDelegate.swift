import UIKit
import Flutter
import GoogleMaps
import Firebase
import UserNotifications

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    GMSServices.provideAPIKey("AIzaSyCodjRNiGQ3GaUNKoEBCY1V1NCnmof82Aw")
    // GMSServices.provideAPIKey("AIzaSyA0QlNOrMY6JU7wqgBXBamQq1v9wbR11Z0")
    GeneratedPluginRegistrant.register(with: self)
    
    //application.registerForRemoteNotifications()
    //self.registerForPushNotifications()
    
    //FirebaseApp.configure()
    
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
  
}
