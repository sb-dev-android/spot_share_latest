import 'package:flushbar/flushbar_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:peer_to_peer/designModelClass/podoclasses/common.dart';
import 'package:peer_to_peer/stylesheet.dart';
import 'package:peer_to_peer/Location.dart';
import 'package:peer_to_peer/locationAfterParking.dart';
import 'package:peer_to_peer/swapRequest.dart';
import 'package:peer_to_peer/savedLocation.dart';
import 'package:peer_to_peer/history.dart';
import 'package:peer_to_peer/Login.dart';
import 'package:peer_to_peer/setting.dart';
import 'package:peer_to_peer/profileScreen.dart';
import 'package:peer_to_peer/designModelClass/podoclasses/helper.dart';
import 'package:peer_to_peer/successfull_match.dart';
import 'designModelClass/podoclasses/internet_connectivity.dart';
import 'package:flushbar/flushbar.dart';
import 'dart:io' as io;
import 'package:location/location.dart' as loc;
import 'designModelClass/customAppbar.dart' as apbar;
import 'package:peer_to_peer/designModelClass/preventMultiTap.dart';
import 'designModelClass/podoclasses/httpservices.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'designModelClass/podoclasses/models.dart';

class Home extends StatelessWidget {

  Home(this.status,{Key key, this.analytics, this.observer})
      : super(key: key);

  final FirebaseAnalytics analytics;
  final FirebaseAnalyticsObserver observer;
  final bool status;

  @override
  Widget build(BuildContext context) {

    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarIconBrightness:Brightness.dark,
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        //systemNavigationBarDividerColor: Colors.black
      ),
    );

    return  MaterialApp(
      initialRoute: '/',
      routes: <String, WidgetBuilder> {
        '/login': (BuildContext context) => new Login(),
        '/home': (BuildContext context) => new Home(status),
      },
      debugShowCheckedModeBanner: false,
      home:Scaffold(
        body: DrawerAnimation(status),
      ),
    );
  }
}



backgroundHandler(BuildContext context, {Map<String, dynamic> message}) async{

  Firebasemodel fm = Firebasemodel.fromJson(message);
  Flushbar(
    titleText: Text(fm.title,style: notificationTitleTextstyle,),
    messageText:  Text(fm.message,style: notificationMessageTextstyle,),
    icon: Icon(Icons.notifications_active,size: 25,),
    flushbarPosition: FlushbarPosition.TOP,
    flushbarStyle: FlushbarStyle.FLOATING,
    borderRadius: 10,
    backgroundColor: Colors.white,
    margin: EdgeInsets.all(10),
    duration: Duration(seconds: 2),
    boxShadows: [BoxShadow(color: Colors.black45,blurRadius: 2)],
  ).show(context);
}




class DrawerAnimation extends StatefulWidget {

  DrawerAnimation(this.status,{Key key, this.analytics, this.observer})
      : super(key: key);
  final FirebaseAnalytics analytics;
  final FirebaseAnalyticsObserver observer;
  final bool status;

  @override
  _DrawerAnimationState createState() => _DrawerAnimationState(analytics, observer);
}


class _DrawerAnimationState extends State<DrawerAnimation> {

  _DrawerAnimationState(this.analytics, this.observer);
  final FirebaseAnalyticsObserver observer;
  final FirebaseAnalytics analytics;

  Helper helper =  Helper();
  bool isCollapsed = true;
  double screenWidth;
  double screenHeight;
  Duration _duration = const Duration(milliseconds: 300);
  InternetConnectivity _internetConnectivity = InternetConnectivity();
  bool tapHandle = true;

 /* setLastMapLatLong() async{
    var lastMapLocation = await helper.getLastMapLocation();
  }*/



 //Todo: firebaseeeeeeeeeee
  FirebaseMessaging _firebaseMessaging;

  void setUpFirebase() {
    _firebaseMessaging = FirebaseMessaging();
    if (io.Platform.isIOS) iOS_Permission();
    firebaseCloudMessaging_Listeners(); }

  void firebaseCloudMessaging_Listeners()async {

    await _firebaseMessaging.getToken().then((token) async{
      await setFcmToken(fcmToken: token);
      print("firebase token : "+token);
    });

    await _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {

        print("On message firebae notification : BEFORE : $message");

        backgroundHandler(context,message: message);

        print("On message firebae notification : AFTER : $message");

      },

      onResume: (Map<String, dynamic> message) async {
        print('on resume : >>>>>>>>> $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch : >>>>>>>>> $message');
      },
     /* onBackgroundMessage: (Map<String, dynamic> message) async {
        backgroundHandler(context,message: message);
      },*/
    );
  }

  void iOS_Permission()async{
    await _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(  sound: true,  badge: true,  alert: true, provisional: false )
    );
    await _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }

  setFcmToken({String fcmToken}) async {
    await helper.setFcmToken(fcmToken: fcmToken);
  }


  @override
  void initState() {
    // TODO: remove below comment lines


    setUpFirebase();
    setUserInfoMethod();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    Size size = MediaQuery.of(context).size;
    screenHeight = size.height;
    screenWidth = size.width;

    return SafeArea(
      top: false,
      bottom: false,
      child: Stack(fit: StackFit.expand,
        children: <Widget>[
          menu(context),
          home(context),
        ],
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  setUserInfoMethod() async{
    var userInfoData = await helper.getUserInfo();
    setState(() {
      Common.profileCarUser = userInfoData["ProfileCarUser"];
      Common.profileCarColor = userInfoData["ProfileCarColor"];
      Common.profileCarMake = userInfoData["ProfileCarMake"];
      Common.profileCarModel = userInfoData["ProfileCarModel"];
      Common.profileCarNo = userInfoData["ProfileCarNo"];
      Common.profileEmail = userInfoData["ProfileEmail"];
    });
  }

  Widget menu(context){
    var listTilePadding = EdgeInsets.only(
        bottom: (MediaQuery.of(context).size.width * 0.1) - 30, right: 25);
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(
              top: MediaQuery.of(context).size.height * 0.1 + 20
          ),
          child: Container(
            padding: EdgeInsets.only(left: 15,right: 60),
            child: OnlyOnePointerRecognizerWidget(
              child: ListView(
                shrinkWrap: true,
                children: <Widget>[
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("Home",style: widget.status? activeSideMenuText: sideMenuText,),
                      trailing: Image(
                        image: AssetImage(
                          "assets/icn_home.png",
                        ),
                        color: Colors.blueAccent,

                      ),
                      onTap: () async{
                        var conInternetCheckBool = await _internetConnectivity.checkConnectivity();
                        var chkValidate = await validateAuth();
                        conInternetCheckBool
                            ? chkValidate
                            ? Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context)=>Home(widget.status)))
                            : Navigator.pushReplacementNamed(context, '/login')
                            :  _flushPopUp( message: Common.internetErrorMessage,
                            bgColor: Color.fromRGBO(178,0,0,1),
                            duration: Duration(milliseconds: 2500));
                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("Swap Request",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage(
                          "assets/icn_swap_request.png",
                        ),
                        color: Colors.blueAccent,

                      ),
                      onTap: () async{
                        var chkValidate = await validateAuth();
                        chkValidate
                            ? Navigator.of(context).push(MaterialPageRoute(
                            builder: (context)=>SwapRequest(widget.status)))
                            : Navigator.pushReplacementNamed(context, '/login',arguments: false);
                        setState(() {
                          isCollapsed = true;
                        });
                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("History",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage(
                          "assets/icn_history.png",
                        ),
                        color: Colors.blueAccent,

                      ),
                      onTap: () async{
                        var chkValidate = await validateAuth();
                        chkValidate
                            ? Navigator.of(context).push(MaterialPageRoute(
                            builder: (context)=>History(widget.status)
                        )
                        ): Navigator.pushReplacementNamed(context, '/login');
                        setState(() {
                          isCollapsed = true;
                        });
                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("Saved Location",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage(
                          "assets/icn_saved_location.png",
                        ),
                        color: Colors.blueAccent,
                      ),
                      onTap: () async{
                        var chkValidate = await validateAuth();
                        chkValidate
                            ? Navigator.of(context).push(MaterialPageRoute(
                            builder: (context)=>SavedLocation(widget.status)))
                            : Navigator.pushReplacementNamed(context, '/login');
                        setState(() {
                          isCollapsed = true;
                        });
                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("Settings",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage(
                          "assets/icn_settings.png",
                        ),
                        color: Colors.blueAccent,
                      ),
                      onTap: ()async{
                        var chkValidate = await validateAuth();
                        chkValidate
                            ? Navigator.of(context).push(MaterialPageRoute(
                            builder: (context)=>Setting(widget.status)))
                            : Navigator.pushReplacementNamed(context, '/login');
                        setState(() {
                          isCollapsed = true;
                        });
                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("About Us",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage(
                            "assets/icn_about_us.png"
                        ),
                        color: Colors.blueAccent,
                      ),
                      onTap: () async{
                        var chkValidate = await validateAuth();
                        chkValidate
                            ? null
                            : Navigator.pushReplacementNamed(context, '/login');
                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("Contact Us",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage(
                          "assets/icn_contact_us.png",
                        ),
                        color: Colors.blueAccent,
                      ),
                      onTap: () async{
                        var chkValidate = await validateAuth();
                        chkValidate
                            ? null
                            : Navigator.pushReplacementNamed(context, '/login');
                      },
                    ),
                  ),
                  Padding(
                      padding: listTilePadding,
                      child: AbsorbPointer(
                        absorbing: !tapHandle,
                        child: ListTile(
                          title: Text("Rate Us",style: sideMenuText,),
                          trailing: Image(
                            image: AssetImage(
                              "assets/icn_rate_us.png",
                            ),
                            color: Colors.blueAccent,
                          ),
                          onTap: () async{
                            //TODO: handle multiple tap
                            /*setState(() {
                        tapHandle = false;
                      });*/

                            var chkValidate = await validateAuth();
                            chkValidate
                                ? rateUs(context)
                                : Navigator.pushReplacementNamed(context, '/login');

                          },
                        ),
                      )
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("Sign Out",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage(
                          "assets/icn_signout.png",
                        ),
                        color: Colors.blueAccent,
                      ),
                      onTap: (){
                        Navigator.of(context).pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
                        //Navigator.pushNamedAndRemoveUntil(context,'/login', ModalRoute.withName('/home'));
                        helper.clearOauth();
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.only(left: 15,right: 80,),
          margin: EdgeInsets.only(top: 20),
          height: 80,
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border(
              bottom: BorderSide(
                color: Colors.black.withOpacity(0.5),
                width: 0.5,
              )
            )
          ),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment:  MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Hi ${Common.profileCarUser}",
                    style: textProfileNameStyle,
                  ),
                  IconButton(
                    icon: Icon(Icons.arrow_forward),
                    onPressed: (){
                      Navigator.push(context, MaterialPageRoute(
                          builder: (context)=>ProfileScreen()
                      ));
                    },
                  )
                ],
              ),
              Row(
                children: <Widget>[
                  Text(
                    "${Common.profileEmail}",
                    style: textProfileEmailStyle,
                  )
                ],
              )
            ],
          ),
        )
      ],
    );
  }

  Widget home(context){

    return AnimatedPositioned(
      top: isCollapsed ? 0: 0.0 * screenHeight,
      bottom: isCollapsed ? 0 : 0.0 * screenWidth,
      right: isCollapsed ? 0 : -4 * screenWidth,
      left: isCollapsed ? 0 : 0.8 * screenWidth,

      duration: _duration,

      child: Material(

        elevation: 8,

        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: apbar.AppBar(
            backgroundColor: Colors.white,
            centerTitle: true,
            elevation: 5,
            leading:IconButton(
              icon: Image(
                image: AssetImage("assets/icn_menu_home.png"),
              ),
              onPressed: (){
                setState(() {
                  if(isCollapsed){
                    setState(() {
                      isCollapsed =false;
                    });
                  }else{
                    setState(() {
                      isCollapsed=true;
                    });
                  }
                });
              },
            ),
            title: Text(
              "Home",
              style: appbarHeading,
            ),
          ),
          body: Container(
            child: Column(
              children: <Widget>[
                Expanded(
                  child: Stack(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top:50),
                        child: GestureDetector(
                          onTap:() async {
                            var location = loc.Location();
                            var _permissionGranted = await location.hasPermission();
                            if (_permissionGranted == loc.PermissionStatus.DENIED) {
                              _permissionGranted = await location.requestPermission();
                              if (_permissionGranted != loc.PermissionStatus.GRANTED) {
                                return;
                              }
                            }
                            navigateLookingParkingPage();
                          },
                          child: Container(
                            width:MediaQuery.of(context).size.width ,
                            height: MediaQuery.of(context).size.height * 0.5,
                            decoration: BoxDecoration(
                                border: Border(
                                    bottom: BorderSide(color: Colors.white,width: 2)
                                ),
                                image: DecorationImage(
                                    image: AssetImage("assets/Subtraction.png"),
                                    fit: BoxFit.fill
                                )
                            ),
                            child: Column(
                              children: <Widget>[
                                Container(
                                  height: (MediaQuery.of(context).size.height * 0.4),
                                  decoration: BoxDecoration(

                                      image: DecorationImage(
                                        image: AssetImage("assets/icn_looking_for_parking.png"),
                                        //fit: BoxFit.fill
                                      )
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          Padding(
                                            padding: EdgeInsets.only(
                                                bottom:
                                                (MediaQuery.of(context).size.height * 0.1) -30
                                            ),
                                            child: Text(
                                              "${Common.lookingParking}",
                                              style: TextStyle(
                                                  fontFamily: 'AdelleLight',
                                                  fontSize: 18,
                                                  color: Colors.blueAccent
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),

                      Align(
                        alignment: Alignment.bottomLeft,
                        child: GestureDetector(
                          onTap:() async{

                            var location = loc.Location();
                            var _permissionGranted = await location.hasPermission();
                            if (_permissionGranted == loc.PermissionStatus.DENIED) {
                              _permissionGranted = await location.requestPermission();
                              if (_permissionGranted != loc.PermissionStatus.GRANTED) {
                                return;
                              }
                            }

                            navigateLeavingParking();
                          },
                          child: Container(
                            width:MediaQuery.of(context).size.width,
                            height: MediaQuery.of(context).size.height * 0.4,
                            decoration: BoxDecoration(
                                color: Color.fromRGBO(28,129,247,1),
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(50),
                                  topRight: Radius.circular(50),
                                ),
                                image: DecorationImage(
                                  image: AssetImage(
                                      "assets/icn_leaving_my_parking.png"
                                  ),
                                  //fit: BoxFit.fill
                                )
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.only(
                                          bottom:
                                          (MediaQuery.of(context).size.height * 0.1) - 30
                                      ),
                                      child: Text(
                                        "${Common.leavingParking}",
                                        style: TextStyle(
                                            fontFamily: Common.fontAdelleLight,
                                            fontSize: 18,
                                            color: Colors.white
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  _flushPopUp({String message ,Color bgColor, Duration duration}){
    Flushbar(
      message:  "${message}",
      flushbarPosition: FlushbarPosition.TOP,
      flushbarStyle: FlushbarStyle.FLOATING,
      borderRadius: 10,
      backgroundColor: bgColor,
      margin: EdgeInsets.all(10),
      duration: duration,
    ).show(context);
  }

  validateAuth() async{
    var valRes = await helper.validateAuth();
    if(valRes['status'] == 1){
      return true;
    }else{
      return false;
    }
  }

  navigateLookingParkingPage() async{

    var cnChk = await _internetConnectivity.checkConnectivity();
    var chkValidate = await validateAuth();

    isCollapsed
        ? cnChk
       // ? chkValidate
        ? Navigator.of(context).push( MaterialPageRoute(
        builder: (context) => Location(widget.status,chkBkmark: false,)) )
       // :  Navigator.pushReplacementNamed(context, '/login')
        :  _flushPopUp( message: Common.internetErrorMessage,
        bgColor: Color.fromRGBO(178,0,0,1),
        duration: Duration(milliseconds: 2500)  )
        : GestureDetector(  child: Container()  );
  }

  navigateLeavingParking() async{

    var cnChk = await _internetConnectivity.checkConnectivity();
    var chkValidate = await validateAuth();

    isCollapsed
        ? cnChk ?
       // ? chkValidate?
          Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => LocationAfterParking()  ))
      //  : Navigator.pushReplacementNamed(context, '/login')
        :  _flushPopUp( message: Common.internetErrorMessage,
        bgColor: Color.fromRGBO(178,0,0,1),
        duration: Duration(milliseconds: 2500)  )
        : Container();
  }

  /*locationAccessPermissionMethodforapp() async{
    var location = loc.Location();
    var _permissionGranted = await location.hasPermission();
    if (_permissionGranted == loc.PermissionStatus.DENIED) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != loc.PermissionStatus.GRANTED) {
        locationAccessPermissionMethodforapp();
      }
    }
  }*/


// TODO: api calling methods

  Future getUserInfo({String authToken}) async{

    //Helper helper = Helper();
    var auth = await helper.getOauth();
    HttpService _httpService = HttpService();

    var userInfo = await _httpService.viewProfileApi(token: auth);

  }

}



