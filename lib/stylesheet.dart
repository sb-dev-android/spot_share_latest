import 'package:flutter/material.dart';
import 'designModelClass/podoclasses/common.dart';


TextStyle notificationTitleTextstyle
= TextStyle(fontFamily: Common.fontAdelleRegular, color: Colors.black,fontSize: 14);


TextStyle notificationMessageTextstyle
      = TextStyle(fontFamily: Common.fontAdelleRegular, color: Colors.black,fontSize: 12);


TextStyle loginPlaceHolder
= TextStyle(fontFamily: 'AdelleLight', color: Color.fromRGBO(185, 186, 187, 1));

TextStyle loginInputTxtSize
= TextStyle( fontSize: 18,
    color: Colors.black,
    fontFamily: Common.fontAdelleRegular,
    decoration: TextDecoration.none);

TextStyle  loginSignInButton
= TextStyle(
    fontSize: 16,
    color: Colors.white,
    fontFamily: Common.fontAdelleRegular
);

TextStyle  appbarHeading
= TextStyle(
    fontFamily: Common.fontAdelleRegular,
    color:Colors.black,
    fontSize: 18,
    fontWeight: FontWeight.w600);

TextStyle headingSavedLocation
= TextStyle(fontFamily: Common.fontAdelleRegular, color:Colors.black, fontSize: 24);

TextStyle forgotPasswordHeading
= TextStyle(fontFamily: Common.fontAdelleRegular,
    color:Colors.black, fontSize: 22,
);

TextStyle forgotPasswordLabel
= TextStyle( color: Color.fromRGBO(130, 150, 251, 1), fontFamily: Common.fontAdelleLight,
    fontSize: 15);

TextStyle  labelHeading
= TextStyle( color: Color.fromRGBO(130, 150, 251, 1), fontFamily: Common.fontAdelleLight,
    fontSize: 16);

TextStyle  textStyle
= TextStyle(fontFamily: Common.fontAdelleLight,
    color: Colors.white, fontSize: 18 ,
    decoration: TextDecoration.none

);

TextStyle  signInButton
= TextStyle(fontFamily: Common.fontAdelleLight, color: Color.fromRGBO(28, 129, 245, 1),
    fontSize: 16);


TextStyle  signInBottomThankYou
= TextStyle(color: Color.fromRGBO(28, 129, 245, 1), fontSize: 30, fontFamily: Common.fontAdelleRegular,);

TextStyle  signInBottomSwaprequesSent
= TextStyle(color: Color.fromRGBO(28, 129, 245, 1), fontSize: 25, fontFamily: Common.fontAdelleRegular,);


TextStyle  saveLocationBottom
= TextStyle(
    color: Color.fromRGBO(28, 129, 245, 1),
    fontSize: 18,
    fontFamily: Common.fontAdelleRegular,
);

TextStyle  signInBottomThankYouSubHeading
= TextStyle(
    color:Color.fromRGBO(116, 138, 181, 1),
    fontSize: 18, fontFamily: Common.fontAdelleLight,
);

TextStyle  signInBottomThankYouButtonOk
= TextStyle(fontSize: 20, color: Colors.white, fontFamily: Common.fontAdelleRegular);

TextStyle  signInBottomThankYouButtonNo
= TextStyle(fontSize: 20, color: Color.fromRGBO(28, 129, 245, 1), fontFamily: Common.fontAdelleRegular);

TextStyle  updateDepartureTimeButtonNo
= TextStyle(fontSize: 16, color: Color.fromRGBO(28, 129, 245, 1), fontFamily: Common.fontAdelleRegular,);


TextStyle  registerButton
= TextStyle( fontSize: 16, color: Colors.white,fontFamily: Common.fontAdelleRegular,);

TextStyle  listViewLabelText
= TextStyle(
    fontSize: 14,
    color:Color.fromRGBO(116, 138, 181, 1),
    fontFamily: Common.fontAdelleRegular,);

TextStyle  swaplistViewLabel
= TextStyle(
    fontSize: 14,
    color: Color.fromRGBO(190, 201, 255, 1),
    fontFamily: Common.fontAdelleRegular,);

TextStyle  historylistViewLabelText
= TextStyle( fontSize: 14, color: Colors.black,fontFamily: Common.fontAdelleRegular,);

TextStyle  historyListViewLabel
= TextStyle( fontSize: 14, color: Color.fromRGBO(0,0,0,0.5),fontFamily: Common.fontAdelleRegular,);

TextStyle  listViewCarNo
= TextStyle(color: Color.fromRGBO(28, 129, 245, 1), fontFamily: Common.fontAdelleRegular,fontSize: 20);

TextStyle  listViewHistoryLocation
= TextStyle(
    color: Color.fromRGBO(28, 129, 245, 1),
    fontFamily: Common.fontAdelleRegular,
    fontSize: 20);

TextStyle  listViewLocation
= TextStyle( fontSize: 18, color: Colors.black,fontFamily: Common.fontAdelleRegular,letterSpacing: 0.5);


TextStyle  savedLocationViewText
= TextStyle( fontSize: 16, color: Colors.black,fontFamily: Common.fontAdelleRegular,letterSpacing: 0.5);

TextStyle  bottomSheetMyLocation
= TextStyle(
    fontFamily: Common.fontAdelleRegular,
    fontSize: 18,
    color: Color.fromRGBO(247, 255, 0, 1));

TextStyle  bottomSheetMyLocationHeading
= TextStyle(color: Colors.white, fontSize: 20, fontFamily: Common.fontAdelleRegular,);

TextStyle  bottomSheetMyLocationAddress
= TextStyle(
  color: Color.fromRGBO(190, 201, 255, 1),
  //color: Colors.white,
  fontSize: 16,
  fontFamily: Common.fontAdelleRegular,);

TextStyle  swapRequestText
= TextStyle( color: Colors.white, fontSize: 14 );

TextStyle  bottomSheetMyLocationRadius
= TextStyle(fontFamily: Common.fontAdelleRegular, fontSize: 18, fontWeight: FontWeight.bold);

TextStyle  bottomSheetMyLocationRadiusButton
= TextStyle( fontSize: 16,color: Colors.white,fontFamily: Common.fontAdelleRegular,);

TextStyle  bottomSheetCarDetailCarNo
= TextStyle(fontFamily: Common.fontAdelleRegular, fontSize: 20, color: Colors.white
);

TextStyle  bottomSheetCarDetailLabel
= TextStyle(
    fontFamily: Common.fontAdelleRegular,
    //color: Colors.white,
    color:Color.fromRGBO(190, 201, 255, 1),
    fontSize: 14);

TextStyle  bottomSheetCarDetailText
= TextStyle(fontFamily: Common.fontAdelleLight, color: Colors.white,fontSize: 16);

TextStyle  bottomSheetCarDetailSwapParkingButton
= TextStyle(fontSize: 18, fontFamily: Common.fontAdelleRegular, color:Color.fromRGBO(28, 129, 245, 1));

TextStyle txtDrawer = TextStyle(fontFamily: Common.fontAdelleRegular,fontSize: 14,);

TextStyle  historyListViewLocation
= TextStyle( fontSize: 16, color: Colors.black,fontFamily: Common.fontAdelleRegular);


TextStyle  leavingparkingListHeading
= TextStyle( fontSize: 16, color: Colors.black,fontFamily: Common.fontAdelleRegular);

TextStyle  leavingparkingListInfoText
= TextStyle(fontFamily: Common.fontAdelleRegular, fontSize: 16, color: Color.fromRGBO(232, 240, 34, 1));

TextStyle  leavingParkingDepartureButton
= TextStyle(fontSize: 16, color: Colors.blueAccent,fontFamily: Common.fontAdelleRegular);

TextStyle sideMenuText =
TextStyle(fontFamily: Common.fontAdelleRegular, fontSize: 16,fontWeight: FontWeight.w600);

TextStyle activeSideMenuText =
TextStyle(fontFamily: Common.fontAdelleRegular,fontSize: 16,fontWeight: FontWeight.w600,color: Colors.blue);

TextStyle homeOption =
TextStyle(fontFamily: Common.fontAdelleRegular,fontSize: 16,);

TextStyle homePageListViewOptionLabel =
TextStyle(fontFamily: Common.fontAdelleRegular,fontSize: 14,color: Colors.blueAccent);

TextStyle homePageListViewOptionText =
TextStyle(fontFamily: Common.fontAdelleLight,fontSize: 16,color: Colors.white);

TextStyle selectCarLocationLabel =
TextStyle(fontFamily: Common.fontAdelleLight,fontSize: 14,color: Colors.white);

TextStyle locationSearchLocationButton =
TextStyle(fontFamily: Common.fontAdelleRegular,fontSize: 14,color: Colors.white);

TextStyle  successFullCarNo
= TextStyle(
    color:Colors.black,
    fontSize: 18,
    fontFamily: Common.fontAdelleLight);


TextStyle  successFulluSwapPopUp
= TextStyle(color: Color.fromRGBO(255, 165, 0, 1), fontSize: 50, fontFamily: Common.fontAdelleRegular);

TextStyle  settingText
= TextStyle(color: Color.fromRGBO(28,129,245,1), fontSize:19, fontFamily: Common.fontAdelleRegular);

TextStyle  settingTextClearCache
= TextStyle(color: Color.fromRGBO(255,255,255,1), fontSize:20, fontFamily: Common.fontAdelleRegular);

TextStyle  textProfileNameStyle
= TextStyle(color: Color.fromRGBO(0,0,0,1), fontSize:20, fontFamily: Common.fontAdelleRegular);

TextStyle  textProfileEmailStyle
= TextStyle(color: Color.fromRGBO(0,0,0,1), fontSize:16, fontFamily: Common.fontAdelleRegular);


