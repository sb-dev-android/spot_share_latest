import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:peer_to_peer/SignIn.dart';
import 'package:peer_to_peer/forgor_password.dart';
import 'package:peer_to_peer/home.dart';
import 'package:peer_to_peer/stylesheet.dart';
import 'package:peer_to_peer/designModelClass/podoclasses/httpservices.dart';
import 'package:peer_to_peer/designModelClass/podoclasses/models.dart';
import 'package:peer_to_peer/designModelClass/podoclasses/helper.dart';
import 'package:peer_to_peer/designModelClass/podoclasses/common.dart';
import 'dart:convert';
import 'dart:io' as io;
import 'package:device_info/device_info.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flushbar/flushbar.dart';
import 'package:regexed_validator/regexed_validator.dart';
import 'designModelClass/podoclasses/internet_connectivity.dart';
import 'package:back_button_interceptor/back_button_interceptor.dart';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';



class Login extends StatefulWidget {
  Login({Key key, this.analytics, this.observer})
      : super(key: key);
  final FirebaseAnalytics analytics;
  final FirebaseAnalyticsObserver observer;
  //bool authFailedstatus;

  @override
  _LoginState createState() => _LoginState(analytics, observer);
}

class _LoginState extends State<Login> {

  _LoginState(this.analytics, this.observer);
  final FirebaseAnalyticsObserver observer;
  final FirebaseAnalytics analytics;

  Helper helper = Helper();
  InternetConnectivity _internetConnectivity = InternetConnectivity();

  bool status=true;
  EdgeInsets inputPadding2 = EdgeInsets.only(top: 3,bottom: 3,left: 20);
  EdgeInsets contentPadding = EdgeInsets.all(10);

  TextEditingController uNameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  FocusNode _userNamefocusNode = new FocusNode();
  FocusNode _passwordfocusNode = new FocusNode();

  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  _fieldFocusChange(BuildContext context, FocusNode currentFocus,FocusNode nextFocus){
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }


  /*bool myInterceptor(bool stopDefaultButtonEvent) {
    //Navigator.pushReplacement( context, MaterialPageRoute(  builder: (context) => Login() ) );
    //io.exit(0);
    Navigator.pop(context);
    return true;
  }*/



  @override
  void initState() {

    //BackButtonInterceptor.add(myInterceptor);

  /* Common.authvalidatestatus
        ?_flushPopUp(message: Common.authInvalid,bgColor: Colors.red,duration: Duration(seconds: 2))
        :null;
*/
    // TODO: implement initState
    super.initState();

    //TODO: get device info
    deviceInfomethod();

  }

  Future<bool> onWillPop() {
    return Future.value(true);
  }

  @override
  Widget build(BuildContext context) {

    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarIconBrightness:Brightness.dark,
        statusBarColor: Colors.transparent,
      ),
    );

    return MaterialApp(
      routes: <String, WidgetBuilder> {
        '/home': (BuildContext context) => new Home(status),
        '/login': (BuildContext context) => new Login(),
      },
      debugShowCheckedModeBanner: false,
      home: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(
                  "assets/login_bg222.png",
                ),
                fit: BoxFit.fill
            )
        ),
        child: SafeArea(
          /*top: false,
          bottom: false,*/
          child: Scaffold(
            key: _scaffoldKey,
            backgroundColor: Colors.white,
            body:GestureDetector(
              onTap: (){
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: Container(
                // padding: EdgeInsets.all(5),
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(
                          "assets/login_bg222.png",
                        ),
                        fit: BoxFit.fill
                    )
                ),
                child: Column(
                  children: <Widget>[
                    Flexible(
                      child: Container(

                        child: Padding(
                          padding:  EdgeInsets.only(
                              top: (MediaQuery.of(context).size.height * 0.1)-20,
                              bottom: 30
                          ),
                          child: Image(
                            image: AssetImage("assets/logo_login.png"),
                          ),
                        ),
                      ),
                    ),

                    Container(
                      //color: Colors.red,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[

                          Padding(
                            padding: const EdgeInsets.only(bottom:20,left: 60),
                            child: Column(
                              children: <Widget>[
                                Container(
                                  padding: inputPadding2,
                                  //height: 60,
                                  decoration:BoxDecoration(
                                    color: Color.fromRGBO(245, 245, 245, 1),
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(40),
                                        bottomLeft: Radius.circular(40)
                                    ),
                                  ),
                                  child: Row(
                                    children: <Widget>[
                                      Flexible(
                                        child: SingleChildScrollView(
                                          scrollDirection: Axis.vertical,
                                          reverse: true,
                                          child: TextField(
                                            keyboardType: TextInputType.emailAddress,
                                            enableSuggestions: false,
                                            textInputAction: TextInputAction.next,
                                            focusNode: _userNamefocusNode,
                                            controller: uNameController,
                                            autocorrect: false,
                                            enableInteractiveSelection: true,
                                            keyboardAppearance: Brightness.dark,
                                            style: loginInputTxtSize,
                                            decoration: InputDecoration(
                                                hintText: 'Email',
                                                hintStyle: loginPlaceHolder,
                                                enabledBorder: OutlineInputBorder(
                                                  borderSide: BorderSide.none,
                                                ),
                                                isDense: true,
                                                focusedBorder:  OutlineInputBorder(
                                                  borderSide: BorderSide.none,
                                                )
                                            ),
                                            inputFormatters: <TextInputFormatter>[
                                              WhitelistingTextInputFormatter(RegExp("[a-zA-Z0-9.@\b]")),
                                            ],
                                            onChanged: (value){ },
                                            onSubmitted: (value){
                                              _fieldFocusChange(context,_userNamefocusNode,_passwordfocusNode);
                                            },
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),

                          Padding(
                            padding: const EdgeInsets.only(bottom:10,left: 60),
                            child: Column(
                              children: <Widget>[
                                Container(
                                  padding: inputPadding2,
                                  //height: 60,
                                  decoration:BoxDecoration(
                                    color: Color.fromRGBO(245, 245, 245, 1),
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(40),
                                        bottomLeft: Radius.circular(40)
                                    ),
                                  ),
                                  child: Row(
                                    children: <Widget>[
                                      Flexible(
                                          flex: 4,
                                          fit:FlexFit.tight,
                                          child: SingleChildScrollView(
                                            scrollDirection: Axis.vertical,
                                            reverse: true,
                                            child: TextField(
                                              textInputAction: TextInputAction.done,
                                              focusNode: _passwordfocusNode,
                                              controller: passwordController,
                                              autocorrect: false,
                                              enableInteractiveSelection: true,
                                              enableSuggestions: false,
                                              style: loginInputTxtSize,
                                              decoration: InputDecoration(
                                                  isDense: true,
                                                  hintText: 'Password',
                                                  hintStyle: loginPlaceHolder,
                                                  enabledBorder: OutlineInputBorder(
                                                    borderSide: BorderSide.none,
                                                  ),
                                                  focusedBorder:  OutlineInputBorder(
                                                    borderSide: BorderSide.none,
                                                  )
                                              ),
                                              inputFormatters: <TextInputFormatter>[
                                                //LengthLimitingTextInputFormatter(12),
                                                WhitelistingTextInputFormatter(RegExp("[a-zA-Z0-9@\b]")),
                                                //BlacklistingTextInputFormatter.singleLineFormatter,
                                              ],
                                              obscureText: true,
                                              keyboardAppearance: Brightness.dark,
                                              onSubmitted: (val){
                                                _passwordfocusNode.unfocus();
                                              },
                                            ),
                                          )
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),

                          Padding(
                            padding: const EdgeInsets.only(bottom:10,left: 60),
                            child:  Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[

                                FlatButton(
                                  child:Container(
                                    width: 150,
                                    padding: EdgeInsets.only(left: 20,top: 5,bottom: 5),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      mainAxisSize: MainAxisSize.max,
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          mainAxisSize: MainAxisSize.max,
                                          children: <Widget>[
                                            Text(
                                              "Forgot Password?",
                                              style: TextStyle(
                                                  color: Colors.blue
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                  highlightColor: Colors.lightBlue[100],
                                  hoverColor: Colors.red,
                                  splashColor: Colors.white,
                                  onPressed: () async{
                                    var cnChk = await _internetConnectivity.checkConnectivity();
                                    if(cnChk){
                                      _forgotPasswordBottomSheet();
                                    }else{
                                      _flushPopUp(
                                          message: Common.internetErrorMessage,
                                          bgColor: Color.fromRGBO(178,0,0,1),
                                          duration: Duration(milliseconds: 2500)
                                      );
                                    }
                                  },
                                ),
                              ],
                            ),
                          ),



                        ],
                      ),
                    )

                  ],
                ),
              ),
            ),

            bottomNavigationBar: Container(
              decoration: BoxDecoration(
                  color: Color.fromRGBO(28, 129, 245, 1),
                  border: Border.all(width: 0.0,color: Colors.white)
              ),
              padding: EdgeInsets.only(bottom: 22),
              height: 210,

              child: Column(
                //mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                      height: 70.1,
                      decoration: BoxDecoration(
                          color: Color.fromRGBO(28, 129, 245, 1),
                          border: Border.all(width: 0.0,color: Colors.transparent)
                      ),
                      child: Stack(
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width,
                            height: 20,
                            decoration: BoxDecoration(
                              border: Border.all(width: 0.0,color: Colors.white),
                              color: Colors.white,
                            ),
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: Container(
                              height: 52,
                              decoration: BoxDecoration(
                                  color: Color.fromRGBO(28, 129, 245, 1),
                                  border: Border.all(width: 0.0,color: Colors.transparent),
                                  image: DecorationImage(
                                      image: AssetImage("assets/Log_in.png"),
                                      fit: BoxFit.fill
                                  )
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.centerRight,
                            child: Container(
                              //color: Colors.green,
                              child: Column(
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      Container(
                                        width: MediaQuery.of(context).size.width/2-11,
                                        child: FloatingActionButton(
                                          backgroundColor: Color.fromRGBO(255, 165, 0, 1),
                                          child: Icon(Icons.arrow_forward),
                                          onPressed: (){
                                            FocusScope.of(context).requestFocus(new FocusNode());
                                            logineventclick();
                                          },
                                        ),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),

                        ],
                      )
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          GestureDetector(
                            child: Container(
                              width: 220,
                              decoration: BoxDecoration(
                                  border: Border.all(color: Color.fromRGBO(255,255,255,0.1)),
                                  borderRadius: BorderRadius.all(Radius.circular(30))
                              ),
                              padding: EdgeInsets.only(right: 15,left: 20,top: 10,bottom: 10),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      Text(
                                        "New User? Signup ",
                                        style: loginSignInButton,
                                      ),
                                      Icon(Icons.arrow_forward,color: Colors.white)
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            onTap: () async{

                              var cnChk = await _internetConnectivity.checkConnectivity();
                              if(cnChk){
                                Navigator.of(context).push(
                                    MaterialPageRoute(
                                      builder: (context)=>SignIn(),
                                    )
                                );
                              }else{
                                _flushPopUp(
                                    message: Common.internetErrorMessage,
                                    bgColor: Color.fromRGBO(178,0,0,1),
                                    duration: Duration(milliseconds: 2500)
                                );
                              }

                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }


  Future logiInfoMethod2() async {

    try{
      HttpService _httpService = HttpService();

      var loginData = await _httpService.logindata(
          email: uNameController.text,
          password: passwordController.text,
          deviceType: Common.deviceType,
          deviceOsVersion: Common.deviceOsVersion ,
          deviceToken: Common.deviceToken ,
          deviceId:  Common.deviceId ,
          appVersion: Common.appVersion
      );

      if(loginData.statusCode == 200){

        var jsonData = LogInModel.fromJson2(jsonDecode(loginData.body));

        if(jsonData.status == 0){
          _messageBottomSheet(errrorMessage: jsonData.message);
        }else{
          var  out = await helper.getOauth();

          //if(out == null){
            await helper.setOauth(
                ouath:  jsonData.oauth ,
               /* usercarno: jsonData.carno,
                username: '${jsonData.fname} ${jsonData.lname}'*/);

            await setUserInfo(token:jsonData.oauth );
         // }
          /*else{
            await helper.setOauth(
              ouath:out,
              usercarno: jsonData.carno ,
            );
             await setUserInfo(token:jsonData.oauth );
          }*/

          Navigator.pushReplacement(context,  MaterialPageRoute(
              builder: (context) => Home(status)
          ));
          clearfield();
        }
      }
      else if(loginData.statusCode == 401){
        //Login
      } else {
        var jsonData = LogInModel.fromJson2(jsonDecode(loginData.body));
        _messageBottomSheet(errrorMessage: Common.serverError);
      }


    } catch(e){
      print(e.toString());
    }
  }

  setUserInfo({String token}) async{
    HttpService _httpService = HttpService();
    var userinfo = await _httpService.viewProfileApi(token: token);

    if(userinfo.statusCode == 200){
      var jsonData = ViewProfile.fromJson(jsonDecode(userinfo.body));
      setState(() {
        helper.setUserInfo(
          profileCarUser: jsonData.fname,
          profileEmail: jsonData.email,
          profileCarNo: jsonData.carno,
          profileCarMake:jsonData.make,
          profileCarModel: jsonData.model,
          profileCarColor: jsonData.color
        );
      });
    }else if(userinfo.statusCode == 401){

    }else{
      _messageBottomSheet(errrorMessage: Common.serverError);
    }

  }

  _forgotPasswordBottomSheet({String errrorMessage}) {
    popupAnimation(child: GestureDetector(
      onTap: (){
        //Navigator.pop(context);
      },
      child: ForgotPassword(),
    ));
  }

  popupAnimation({Widget child}){
    showGeneralDialog(
      useRootNavigator: true,
      barrierDismissible: true,
      barrierColor: Color.fromRGBO(24, 121, 232, 0.6),
      barrierLabel: '',
        context: context,
        pageBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            )=>child,
      transitionDuration: Duration(milliseconds: 500),
      transitionBuilder: (
          BuildContext context,
          Animation<double> animation,
          Animation<double> secondaryAnimation,
          Widget child,
          ) =>
          SlideTransition(
            position: Tween<Offset>(
              begin: const Offset(0, 1),
              end: Offset.zero,
            ).animate(animation),
            child: child,
          ),
    );
  }

  _messageBottomSheet({String errrorMessage}){
    Flushbar(
      message:  "$errrorMessage",
      flushbarPosition: FlushbarPosition.TOP,
      flushbarStyle: FlushbarStyle.FLOATING,
      borderRadius: 10,
      margin: EdgeInsets.all(10),
      duration:  Duration(milliseconds: 1000),
    )..show(context);
  }

  _flushPopUp({String message ,Color bgColor, Duration duration}){
    Flushbar(
      message:  "${message}",
      flushbarPosition: FlushbarPosition.TOP,
      flushbarStyle: FlushbarStyle.FLOATING,
      borderRadius: 10,
      backgroundColor: bgColor,
      margin: EdgeInsets.all(10),
      duration: duration,
    ).show(context);

  }


  logineventclick() async{
    var cnChk = await _internetConnectivity.checkConnectivity();
    if(cnChk){
      clickFunction();
    }
    else{
      _flushPopUp(
          message: Common.internetErrorMessage,
          bgColor: Color.fromRGBO(178,0,0,1),
          duration: Duration(milliseconds: 2500)
      );
    }
  }

  clickFunction() async {

/*
    _passwordfocusNode.unfocus();
    _userNamefocusNode.unfocus();
*/

    bool chk = validateField();

    if(chk){
      await logiInfoMethod2();
    }
  }

  bool validateField(){
   //
    if (uNameController.text == "") {
      _messageBottomSheet(errrorMessage: Common.enterEmailString);
      return false;
    } else if (uNameController.text.trim().length == 0) {
      _messageBottomSheet(errrorMessage: '${Common.enterEmailString}');
      return false;
    } else if (!validator.email(uNameController.text)) {
      _messageBottomSheet(errrorMessage: Common.enterValidEmailString);
      return false;
    } else if (passwordController.text == "") {
      _messageBottomSheet(errrorMessage: Common.enterPasswordString);
      return false;
    }  else if (passwordController.text.trim().length == 0) {
      _messageBottomSheet(errrorMessage: '${Common.enterPasswordString}');
      return false;
    } else{
      return true;
    }
  }

  clearfield(){
    uNameController.clear();
    passwordController.clear();
  }

  deviceInfomethod() async{

    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();

    if(io.Platform.isAndroid){
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      String temp = await helper.getFcm();
      print(' device ID :  >>>>>>>   : ${androidInfo.androidId}');
      setState(() {
        Common.deviceOsVersion = androidInfo.version.release;
        Common.deviceToken = temp;
        Common.deviceType = "Android";
        Common.deviceId = androidInfo.androidId;
        Common.appVersion = "1.0.0";
      });

    }else if(io.Platform.isAndroid){
      String temp = await helper.getFcm();
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      setState(() {
        Common.deviceOsVersion = iosInfo.systemVersion;
        Common.deviceToken = temp;
        Common.deviceType = "iOS";
        Common.deviceId = iosInfo.identifierForVendor;
        Common.appVersion = "0.0.1";
      });

    }
  }

  @override
  void dispose() {
    // BackButtonInterceptor.remove(myInterceptor);
    // TODO: implement dispose
    super.dispose();
  }

}
