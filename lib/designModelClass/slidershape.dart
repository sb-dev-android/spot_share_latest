import 'package:flutter/material.dart';

class MyRadioSliderThumbShape extends SliderComponentShape {
  bool outerCircle;
  Color activeColor;
  double radius;
  int radValue;

  MyRadioSliderThumbShape({this.outerCircle = true, this.activeColor,this.radius,this.radValue});

  @override
  Size getPreferredSize(bool isEnabled, bool isDiscrete) {
    return Size.fromRadius(10);
  }



  @override
  void paint(PaintingContext context, Offset center,
      {Animation<double> activationAnimation, Animation<double> enableAnimation,
        bool isDiscrete, TextPainter labelPainter, RenderBox parentBox,
        SliderThemeData sliderTheme, TextDirection textDirection,
        double value}) {

    var outerValueStyle ;

    var innerValueStyle ;

    var txtColor;

    // paint the optional outer circle

    switch(textDirection){

      case TextDirection.rtl:
      // TODO: Handle this case.
        innerValueStyle = Paint()
          ..color = this.activeColor ?? sliderTheme.inactiveTickMarkColor
          ..style = PaintingStyle.stroke;

        outerValueStyle = Paint()
          ..color = this.activeColor ?? sliderTheme.activeTickMarkColor
          ..strokeWidth = 4.0
          ..style = PaintingStyle.fill;

            txtColor = TextStyle(color: sliderTheme.activeTickMarkColor,);
        break;

      case TextDirection.ltr:
      // TODO: Handle this case.
        outerValueStyle  = Paint()
          ..color = this.activeColor ?? sliderTheme.activeTickMarkColor
          ..style = PaintingStyle.stroke;

        innerValueStyle = Paint()
          ..color = this.activeColor ?? sliderTheme.inactiveTickMarkColor
          ..strokeWidth = 4.0
          ..style = PaintingStyle.fill;

        txtColor = TextStyle(color: sliderTheme.inactiveTickMarkColor,);
        break;
    }

    if (outerCircle == true) {
      context.canvas.drawCircle(center, radius, outerValueStyle);
    }



    // paint the inner circle
    context.canvas.drawCircle(center, radius, innerValueStyle);
    TextSpan span = new TextSpan(style: txtColor, text: (radValue+1).toString());
    TextPainter tp = new TextPainter(text: span, textAlign: TextAlign.left, textDirection: TextDirection.ltr);
    tp.layout();
    tp.paint(context.canvas, Offset(center.dx-4,center.dy-10));

  }
}

class MyRadioSliderTickMarkShape extends SliderTickMarkShape {

  MyRadioSliderTickMarkShape( { this.radius, this.outerRadius, this.index , this.initvalue} );
  double radius;
  double outerRadius;
  int index;
  int initvalue;
  static var i = 1;

  increment( int value){
      var j =i;
      i++;
      if (i>index){
        i=1;
      }
     return j;
  }

  @override
  Size getPreferredSize({SliderThemeData sliderTheme, bool isEnabled}) {

    return Size.fromRadius(0);
  }

  @override
  void paint(PaintingContext context, Offset center,
      {RenderBox parentBox, SliderThemeData sliderTheme,
        Animation<double> enableAnimation, Offset thumbCenter, bool isEnabled,
        TextDirection textDirection,
      }) {
    var outerStyle = Paint()
      ..color = Colors.blue[500] //sliderTheme.inactiveTrackColor
      ..strokeWidth = 0.1
      ..style = PaintingStyle.stroke;

    Color begin;
    Color end;
    var txtColor;


    switch (textDirection) {
      case TextDirection.ltr:
        final bool isTickMarkRightOfThumb = center.dx > thumbCenter.dx;
        begin = isTickMarkRightOfThumb
            ? sliderTheme.disabledInactiveTickMarkColor
            : sliderTheme.disabledActiveTickMarkColor;
        end =
        isTickMarkRightOfThumb ? sliderTheme.inactiveTickMarkColor : sliderTheme
            .activeTickMarkColor;
        txtColor =
        isTickMarkRightOfThumb ? TextStyle(color:Color.fromRGBO(116, 138, 181, 1),) : TextStyle(
          color: Colors.white,);
        break;
      case TextDirection.rtl:
        final bool isTickMarkLeftOfThumb = center.dx < thumbCenter.dx;
        begin = isTickMarkLeftOfThumb
            ? sliderTheme.disabledInactiveTickMarkColor
            : sliderTheme.disabledActiveTickMarkColor;
        end =
        isTickMarkLeftOfThumb ? sliderTheme.inactiveTickMarkColor : sliderTheme
            .activeTickMarkColor;

        txtColor =
        isTickMarkLeftOfThumb ? TextStyle(color: Colors.white,) : TextStyle(
            color:Color.fromRGBO(116, 138, 181, 1),);

        break;
    }

    final Paint paint = Paint()
      ..color = ColorTween(begin: begin, end: end).evaluate(enableAnimation);

    // inner circle
    context.canvas.drawCircle(center, radius, paint);
    context.canvas.drawCircle(center, outerRadius, outerStyle);

    TextSpan span = new TextSpan(style: txtColor, text: i.toString() );
    TextPainter tp = new TextPainter(
        text: span,
        textAlign: TextAlign.left,
        textDirection: TextDirection.ltr);
    tp.layout();
    tp.paint(context.canvas, Offset(center.dx - 4, center.dy - 10));

    increment(i);
  }
}

class RoundSliderTrackShape2 extends SliderTrackShape {

  const RoundSliderTrackShape2({this.disabledThumbGapWidth = 2.0, this.radius = 0});

  final double disabledThumbGapWidth;
  final double radius;

  @override
  Rect getPreferredRect({
    RenderBox parentBox,
    Offset offset = Offset.zero,
    SliderThemeData sliderTheme,
    bool isEnabled,
    bool isDiscrete,
  }) {
    final double overlayWidth = sliderTheme.overlayShape.getPreferredSize(isEnabled, isDiscrete).width;
    final double trackHeight = sliderTheme.trackHeight;
    assert(overlayWidth >= 0);
    assert(trackHeight >= 0);
    assert(parentBox.size.width >= overlayWidth);
    assert(parentBox.size.height >= trackHeight);

    final double trackLeft = offset.dx + overlayWidth / 2;
    final double trackTop = offset.dy + (parentBox.size.height - trackHeight) / 2;

    final double trackWidth = parentBox.size.width - overlayWidth;
    return Rect.fromLTWH(trackLeft, trackTop, trackWidth, trackHeight);
  }

  @override
  void paint(
      PaintingContext context,
      Offset offset, {
        RenderBox parentBox,
        SliderThemeData sliderTheme,
        Animation<double> enableAnimation,
        TextDirection textDirection,
        Offset thumbCenter,
        bool isDiscrete,
        bool isEnabled,
      }) {
    if (sliderTheme.trackHeight == 0) {
      return;
    }

    final ColorTween activeTrackColorTween =
    ColorTween(
        begin: sliderTheme.disabledActiveTrackColor,
        end: sliderTheme.activeTrackColor);


    final Paint activePaint = Paint()..color = activeTrackColorTween.evaluate(enableAnimation);

    final Paint inactivePaint = Paint()..color = Colors.grey[300]

    /*inactiveTrackColorTween.evaluate(enableAnimation)*/;

    Paint leftTrackPaint;

    Paint rightTrackPaint;

    switch (textDirection) {
      case TextDirection.ltr:
        leftTrackPaint = activePaint;
        rightTrackPaint = inactivePaint;
        break;
      case TextDirection.rtl:
        leftTrackPaint = inactivePaint;
        rightTrackPaint = activePaint;
        break;
    }

    double horizontalAdjustment = 0.0;
    if (!isEnabled) {
      final double disabledThumbRadius =
          sliderTheme.thumbShape.getPreferredSize(false, isDiscrete).width/10 ;
      final double gap = disabledThumbGapWidth * (1.0 - enableAnimation.value);
      horizontalAdjustment = disabledThumbRadius + gap;
    }

    final Rect trackRect = getPreferredRect(
      parentBox: parentBox,
      offset: offset,
      sliderTheme: sliderTheme,
      isEnabled: isEnabled,
      isDiscrete: isDiscrete,
    );
    //Modify this side
    final RRect leftTrackSegment = RRect.fromLTRBR(trackRect.left, trackRect.top,
        thumbCenter.dx - horizontalAdjustment, trackRect.bottom, Radius.circular(radius));

    context.canvas.drawRRect(leftTrackSegment, leftTrackPaint);

    final RRect rightTrackSegment = RRect.fromLTRBR(
        thumbCenter.dx + horizontalAdjustment,
        trackRect.top,
        trackRect.right,
        trackRect.bottom,
        Radius.circular(radius));

    context.canvas.drawRRect(rightTrackSegment, rightTrackPaint);


  }
}
