import 'package:flutter/material.dart';

class LoaderAnimation extends StatefulWidget {
  @override
  _LoaderAnimationState createState() => _LoaderAnimationState();
}

class _LoaderAnimationState extends State<LoaderAnimation> with SingleTickerProviderStateMixin {
AnimationController controller;

Animation base;
Animation inverted;
Animation gap;
Animation rotateanimation;

@override
void initState() {
  super.initState();

  controller = AnimationController(vsync: this, duration: Duration(seconds: 10))
    ..forward()
    ..repeat();


  rotateanimation = Tween<double>(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
          parent: controller,
          curve: Interval(
              0.0,
              1.0,
              curve: Curves.linear)
      )
  );
}

@override
Widget build(BuildContext context) {
  return RotationTransition(
    turns: rotateanimation,
    child:Padding(
        padding: const EdgeInsets.all(5),
        child: CircleAvatar(
          backgroundColor: Colors.transparent,
          radius: 20,
          child: Image(
            image: AssetImage("assets/loading.png"),
          ),
        )
    ),
  );
}

@override
  void dispose() {
    // TODO: implement dispose
  controller.stop();
  controller.dispose();
    super.dispose();
  }
}
