import 'package:flutter/material.dart';
import '../stylesheet.dart';
import '../swapRequest.dart';
import 'dart:async';
import 'dart:io';
import 'package:lokstory_flutter_lottie/lokstory_flutter_lottie.dart';
import 'package:peer_to_peer/designModelClass/customtimepicker.dart';
import 'package:peer_to_peer/designModelClass/podoclasses/common.dart';


class ForLookingMatchLoading extends StatefulWidget {
  @override
  _ForLookingMatchLoadingState createState() => _ForLookingMatchLoadingState();
}

class _ForLookingMatchLoadingState extends State<ForLookingMatchLoading> {

  LottieController controller;
  LottieController controller2;
  StreamController<double> newProgressStream;

  _ForLookingMatchLoadingState() {
    newProgressStream = new StreamController<double>();
  }


  @override
  initState() {
    super.initState();
    Future.delayed(Duration(seconds: 5),(){
      Navigator.pop(context);
    });

  }


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Color.fromRGBO(28, 129, 245, 0.3),
        body: Container(
          child: Stack(
            children: <Widget>[
              Container(
                  padding: EdgeInsets.symmetric(
                      horizontal: MediaQuery.of(context).size.width * 0.2,
                      vertical: MediaQuery.of(context).size.height * 0.2
                  ),
                  child: LottieView.fromFile(
                    filePath: "assets/jsonfile/Loading.json",
                    autoPlay: true,
                    loop: true,
                    reverse: true,
                    onViewCreated: onViewCreatedFile,
                  )),
            ],
          ),
        ),
      ),
    );

    }

  void onViewCreated(LottieController controller) {
    this.controller = controller;

    // Listen for when the playback completes
    this.controller.onPlayFinished.listen((bool animationFinished) {
      print("Playback complete. Was Animation Finished? " + animationFinished.toString());
    });
  }

  void onViewCreatedFile(LottieController controller) {
    this.controller2 = controller;
    newProgressStream.stream.listen((double progress) {
      this.controller2.setAnimationProgress(progress);
    });
  }

  void dispose() {
    super.dispose();
    newProgressStream.close();
  }
}



class ForLeavingParkingMatch extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: <String, WidgetBuilder> {
        '/swapRequest': (BuildContext context) => new SwapRequest(true)
      },
      home: SafeArea(
        top: false,
        bottom: false,
          child: ForLeavingParkingMatchLoading()
      ),
    );
  }
}


class ForLeavingParkingMatchLoading extends StatefulWidget {
  @override
  _ForLeavingParkingMatchLoadingState createState() => _ForLeavingParkingMatchLoadingState();
}

class _ForLeavingParkingMatchLoadingState extends State<ForLeavingParkingMatchLoading> {

  GlobalKey<ScaffoldState> _scaffold = GlobalKey<ScaffoldState>();
  LottieController controller;
  LottieController controller2;
  StreamController<double> newProgressStream;

  _ForLeavingParkingMatchLoadingState() {
    newProgressStream = new StreamController<double>();
  }


  @override
  initState() {
    super.initState();
    Future.delayed(Duration(seconds: 5),(){
      Navigator.pushReplacementNamed(context, '/swapRequest');
      //Navigator.pop(context);
    });

  }


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          key: _scaffold,
          backgroundColor: Color.fromRGBO(28, 129, 245, 0.3),
          body: Container(
              child: Stack(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: MediaQuery.of(context).size.width * 0.2,
                      vertical: MediaQuery.of(context).size.height * 0.2
                    ),
                      child: LottieView.fromFile(
                        filePath: "assets/jsonfile/Loading.json",
                        autoPlay: true,
                        loop: true,
                        reverse: true,
                        onViewCreated: onViewCreatedFile,
                      )),
                ],
              ),
          ),
          bottomNavigationBar: SafeArea(
            top: false,
            bottom: false,
            child:
            Platform.isIOS
              ? Container(
              height: 90,
              decoration: BoxDecoration(
                //color: Color.fromRGBO(255, 165, 0, 1),
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topRight : Radius.circular(30),
                      topLeft : Radius.circular(30)
                  ),
                  border: Border.all(width: 0.2,color: Colors.black)
              ),
              child: Column(
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                          //color: Color.fromRGBO(255, 165, 0, 1),
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                              topRight : Radius.circular(30),
                              topLeft : Radius.circular(30)
                          ),

                        ),
                      ),
                      GestureDetector(
                        onTap: (){
                          _updateDeparture();
                        },
                        child: Container(
                          height: 70,
                          decoration: BoxDecoration(
                            //color: Color.fromRGBO(255, 165, 0, 1),
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                                topRight : Radius.circular(30),
                                topLeft : Radius.circular(30)
                            ),
                            //border: Border.all(width: 0.1,color: Colors.black)
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                "${Common.updateDepartingTime}",
                                style: updateDepartureTimeButtonNo,
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            )
            : GestureDetector(
              onTap: (){
                _updateDeparture();
              },
              child: Container(
                height: 70,
                decoration: BoxDecoration(

                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topRight : Radius.circular(30),
                        topLeft : Radius.circular(30)
                    ),
                    border: Border.all(width: 0.1,color: Colors.black)
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "${Common.updateDepartingTime}",
                      style: updateDepartureTimeButtonNo,
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void onViewCreated(LottieController controller) {
    this.controller = controller;

    // Listen for when the playback completes
    this.controller.onPlayFinished.listen((bool animationFinished) {
      print("Playback complete. Was Animation Finished? " + animationFinished.toString());
    });
  }

  void onViewCreatedFile(LottieController controller) {
    this.controller2 = controller;
    newProgressStream.stream.listen((double progress) {
      this.controller2.setAnimationProgress(progress);
    });
  }

  void dispose() {
    super.dispose();
    newProgressStream.close();
  }

  _updateDeparture(){
    _scaffold.currentState.showBottomSheet<void>(
          (BuildContext context){
        return GestureDetector(
          onTap: (){
            Navigator.pop(context);
          },
          child: SafeArea(
            top: false,
            child: Scaffold(
              backgroundColor: Color.fromRGBO(24, 121, 232, 0.5),
              bottomNavigationBar: GestureDetector(
                onTap: (){  },
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height/2,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30)
                      )
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              child: Text(
                                '${Common.updateDepartingTime}',
                                style: signInBottomThankYouSubHeading,
                                textAlign: TextAlign.center,
                                softWrap: true,
                                maxLines: 3,
                                //textScaleFactor: 1.5,
                              ),
                            )
                          ],
                        ),
                      ),

                      CustomPicker(
                        updateTime: (value){
                          Common.departureTime = value;
                        },
                      ),

                      Padding(
                        padding: const EdgeInsets.all(5),
                        child: Container(
                          decoration: BoxDecoration(
                              color: Color.fromRGBO(255, 165, 0, 1),
                              borderRadius: BorderRadius.all(Radius.circular(50))
                          ),
                          width: 200,
                          child: FlatButton(
                            shape: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.all(Radius.circular(50))
                            ),
                            child: Text(
                              'Save',
                              textAlign: TextAlign.center,
                              style: signInBottomThankYouButtonOk,),
                            onPressed: (){
                              Navigator.pop(context);
                            },
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
      backgroundColor: Color.fromRGBO(24, 121, 232, 0.3),
    );
  }

}







