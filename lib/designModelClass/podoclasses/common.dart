import 'package:flutter/material.dart';
import '../DialogBox.dart';
import 'package:peer_to_peer/stylesheet.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
class Common{

//map key
  static const String googleMapKey = 'AIzaSyDsQipBrDxiVj2u_2-neZa5YVFC5PWOuv4';
// Looking for parking appbar Heading
   static String lookingForParkingHeading = 'Looking for Parking';

// Leaving for parking appbar Heading
   static String leavingForParkingHeading  = 'Leaving for Parking';

//  New York LatLng set when application first time installed
   static const LatLng newYorklatlng = LatLng(40.730610, -73.935242);

// Initial Map Camera Position
   static double zoomCamera = 16.87555694580078;

// last location variable
  static var _lattitudeLast;
  static var _longitudeLast;

// Circle Radius
  static var radiusCircle = 30.48;
  static Size customMrkerSize = Size(40,40);

// strings constent
   static const String leavingForParkingString  = 'Leaving for Parking';
   static const String lookingForParkingString = 'Looking for Parking';
   static const String lookingParking = 'Looking for parking?';
   static const String leavingParking = 'Leaving my parking';
   static const String myLocationString = "My Location";
   static const String forgotPassword = "Forgot your password?";
   static const String searchCurrentLocation = 'Search Current Location';
   static const String departingNow = 'Departing Now ?';
   static const String selectDepartingTime = "Select Departure Time ";
   static const String updateDepartingTime = 'Update Departure Time';
   static const String change = 'Change';
   static const String unknown = 'Unknown';
   static const String submitString = 'Submit';
   static const freeParkingAvailableString = 'Free Parking Available';


// side menu constent strings

// static const String homeString ='Home';
// static const String swapRequestString ='Swap Request';
// static const String historyString ='History';
// static const String savedLocationString ='Saved Location';
// static const String settingsString ='Settings';
// static const String aboutUsString ='About Us';
// static const String contactUsString ='Contact Us';
// static const String rateUsString ='Rate Us';
// static const String signOutString ='Sign Out';

// animation constent
   static bool iconStatus = false;
   static bool showSecond = false;
   static bool authvalidatestatus = false;
   static var  departureTime = 5;
   static const double elevation = 5;
   static const minLenPass = 6;

// TODO: Login user info
   static var profileCarNo;
   static var profileCarColor;
   static var profileCarModel;
   static var profileCarMake;
   static var profileCarUser;
   static var profileEmail;

// Todo :  selected car info;
   static var selectedCarColor;
   static var selectedCarNumber;
   static var selectedAddress;
   static var selectedAddressline;
   static var selectedCarMake;
   static var selectedCarModel;
   static var selectedCarUsername;
   static var selectedCarUserParkingId;

   static var selectedLeaveaddress;
   static var selectedLeavecity;


// TODO: device info
   static var deviceId;
   static var deviceToken;
   static var appVersion;
   static var deviceType;
   static var deviceOsVersion;

// Messages constent strings
 static const String messageRequestNotSent = "Request could not be sent...?";
 static const String pleaseEnterEmail = '''Please enter your registered email address below and we will send you an email to help you to reset your password.''';
 static const String sentLinkMessage = "We have sent a link to your \n registered email for reset password.";
 static const String enterEmailString = 'Please enter Email';
 static const String enterNameString = 'Please enter Full name';
 static const String enterValidEmailString = 'Please enter valid email';
 static const String enterPasswordString = 'Please enter Password';
 static const String enterConfirmPasswordString = 'Please enter Confirm password';
 static const String enterPasswordNotMatchedString = 'Password does not matched...';
 static const String enterMakeString = 'Please enter Make name';
 static const String enterModelString = 'Please enter Model';
 static const String enterColorString = 'Please enter Color';
 static const String swapRequestSent = "Swap request has \n been sent";
 static const String waitingForConfirm = "Waiting for confirmation...";
 static const String successsfullySwapWith = "You have successfully swap with";
 static const String locationHasSaved = "Your selected location has been added in Saved Location";
 static const String rateUsMessage = 'Please rate our services. Your feedback is valuable for us.';
 static const String registrationCompletMessage = "Your account has been Successfully Registered";
 static const String internetErrorMessage = "Check your internet connection and Try again.";
 static const String minLenPassMessage = "Password must be atleast 6 characters long.";
 static const String underDevelopment = "Under Development.";
 static const String serverError = "There was a problem connecting to the servers. Please try again.";
  static const String authInvalid = "Account authorization failed.";
 static const String permissionRequiredMessage = "Location permission is mandatory for this app to work properly. Please enable location permission";
 static const String bookmarkAddedsuccessfully = "Saved location successfully added in list.";
 static const String bookmarkDeletesuccessfully = "Saved location successfully removed from list.";

  static const String locationMandatory =
  "Location permission is mandatory for this app to work properly. Please enable location permission.";
// Font styles
 static const String fontAdelleLight = 'AdelleLight';
 static const String fontAdelleRegular = 'AdelleRegular';


}

class CommonMethod extends StatefulWidget {
   final String errrorMessage;
   CommonMethod({this.errrorMessage});
  @override
  _CommonMethodState createState() => _CommonMethodState();
}

class _CommonMethodState extends State<CommonMethod> {
  @override
  Widget build(BuildContext context) {
    return _messageBottomSheet(errrorMessage: widget.errrorMessage);
  }

  _messageBottomSheet({String errrorMessage}){
     showwDialog(
         context: context,
         builder: (context)=>SafeArea(
            top: false,
            bottom: false,
            child: Scaffold(
               //appBar: AppBar(backgroundColor: Colors.red,),
               backgroundColor: Color.fromRGBO(24, 121, 232, 0.5),
               bottomNavigationBar: GestureDetector(
                  onTap: (){
                     Navigator.of(context).pop();
                  },
                  child: Container(
                     width: MediaQuery.of(context).size.width,
                     height: MediaQuery.of(context).size.height/2,
                     decoration: BoxDecoration(
                         color: Colors.white,
                         borderRadius: BorderRadius.only(
                             topLeft: Radius.circular(30),
                             topRight: Radius.circular(30)
                         )
                     ),
                     child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                           Padding(
                              padding: const EdgeInsets.all(5),
                              child: Row(
                                 mainAxisAlignment: MainAxisAlignment.center,
                                 children: <Widget>[
                                    Flexible(
                                       child: Image.asset(
                                          'assets/check_registration_popup.png',
                                          width: 100,
                                          height: 100,
                                       ),
                                    )
                                 ],
                              ),
                           ),
                           Container(
                              width: 320,
                              child: Padding(
                                 padding: const EdgeInsets.all(5),
                                 child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                       Expanded(
                                          child: Text(
                                             errrorMessage,
                                             style: signInBottomThankYouSubHeading,
                                             textAlign: TextAlign.center,
                                             softWrap: true,
                                             maxLines: 2,
                                             //textScaleFactor: 1.5,
                                          ),
                                       )
                                    ],
                                 ),
                              ),
                           ),
                        ],
                     ),
                  ),
               ),
            ),
         )
     );

  }

}
