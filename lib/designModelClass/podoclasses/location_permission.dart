import 'package:permission_handler/permission_handler.dart';
import 'dart:async';

class PermissionsService {

  final PermissionHandler _permissionHandler = PermissionHandler();
  bool  _serviceEnabled ;

  Future<bool> _requestPermission(PermissionGroup permission) async {

    var result = await _permissionHandler.requestPermissions([permission]);
    if (result[permission] == PermissionStatus.granted) {
      return true;
    }
    else{
      requestLocationPermission();
    }
  }

  Future<bool> requestLocationPermission() async {

    return _requestPermission(PermissionGroup.locationWhenInUse);

  }

}