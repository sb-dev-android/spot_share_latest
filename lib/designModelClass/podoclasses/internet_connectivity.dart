import 'package:connectivity/connectivity.dart';
import 'dart:async';

class InternetConnectivity{

  final Connectivity _connectivity = Connectivity();
  static StreamSubscription<ConnectivityResult> connectivitySubscriptionVariable;

  connectivitySubscription() async{
   connectivitySubscriptionVariable = await _connectivity.onConnectivityChanged.listen(
        (ConnectivityResult result){
          print(" connectivity reulsts  $result");
          if (result == ConnectivityResult.mobile) {
            return true;
          } else if (result == ConnectivityResult.wifi) {
            return true;
          }else{
            return false;
          }
        });
  }

  checkConnectivity()async{
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return true;
    }else{
      return false;
    }
  }

  connectivitySubscriptionCancel(){
    connectivitySubscriptionVariable.cancel();
  }


Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        return true;
        break;
      case ConnectivityResult.mobile:
          return true;
          break;
      case ConnectivityResult.none:
        return false;
        break;
      default:
        break;
    }
  }

}

