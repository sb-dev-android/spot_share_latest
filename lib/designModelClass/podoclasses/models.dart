
import 'dart:io';

class LogInModel {
  int status;
  int isVerified;
  String message;
  String id;
  String fname;
  String lname;
  String email;
  String carno;
  String make;
  String model;
  String color;
  String oauth;

  LogInModel({
    this.status, this.isVerified, this.message,
    this.id, this.fname, this.lname, this.email,
    this.carno, this.make, this.model, this.color, this.oauth
  });



  factory LogInModel.fromJson2(Map<String, dynamic> json) {

    if(json['status'] == 0){
      return LogInModel(
        status: json['status'],
        message: json['message'],
      );

    }else{
      return LogInModel(
        status: json['status'],
        message: json['message'],
        id: json["data"][0]['_id'],
        isVerified: json["data"][0]['is_verified'],
        fname: json["data"][0]['first_name'],
        lname: json["data"][0]['last_name'],
        email: json["data"][0]['email'],
        carno: json["data"][0]["cars"]['car_number'].toString(),
        make: json["data"][0]["cars"]['make'].toString(),
        model: json["data"][0]["cars"]['model'].toString(),
        color: json["data"][0]["cars"]['color'].toString(),
        oauth: json["data"][0]['Oauth'].toString(),
      );
    }


  }


}

class SignInModel{

  int status;
  int isVerified;
  String message;
  String id;
  String fname;
  String lname;
  String email;
  String carno;
  String make;
  String model;
  String color;
  String oauth;

  SignInModel({
    this.status, this.isVerified, this.message,
    this.id, this.fname, this.lname, this.email,
    // this.carno, this.make, this.model, this.color, this.oauth
  });

  factory SignInModel.fromJson2(Map<String, dynamic> json) {

    if(json['status'] == 0){
      return SignInModel(
        status: json['status'],
        message: json['message'],
      );

    }else if(json['message'] == "Email already exists"){
      return SignInModel(
        status: json['status'],
        message: json['message'],
      );
    }else{
      return SignInModel(
        status: json['status'],
        message: json['message'],
        isVerified: json["data"]["is_verified"],
        id: json["data"]['_id'],
        fname: json["data"]['first_name'],
        lname: json["data"]['last_name'],
        email: json["data"]['email'],
      );

    }

  }

}

class SearchNearModel{

  int status;
  String message;
  double latitude;
  double longitude;
  String userId;
  String carNumber;
  String address;

  String make;
  String model;
  String color;

  SearchNearModel({
    this.status, this.message, this.latitude, this.longitude,
    this.userId, this.carNumber, this.address, this.make,
    this.color, this.model
  });

  factory SearchNearModel.fromJson2(Map<String, dynamic> json){
   if(json['status'] == 0){
     return SearchNearModel(
       status: json['status'],
       message: json['message'],
     );
   }else{
     return SearchNearModel(
         status: json['status'],
         message: json['message'],
         address: json["address"],
         carNumber: json["car_number"],
         userId: json["user_id"],
         longitude: json["longitude"],
         latitude: json["latitude"]
     );
   }
  }

}

class ParkingAddModel{

  int status;
  String message;
  double latitude;
  double longitude;
  String carNumber;
  String address;

  ParkingAddModel({
    this.status, this.message, this.latitude, this.longitude,
    this.carNumber, this.address
  });

  factory ParkingAddModel.fromJson2(Map<String, dynamic> json){
    if(json['status'] == 0){
      return ParkingAddModel(
        status: json['status'],
        message: json['message'],
      );
    }else{

      return ParkingAddModel(
          status: json['status'],
          message: json['message'],
          address: json["data"]["address"],
          carNumber: json["data"]["car"]["car_number"],
          longitude: json["data"]["longitude"],
          latitude: json["data"]["latitude"]
      );

    }
  }

}

class BookmarkAddModel{
  int status;
  String message;
  double latitude;
  double longitude;
  String address;
  String userId;
  String bookmarkId;

  BookmarkAddModel({ this.status,this.message,
    this.latitude,this.longitude,this.address,this.userId, this.bookmarkId});

  factory BookmarkAddModel.fromJson(Map<String, dynamic> json){
    return BookmarkAddModel(
      status: json['status'],
      message: json['message'],
      latitude: json["data"]['latitude'],
      longitude: json["data"]['longitude'],
      address: json["data"]['address'],
      userId: json["data"]['user_id'],
      bookmarkId:json["data"]['_id']
    );
  }
}

class BookmarkDeleteModel{
  int status;
  String message;


  BookmarkDeleteModel({ this.status,this.message,});

  factory BookmarkDeleteModel.fromJson(Map<String, dynamic> json){
    return BookmarkDeleteModel(
        status: json['status'],
        message: json['message'],
    );
  }
}

class BookmarkModel{

  int status;
  String message;
  double latitude;
  double longitude;
  String address;
  String locality;
  String bookmarkId;

  BookmarkModel({
    this.status, this.message, this.address, this.locality,
    this.longitude, this.latitude ,this.bookmarkId});

  factory BookmarkModel.fromJson(Map<String, dynamic> json){
    return BookmarkModel(
      status: json['address'],
      message: json['message'],
      latitude: json['latitude'],
      longitude: json['longitude'],
      address: json['address'],
      locality: json['locality'],
    );
  }
}

class CardetailModel{

  int status;
  String message;
  String carNumber;
  String make;
  String model;
  String color;
  String fname;
  String lname;
  String parkingId;

  CardetailModel({
    this.status, this.model, this.message, this.color,
    this.carNumber, this.make, this.fname, this.lname,
    this.parkingId});


   factory CardetailModel.fromJson(Map<String, dynamic> json){
    print("$json");
    return CardetailModel(
      status: json['status'],
      message: json['message'],
      model: json["data"][0]['model'],
      carNumber: json["data"][0]['car_number'],
      color: json["data"][0]['color'],
      make: json["data"][0]['make'],
      lname:  json["data"][0]['last_name'],
      fname:  json["data"][0]['first_name'],

      parkingId: json["data"][0]['parking_id'], );
  }


}


class ParkingRequestModel{

  int status;
  String message;
  String parkingId;
  String requestUserId;
  String carNumber;
  String status2;

  ParkingRequestModel({
      this.status,  this.status2, this.requestUserId,
      this.carNumber, this.parkingId,   this.message
  });

  factory ParkingRequestModel.fromJson(Map<String, dynamic> json){
    print("$json");
    return ParkingRequestModel(
      status: json['status'],
      message: json['message'],
      carNumber: json["data"]['car_number'],
      parkingId: json["data"]['parking_id'],
      requestUserId: json["data"]['request_user_id'],
      status2:  json["data"]['status'],
    );
  }
}

class ParkingRequestListModel{

  int status;
  String message;
  String carNumber;
  String make;
  String model;
  String color;
  String fname;
  String lname;
  String location;
  String parkingId;
  String id;

  ParkingRequestListModel({
    this.status, this.model, this.message, this.color,
    this.carNumber, this.make, this.fname, this.lname,
    this.location, this.parkingId, this.id
  });

  factory ParkingRequestListModel.fromJson(Map<String, dynamic> json){
    print("$json");
    return ParkingRequestListModel(
      status: json['status'],
      message: json['message'],

      carNumber: json["data"][0]['car_number'],
      color: json["data"][0]['color'],
      make: json["data"][0]['make'],
      model: json["data"][0]['model'],
      lname:  json["data"][0]['last_name'],
      fname:  json["data"][0]['first_name'],
      //location: json["data"][0]['parking_id'],
      parkingId: json["data"][0]['parking_id'],
      id: json["data"][0]['_id'],
    );
  }
}


class ViewProfile {
  int status;
  int isVerified;
  String message;
  String id;
  String fname;
  String lname;
  String email;
  String carno;
  String make;
  String model;
  String color;
  String oauth;

  ViewProfile({
    this.status, this.isVerified, this.message,
    this.id, this.fname, this.lname, this.email,
    this.carno, this.make, this.model, this.color, this.oauth
  });



  factory ViewProfile.fromJson(Map<String, dynamic> json) {

    if(json['status'] == 0){
      return ViewProfile(
        status: json['status'],
        message: json['message'],
      );

    }else{
      return ViewProfile(
        status: json['status'],
        message: json['message'],
        id: json["data"][0]['_id'],
        isVerified: json["data"][0]['is_verified'],
        fname: json["data"][0]['first_name'],
        lname: json["data"][0]['last_name'],
        email: json["data"][0]['email'],
        carno: json["data"][0]["cars"]['car_number'].toString(),
        make: json["data"][0]["cars"]['make'].toString(),
        model: json["data"][0]["cars"]['model'].toString(),
        color: json["data"][0]["cars"]['color'].toString(),
        oauth: json["data"][0]['Oauth'].toString(),
      );
    }


  }


}

class Firebasemodel {
  String message;
  String title;

  Firebasemodel({this.message,this.title});

  factory Firebasemodel.fromJson(Map<String, dynamic> message) {
    return Firebasemodel(
      title: message['notification']['title'] ,
      message: message['notification']['body'],
    );

  }

}

