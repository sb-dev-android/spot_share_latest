import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:meta/meta.dart';
import 'dart:async';

NotificationDetails get _ongoing{
  final androidChannelSpecifies = AndroidNotificationDetails(
      'id',
      'name',
      'description',
      importance: Importance.Max,
      priority: Priority.High,
      ongoing: true,
      autoCancel: false
  );
  final iosChannel = IOSNotificationDetails();
  return NotificationDetails(androidChannelSpecifies,iosChannel);
}


Future showOnGoingNotification(FlutterLocalNotificationsPlugin notification,{
  @required String title,
  @required String body,
  int id = 0
}) => showNotification(notification, title: title,body: body,id: id, type: _ongoing);


Future showNotification(FlutterLocalNotificationsPlugin notification,{
  @required String title,
  @required String body,
  @required NotificationDetails type,
  int id = 0
})=>notification.show(id, title, body, type);


