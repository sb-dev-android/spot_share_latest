
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class NotificationDetails{

  final AndroidNotificationDetails android;

  final IOSNotificationDetails ios;

  NotificationDetails(this.android,this.ios);
}