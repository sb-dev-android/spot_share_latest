
import 'dart:convert';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;

class Place {
  Place(
      Geocoding geocode, {
        this.description,
        this.placeId,
        this.types,
      }) {
    this._geocode = geocode;
  }

  Place.fromJSON(place) {
    try {
      this.description = place["description"];
      this.placeId = place["place_id"];
      this.types = place["types"];

      //this._geocode = geocode;
      this.fullJSON = place;
    } catch (e) {
      print("The argument you passed for Place is not compatible.");
    }
  }

  /// Contains the human-readable name for the returned result. For establishment results, this is usually the business name.
  String description;

  /// A textual identifier that uniquely identifies a place. To retrieve information about the place, pass this identifier in the placeId field of a Places API request. For more information about place IDs, see the [Place IDs](https://developers.google.com/places/web-service/place-id) overview.
  String placeId;

  /// Contains an array of types that apply to this place. For example:
  /// ```
  /// [ "political", "locality" ]
  /// ```
  /// or
  /// ```
  /// [ "establishment", "geocode", "beauty_salon" ]
  /// ```
  /// The array can contain multiple values. Learn more about [Place types](https://developers.google.com/places/web-service/supported_types).
  List<dynamic> types;

  /// Has the full JSON response received from the Places API. Can be used to extract extra information. More info on the [Places Autocomplete API documentation](https://developers.google.com/places/web-service/autocomplete)
  ///
  /// All of its information can be accessed like a regular [Map]. For example:
  /// ```
  /// fullJSON["structured_formating"]["main_text"]
  /// ```
  var fullJSON;

  Geocoding _geocode;
  Geolocation _geolocation;

  /// Fetches the Geolocation API from Google Maps to get more information about the place, including coordinates, bounds, etc.
  ///
  /// Learn more at [Geolocation docs](https://developers.google.com/maps/documentation/geolocation/intro)
  Future<Geolocation> get geolocation async {
    if (this._geolocation == null) {
      this._geolocation = await _geocode.getGeolocation(description);
      return _geolocation;
    }
    return _geolocation;
  }
}

class Geocoding {
  Geocoding({this.apiKey, language = 'en'});
  String apiKey;
  String language;

  Future<dynamic> getGeolocation(String adress) async {
    String trimmedAdress = adress.replaceAllMapped(' ', (m) => '+');
    final url = "https://maps.googleapis.com/maps/api/geocode/json?address=$trimmedAdress&key=$apiKey&language=$language";
    final response = await http.get(url);
    final json = jsonDecode(response.body);
    if (json["error_message"] == null) {
      return Geolocation.fromJSON(json);
    } else {
      var error = json["error_message"];
      if (error == "This API project is not authorized to use this API.")
        error +=
        " Make sure both the Geolocation and Geocoding APIs are activated on your Google Cloud Platform";
      throw Exception(error);
    }
  }
}

class Geolocation {
  Geolocation(this._coordinates, this._bounds);

  Geolocation.fromJSON(geolocationJSON) {
    this._coordinates = geolocationJSON["results"][0]["geometry"]["location"];
    this._bounds = geolocationJSON["results"][0]["geometry"]["viewport"];
    this.fullJSON = geolocationJSON["results"][0];
  }

  /// Property that holds the JSON response that contains the location of the place.
  var _coordinates;

  /// Property that holds the JSON response that contains the viewport of the place.
  var _bounds;

  /// Has the full JSON response received from the Geolocation API. Can be used to extract extra information of the location. More info on the [Geolocation API documentation](https://developers.google.com/maps/documentation/geolocation/intro)
  ///
  /// All of its information can be accessed like a regular [Map]. For example:
  /// ```
  /// fullJSON["adress_components"][2]["short_name"]
  /// ```
  var fullJSON;

  /// If you have the `google_maps_flutter` package, this method will return the coordinates of the place as
  /// a `LatLng` object. Otherwise, it'll be returned as Map.
  get coordinates {
    try {
      return LatLng(_coordinates["lat"], _coordinates["lng"]);
    } catch (e) {
      print(
          "You appear to not have the `google_maps_flutter` package installed. In this case, this method will return an object with the latitude and longitude");
      return _coordinates;
    }
  }

  /// If you have the `google_maps_flutter` package, this method will return the coordinates of the place as
  /// a `LatLngBounds` object. Otherwise, it'll be returned as Map.
  get bounds {
    try {
      return LatLngBounds(
        southwest:
        LatLng(_bounds["southwest"]["lat"], _bounds["southwest"]["lng"]),
        northeast:
        LatLng(_bounds["northeast"]["lat"], _bounds["northeast"]["lng"]),
      );
    } catch (e) {
      print(
          "You appear to not have the `google_maps_flutter` package installed. In this case, this method will return an object with southwest and northeast bounds");
      return _bounds;
    }
  }
}

