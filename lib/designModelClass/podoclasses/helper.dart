import 'package:shared_preferences/shared_preferences.dart';
import 'httpservices.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'models.dart';
import 'dart:convert';
import 'package:geocoder/geocoder.dart';

class Helper{

  setLastMapLocation({LatLng lastmapLocation})async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setDouble('LastMapLocation_lattitude', lastmapLocation.latitude);
    sharedPreferences.setDouble('LastMapLocation_longitude', lastmapLocation.longitude);
  }

  getLastMapLocation() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    double lattitude = prefs.getDouble('LastMapLocation_lattitude');
    double longitude = prefs.getDouble('LastMapLocation_longitude');
    return LatLng(lattitude,longitude);
  }

  setOauth({String ouath,}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('OauthValue', ouath);

  }

  getOauth() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString('OauthValue');
    return stringValue;
  }
  setUserInfo({
    String profileCarNo,
    String profileCarUser,
    String profileCarColor,
    String profileCarMake,
    String profileCarModel,
    String profileEmail
  }) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('ProfileCarUser', profileCarUser);
    prefs.setString('ProfileCarNo', profileCarNo);
    prefs.setString('ProfileCarColor', profileCarColor);
    prefs.setString('ProfileCarMake', profileCarMake);
    prefs.setString('ProfileCarModel', profileCarModel);
    prefs.setString('ProfileEmail', profileEmail);
  }

  getUserInfo() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String profileCarUser = prefs.getString('ProfileCarUser');
    String profileCarNo = prefs.getString('ProfileCarNo');
    String profileCarColor = prefs.getString('ProfileCarColor');
    String profileCarMake = prefs.getString('ProfileCarMake');
    String profileCarModel = prefs.getString('ProfileCarModel');
    String profileEmail = prefs.getString('ProfileEmail');

    Map<String, String> map = {
      'ProfileCarUser': profileCarUser,
      'ProfileCarNo': profileCarNo,
      'ProfileCarColor': profileCarColor,
      'ProfileCarMake': profileCarMake,
      'ProfileCarModel': profileCarModel,
      'ProfileEmail': profileEmail
    };
    return map;
  }



  getFcm() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString('fcm');
    return stringValue;
  }

  setFcmToken({String fcmToken}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('fcm', fcmToken);
  }

  getCarUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString('CarNo');
    return stringValue;
  }

  getUserName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString('UserName');
    return stringValue;
  }

  clearOauth() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove("OauthValue");
    prefs.remove('ProfileCarUser');
    prefs.remove('ProfileCarNo');
    prefs.remove('ProfileCarColor');
    prefs.remove('ProfileCarMake');
    prefs.remove('ProfileCarModel');
    prefs.remove('ProfileEmail');
  }

  getLoginUserCarNo() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString('Usercarno');
    return stringValue;
  }


  bookmarkmethod() async{
    String oauth = await getOauth();
    var jsonData;
    try {
      HttpService _httpService = HttpService();

      var bookmarkdata = await _httpService.bookmark(token:  oauth);

      if(bookmarkdata.statusCode == 200){
        jsonData = await json.decode(bookmarkdata.body);
      }
      return jsonData;

    }catch(e){
      print("Exception : >>>>> " + e.toString());
    }
  }

  getAddressLocation({LatLng latLng}) async {
    final coordinates = new Coordinates( latLng.latitude, latLng.longitude );
    var addresses = await Geocoder.local.findAddressesFromCoordinates(
        coordinates);
    var first = addresses.first;

    //test
    return first.addressLine;
  }

  getLocality({LatLng latLng}) async {
    final coordinates = new Coordinates( latLng.latitude, latLng.longitude );
    var addresses = await Geocoder.local.findAddressesFromCoordinates(
        coordinates);
    var first = addresses.first;
    return first.locality;
  }

  Future validateAuth() async {

    try {
      String oauth = await getOauth();
      HttpService _httpService = HttpService();

      var validateAuthData = await _httpService.validate( token:  oauth);

      var jsonData = await jsonDecode(validateAuthData.body);

      return jsonData;

    }catch(e){
      print("Exception : >>>>> " + e.toString());
    }
  }

  validateAuthCheck() async{
    var valRes = await validateAuth();
    if(valRes['status'] == 1){
      return true;
    }else{
      return false;
    }
  }

}
