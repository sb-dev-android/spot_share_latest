
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'dart:io';
import 'dart:convert';

class HttpService {


  var isDebug = 1;

//TODO: log-in add
  Future<http.Response> logindata(
      {
        String email, String password,
        String deviceType,
        String deviceOsVersion,
        String appVersion,
        String deviceToken, String deviceId,
      }) async {

    String postsURL = "http://3.20.90.174:5000/api/entrance/login";
    var header = {
      HttpHeaders.contentTypeHeader: 'application/json',
      "device-id" : "$deviceId",
      "device-token" : "$deviceToken",
      "app-version" : "$appVersion",
      "device-type" : "$deviceType",
      "is-debug" : "$isDebug",
      "device-os-version" :"$deviceOsVersion"     };

    var body = jsonEncode({"email":"$email", "password":"$password"});
    try{
      http.Response res =
      await http.post(
          postsURL,
          body: body,
          headers:header);
      return res;
    }catch(e){
      print(e.toString());
    }

  }


//TODO: sign-in add
  Future<http.Response> signindata({
        String fname,
        String lname,
        String carNo,
        String makeName,
        String model,
        String color,
        String email,
        String password,
        String password2
      }) async {

          String signURL = "http://3.20.90.174:5000/api/entrance/signup";

          // String signURL = "http://192.168.1.118:5000/api/entrance/signup";

          var header = { HttpHeaders.contentTypeHeader: 'application/json', };

          var body = jsonEncode({
            "first_name":   "$fname",
            "last_name":    "$lname",
            "email":        "$email",
            "car_number":    "$carNo",
            "make":         "$makeName",
            "model":        "$model",
            "color":        "$color",
            "password":     "$password",
            "password2":    "$password2"
          });

        try{
          http.Response res =
          await http.post(
              signURL,
              body: body,
              headers:header);

         return res;

        }catch(e){
          print(e.toString());
        }

  }


//TODO: history add
  Future<http.Response> history({
    String token,
    LatLng latLng,
    double radius
  }) async {
        String postsURL = "http://3.20.90.174:5000/api/account/search-near-by";
        var header = {
          HttpHeaders.contentTypeHeader: 'application/json',
          "auth_token" : "$token",
        };

        var body = jsonEncode({
          "latitude":"${latLng.latitude}",
          "longitude":"${latLng.longitude}",
          "radius":"$radius"});

        try{
          http.Response res =
          await http.post(
              postsURL,
              body: body,
              headers:header)
              .timeout(Duration(minutes: 30),onTimeout: (){print("Time Out occurs");});
         return res;
        }catch(e){
          print(e.toString());
        }
  }



//TODO: bookmark add
  Future<http.Response> bookmarkAdd({
    String token,
    LatLng latLng,
    String address
  }) async {
    String postsURL = "http://3.20.90.174:5000/api/account/add-bookmark";
    var header = {
      HttpHeaders.contentTypeHeader: 'application/json',
      "auth_token" : "$token",
    };

    var body = jsonEncode({
      "latitude":"${latLng.latitude}",
      "longitude":"${latLng.longitude}",
      "address":"$address"});

    try{
      http.Response res =
      await http.post(
          postsURL,
          body: body,
          headers:header)
          .timeout(Duration(minutes: 30),onTimeout: (){print("Time Out occurs");});
      return res;
    }catch(e){
      print(e.toString());
    }
  }

  //TODO: bookmark delete
  Future<http.Response> bookmarkDelete({
    String token,
    String bookmarkId
  }) async {
    String postsURL = "http://3.20.90.174:5000/api/account/bookmark-delete";
    var header = {
      HttpHeaders.contentTypeHeader: 'application/json',
      "auth_token" : "$token",
    };

    var body = jsonEncode({ "_id":"$bookmarkId" });

    try{
      http.Response res =
      await http.post(
          postsURL,
          body: body,
          headers:header)
          .timeout(Duration(minutes: 30),onTimeout: (){print("Time Out occurs");});
      return res;
    }catch(e){
      print(e.toString());
    }
  }

//TODO: parking add
  Future<http.Response> parkingAdd({
    String token,
    LatLng latLng,
    String address
  }) async {
     String postsURL = "http://3.20.90.174:5000/api/account/leaving-parking";
    //String postsURL = "http://192.168.1.118:5000/api/account/leaving-parking";
    var header = {
      HttpHeaders.contentTypeHeader: 'application/json',
      "auth_token" : "$token",
    };

    var body = jsonEncode({
      "latitude":"${latLng.latitude}",
      "longitude":"${latLng.longitude}",
      "address":"$address"  });

    try{
      http.Response res =
      await http.post(
          postsURL,
          body: body,
          headers:header)
          .timeout(Duration(minutes: 30),onTimeout: (){print("Time Out occurs");});
      print("add parking  api : >>>>>>${res.body}");

      return res;

    }catch(e){
      print("Leaving parking add Http service Exception : >>>>> " + e.toString());
    }
  }

//TODO: Search Near
  Future<http.Response> searchNearMethod({
    String token,
    LatLng latLng,
    double radius
  }) async {
    String postsURL = "http://3.20.90.174:5000/api/account/search-near-by";
    //String postsURL = "http://192.168.1.118:5000/api/account/search-near-by";
    var header = {
      HttpHeaders.contentTypeHeader: 'application/json',
      "auth_token" : "$token",
    };
    var body = jsonEncode({
    "latitude":   "${latLng.latitude}", //"-35.30278",    //
      "longitude": "${latLng.longitude}",  // "149.14167",   //
      "radius":     "$radius"});

    try{
      http.Response res =
      await http.post(
          postsURL,
          body: body,
          headers:header)
          .timeout(Duration(minutes: 30),onTimeout: (){print("Time Out occurs");});
      return res;
    }catch(e){
      print(e.toString());
    }
  }


//TODO: bookmark
  Future<http.Response> bookmark({
    String token,
    LatLng latLng,
    double radius
  }) async {
    String postsURL = "http://3.20.90.174:5000/api/account/bookmark";
    var header = {
      HttpHeaders.contentTypeHeader: 'application/json',
      "auth_token" : "$token",
    };

    try{
      http.Response res = await http.post(
          postsURL,
          body: jsonEncode({}),
          headers:header)
          .timeout(Duration(minutes: 30),onTimeout: (){print("Time Out occurs");});

        return res;

    }catch(e){
      print(e.toString());
    }
  }

//TODO: car deatail
  Future<http.Response> carDetail({
    String token,
    String parkingId
  }) async {
    String postsURL = "http://3.20.90.174:5000/api/account/car-detail";
    var header = {
      HttpHeaders.contentTypeHeader: 'application/json',
      "auth_token" : "$token",
    };

    try{
      http.Response res = await http.post(
          postsURL,
          body: jsonEncode({"parking_id": "$parkingId"}),
          headers:header)
          .timeout(Duration(minutes: 30),onTimeout: (){print("Time Out occurs");});

      return res;

    }catch(e){
      print(e.toString());
    }
  }


//TODO: parking request
  Future<http.Response> parkingRequest({
    String token,
    String parkingId
  }) async {
    String postsURL = "http://3.20.90.174:5000/api/account/parking-request";
    var header = {
      HttpHeaders.contentTypeHeader: 'application/json',
      "auth_token" : "$token",
    };

    try{
      http.Response res = await http.post(
          postsURL,
          body: jsonEncode({"parking_id": "$parkingId"}),
          headers:header)
          .timeout(Duration(minutes: 30),onTimeout: (){print("Time Out occurs");});

      return res;

    }catch(e){
      print(e.toString());
    }
  }


//TODO: parking request list
  Future<http.Response> parkingRequestList({String token}) async {
    String postsURL = "http://3.20.90.174:5000/api/account/requested-list";
    var header = {
      HttpHeaders.contentTypeHeader: 'application/json',
      "auth_token" : "$token",
    };

    try{
      http.Response res = await http.post(
          postsURL,
          body: jsonEncode({}),
          headers:  header  )
          .timeout(Duration(minutes: 30),onTimeout: (){print("Time Out occurs");});

      return res;

    }catch(e){
      print(e.toString());
    }
  }


//TODO: accept request list
  Future<http.Response> acceptRequest({String token ,String parkingId ,String id}) async {
    String postsURL = "http://3.20.90.174:5000/api/account/accept-request";
    var header = {
      HttpHeaders.contentTypeHeader: 'application/json',
      "auth_token" : "$token",
    };

    try{
      http.Response res = await http.post(
          postsURL,
          body: jsonEncode({
            "parking_id": parkingId,
            "_id" : id
          }),
          headers:  header  )
          .timeout(Duration(minutes: 30),onTimeout: (){print("Time Out occurs");});

      return res;

    }catch(e){
      print(e.toString());
    }
  }


//TODO: decline request list
  Future<http.Response> declineRequest({String token ,String id}) async {
    String postsURL = "http://3.20.90.174:5000/api/account/decline-request";
    var header = {
      HttpHeaders.contentTypeHeader: 'application/json',
      "auth_token" : "$token",
    };

    try{
      http.Response res = await http.post(
          postsURL,
          body: jsonEncode({"_id":"$id"}),
          headers:  header  )
          .timeout(Duration(minutes: 30),onTimeout: (){print("Time Out occurs");});
      return res;

    }catch(e){
      print(e.toString());
    }
  }

//TODO: validate api
  Future<http.Response> validate({String token ,String id}) async {
    String postsURL = "http://3.20.90.174:5000/api/account/validate-auth";
    var header = {
      HttpHeaders.contentTypeHeader: 'application/json',
      "auth_token" : "$token",
    };

    try{
      http.Response res = await http.post(
          postsURL,
          body: jsonEncode({"_id":"$id"}),
          headers:  header  )
          .timeout(Duration(minutes: 30),onTimeout: (){print("Time Out occurs");});
      return res;

    }catch(e){
      print(e.toString());
    }
  }


//TODO: view profile
  Future<http.Response> viewProfileApi({  String token, }) async {
    String postsURL = "http://3.20.90.174:5000/api/account/view-profile";
    var header = {
      HttpHeaders.contentTypeHeader: 'application/json',
      "auth_token" : "$token",
    };

    try{
      http.Response res =
      await http.post(
          postsURL,
          headers:header)
          .timeout(Duration(minutes: 30),onTimeout: (){print("Time Out occurs");});
      return res;
    }catch(e){
      print(e.toString());
    }
  }


//TODO: edit profile
 /* Future<http.Response> editProfileApi({
    String token,
    LatLng latLng,
    double radius
  }) async {
    String postsURL = "http://3.20.90.174:5000/api/account/edit-profile";
    var header = {
      HttpHeaders.contentTypeHeader: 'application/json',
      "auth_token" : "$token",
    };

    var body = jsonEncode({
      "latitude":"${latLng.latitude}",
      "longitude":"${latLng.longitude}",
      "radius":"$radius"});

    try{
      http.Response res =
      await http.post(
          postsURL,
          body: body,
          headers:header)
          .timeout(Duration(minutes: 30),onTimeout: (){print("Time Out occurs");});
      return res;
    }catch(e){
      print(e.toString());
    }
  }*/


//TODO: Forgot Password profile
 Future<http.Response> forgotPasswordApi({String email}) async {
    String postsURL = "http://3.20.90.174:5000/api/entrance/forgot-password";
    var header = { HttpHeaders.contentTypeHeader: 'application/json' };
    var body = jsonEncode({ "email":"$email" });

    try{
      http.Response res =
      await http.post(
          postsURL,
          body: body,
          headers:header)
          .timeout(Duration(minutes: 30),onTimeout: (){print("Time Out occurs");});
      res.body;
      return res;
    }catch(e){
      print(e.toString());
    }
  }

}