import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'number_picker.dart';

class CustomPicker extends StatefulWidget {
  final Function(int) updateTime;
  CustomPicker({@required this.updateTime});
  @override
  _CustomPickerState createState() => _CustomPickerState();
}

class _CustomPickerState extends State<CustomPicker> {

  static var initValueOfPicker = 1;
  @override
  void initState() {
    SystemChrome.setEnabledSystemUIOverlays([]);
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child:
      NumberPicker.integer(
        listViewWidth: 200,
        decoration: BoxDecoration(
            border: Border(
                top: BorderSide(color: Colors.black,width: 0.3),
                bottom: BorderSide(color: Colors.black,width: 0.3)
            )
        ),
        initialValue: initValueOfPicker ,
        minValue: 0,
        maxValue: 11,
        highlightSelectedValue: true,
        onChanged: (value){
          setState(() {
            initValueOfPicker = value;
          });
          widget.updateTime(value * 5);
        },

      ),
    );
  }
}
