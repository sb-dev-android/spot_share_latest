import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:io' as io;
import 'dart:convert';
import 'package:peer_to_peer/designModelClass/podoclasses/common.dart';
import 'package:peer_to_peer/designModelClass/podoclasses/models.dart';
import 'package:peer_to_peer/Login.dart';
import 'package:peer_to_peer/stylesheet.dart';
import 'package:peer_to_peer/designModelClass/podoclasses/httpservices.dart';
import 'package:peer_to_peer/designModelClass/DialogBox.dart';
import 'package:flushbar/flushbar.dart';
import 'package:regexed_validator/regexed_validator.dart';
import 'designModelClass/podoclasses/internet_connectivity.dart';
import 'package:back_button_interceptor/back_button_interceptor.dart';

class SignIn extends StatefulWidget {
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> with SingleTickerProviderStateMixin {

  bool status = true;

  TextEditingController fnameController = new TextEditingController();
  TextEditingController lnameController = new TextEditingController();
  TextEditingController carNoController = new TextEditingController();
  TextEditingController makeController = new TextEditingController();
  TextEditingController modelController = new TextEditingController();
  TextEditingController colorController = new TextEditingController();

  TextEditingController emailController = new TextEditingController();
  TextEditingController pass1Controller = new TextEditingController();
  TextEditingController pass2Controller = new TextEditingController();


  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  var carModel;
  var carmake;
  EdgeInsets inputPadding = EdgeInsets.only(top: 20, bottom: 20, left: 20);
  EdgeInsets padding2 = EdgeInsets.only(bottom: 8, left: 25, top: 12);
  EdgeInsets contentPadding = EdgeInsets.only(top: 20, left: 10, bottom: 23);

  EdgeInsets inputPadding2 = EdgeInsets.only(top: 14, bottom: 11.5, left: 5);
  EdgeInsets padding22 = EdgeInsets.only(bottom: 8, left: 25, top: 15);
  EdgeInsets contentPadding2 = EdgeInsets.only(bottom: 9, left: 25, top: 12,right: 15);

  double height = 73;

  FocusNode _firstNamefocusNode = new FocusNode();
  // FocusNode _lastNamefocusNode = new FocusNode();
  FocusNode _carNofocusNode = new FocusNode();
  FocusNode _makefocusNode = new FocusNode();
  FocusNode _modelfocusNode = new FocusNode();
  FocusNode _colorfocusNode = new FocusNode();

  FocusNode _emailfocusNode = new FocusNode();
  FocusNode _pass1focusNode = new FocusNode();
  FocusNode _pass2focusNode = new FocusNode();


  _fieldFocusChange(BuildContext context, FocusNode currentFocus,
      FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  static const int type_index = 2;


  bool myInterceptor(bool stopDefaultButtonEvent) {
    //Navigator.pushReplacement( context, MaterialPageRoute(  builder: (context) => Login() ) );
    Navigator.pop(context);
    return true;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    BackButtonInterceptor.add(myInterceptor);
  }


  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarIconBrightness: Brightness.dark,
        statusBarColor: Colors.transparent,
      ),
    );

    return MaterialApp(
      routes: <String, WidgetBuilder>{
        '/login': (BuildContext context) => new Login(),
      },
      debugShowCheckedModeBanner: false,
      home: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          key: _scaffoldKey,
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(
                MediaQuery
                    .of(context)
                    .size
                    .height * 0.1 - 20
            ),
            child: SafeArea(
              top: true,
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text("Registration", style: appbarHeading,),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),

          body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: Container(
              height: MediaQuery
                  .of(context)
                  .size
                  .height,
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                          color: Color.fromRGBO(28, 129, 245, 1),
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(35)
                          )
                      ),
                      child: SingleChildScrollView(
                        child: Container(
                          child: Column(
                            children: <Widget>[

                              Padding(
                                padding: padding22,
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      padding: inputPadding2,
                                      //height: height,
                                      decoration:BoxDecoration(
                                        color: Color.fromRGBO(24, 121, 232, 1),
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(40),
                                            bottomLeft: Radius.circular(40)
                                        ),
                                      ),
                                      child: Row(
                                        children: <Widget>[
                                          Flexible(
                                              child: SingleChildScrollView(
                                                scrollDirection: Axis.vertical,
                                                reverse: true,
                                                padding: EdgeInsets.only(top: 3,bottom: 0,left: 0),
                                                child: TextField(

                                                  textCapitalization: TextCapitalization.words,
                                                  textInputAction: TextInputAction.next,
                                                  focusNode: _firstNamefocusNode,
                                                  controller: fnameController,
                                                  style: textStyle,
                                                  autocorrect: false,

                                                  enableInteractiveSelection: true,
                                                  decoration: InputDecoration(
                                                      labelText: 'Full Name',
                                                      labelStyle: labelHeading,
                                                      contentPadding: contentPadding2,
                                                      enabledBorder: OutlineInputBorder(
                                                        borderSide: BorderSide.none,
                                                      ),
                                                      isDense: true,
                                                      focusedBorder:  OutlineInputBorder(
                                                        borderSide: BorderSide.none,
                                                      )
                                                  ),
                                                  keyboardAppearance: Brightness.dark,
                                                  keyboardType: TextInputType.text,
                                                  inputFormatters: <TextInputFormatter>[
                                                    WhitelistingTextInputFormatter(RegExp("[a-zA-Z \b]")),
                                                  ],
                                                  onSubmitted: (value){
                                                    _fieldFocusChange(context, _firstNamefocusNode, _emailfocusNode);
                                                  },
                                                ),
                                              )
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),

                              /*Padding(
                                padding: padding22,
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      padding: inputPadding2,
                                      //height: height,
                                      decoration:BoxDecoration(
                                        color: Color.fromRGBO(24, 121, 232, 1),
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(40),
                                            bottomLeft: Radius.circular(40)
                                        ),
                                      ),
                                      child: Row(
                                        children: <Widget>[
                                          Flexible(
                                              child: SingleChildScrollView(
                                                scrollDirection: Axis.vertical,
                                                reverse: true,
                                                padding: EdgeInsets.only(top: 3,bottom: 0,left: 0),
                                                child: TextField(
                                                  textInputAction: TextInputAction.next,
                                                  focusNode: _lastNamefocusNode,
                                                  controller: lnameController,
                                                  style: textStyle,
                                                  autocorrect: false,
                                                  enableInteractiveSelection: false,
                                                  decoration: InputDecoration(
                                                      labelText: 'Last Name',
                                                      labelStyle: labelHeading,
                                                      contentPadding: contentPadding2,
                                                      enabledBorder: OutlineInputBorder(
                                                        borderSide: BorderSide.none,
                                                      ),
                                                      isDense: true,
                                                      focusedBorder: OutlineInputBorder(
                                                        borderSide: BorderSide.none,
                                                      )
                                                  ),
                                                  keyboardAppearance: Brightness.dark,
                                                  keyboardType: TextInputType.text,
                                                  inputFormatters: <
                                                      TextInputFormatter>[
                                                    WhitelistingTextInputFormatter(
                                                        RegExp("[a-zA-Z\\-]")),
                                                  ],
                                                  onSubmitted: (value) {
                                                    _fieldFocusChange(
                                                        context, _lastNamefocusNode,
                                                        _carNofocusNode);
                                                  },
                                                ),
                                              )
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),*/

                              Padding(
                                padding: padding22,
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      padding: inputPadding2,
                                      //height: height,
                                      decoration:BoxDecoration(
                                        color: Color.fromRGBO(24, 121, 232, 1),
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(40),
                                            bottomLeft: Radius.circular(40)
                                        ),
                                      ),
                                      child: Row(
                                        children: <Widget>[
                                          Flexible(
                                              child: SingleChildScrollView(
                                                scrollDirection: Axis.vertical,
                                                reverse: true,
                                                padding: EdgeInsets.only(top: 3,bottom: 0,left: 0),
                                                child: TextField(

                                                  textInputAction: TextInputAction.next,
                                                  keyboardType: TextInputType.emailAddress,
                                                  focusNode: _emailfocusNode,
                                                  controller: emailController,
                                                  style: textStyle,
                                                  autocorrect: false,
                                                  enableInteractiveSelection: true,
                                                  decoration: InputDecoration(
                                                      labelText: 'Email',
                                                      labelStyle: labelHeading,
                                                      contentPadding: contentPadding2,
                                                      enabledBorder: OutlineInputBorder(
                                                        borderSide: BorderSide.none,
                                                      ),
                                                      isDense: true,
                                                      focusedBorder: OutlineInputBorder(
                                                        borderSide: BorderSide.none,
                                                      )
                                                  ),
                                                  keyboardAppearance: Brightness.dark,
                                                  inputFormatters: <
                                                      TextInputFormatter>[
                                                    //LengthLimitingTextInputFormatter(12),
                                                    WhitelistingTextInputFormatter(
                                                        RegExp("[a-zA-Z0-9.@\b]")),
                                                    //BlacklistingTextInputFormatter.singleLineFormatter,
                                                  ],
                                                  onSubmitted: (value) {
                                                    _fieldFocusChange(
                                                        context, _emailfocusNode,
                                                        _pass1focusNode);
                                                  },
                                                ),
                                              )
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),

                              Padding(
                                padding: padding22,
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      padding: inputPadding2,
                                      //height: height,
                                      decoration:BoxDecoration(
                                        color: Color.fromRGBO(24, 121, 232, 1),
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(40),
                                            bottomLeft: Radius.circular(40)
                                        ),
                                      ),
                                      child: Row(
                                        children: <Widget>[
                                          Flexible(
                                              child: SingleChildScrollView(
                                                scrollDirection: Axis.vertical,
                                                reverse: true,
                                                padding: EdgeInsets.only(top: 3,bottom: 0,left: 0),
                                                child: TextField(
                                                  obscureText: true,
                                                  textInputAction: TextInputAction.next,
                                                  focusNode: _pass1focusNode,
                                                  controller: pass1Controller,
                                                  style: textStyle,
                                                  autocorrect: false,
                                                  enableInteractiveSelection: true,
                                                  decoration: InputDecoration(
                                                      labelText: 'Password',
                                                      labelStyle: labelHeading,
                                                      contentPadding: contentPadding2,
                                                      enabledBorder: OutlineInputBorder(
                                                        borderSide: BorderSide.none,
                                                      ),
                                                      isDense: true,

                                                      focusedBorder: OutlineInputBorder(
                                                        borderSide: BorderSide.none,
                                                      )
                                                  ),
                                                  keyboardAppearance: Brightness.dark,
                                                  keyboardType: TextInputType.text,
                                                  inputFormatters: <
                                                      TextInputFormatter>[
                                                    WhitelistingTextInputFormatter(
                                                        RegExp("[a-zA-Z0-9@\b]")),
                                                  ],
                                                  onSubmitted: (value) {
                                                    _fieldFocusChange(
                                                        context, _pass1focusNode,
                                                        _pass2focusNode);
                                                  },
                                                ),
                                              )
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Padding(
                                padding: padding22,
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      padding: inputPadding2,
                                      //height: height,
                                      decoration:BoxDecoration(
                                        color: Color.fromRGBO(24, 121, 232, 1),
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(40),
                                            bottomLeft: Radius.circular(40)
                                        ),
                                      ),
                                      child: Row(
                                        children: <Widget>[
                                          Flexible(
                                              child: SingleChildScrollView(
                                                scrollDirection: Axis.vertical,
                                                reverse: true,
                                                padding: EdgeInsets.only(top: 3,bottom: 0,left: 0),
                                                child: TextField(
                                                  obscureText: true,
                                                  textInputAction: TextInputAction.next,
                                                  focusNode: _pass2focusNode,
                                                  controller: pass2Controller,
                                                  style: textStyle,
                                                  autocorrect: false,
                                                  enableInteractiveSelection: true,
                                                  decoration: InputDecoration(
                                                      labelText: 'Confirm Password',
                                                      labelStyle: labelHeading,
                                                      contentPadding: contentPadding2,
                                                      enabledBorder: OutlineInputBorder(
                                                        borderSide: BorderSide.none,
                                                      ),
                                                      isDense: true,
                                                      focusedBorder: OutlineInputBorder(
                                                        borderSide: BorderSide.none,
                                                      )
                                                  ),
                                                  keyboardAppearance: Brightness.dark,
                                                  keyboardType: TextInputType.text,
                                                  inputFormatters: <
                                                      TextInputFormatter>[
                                                    WhitelistingTextInputFormatter(
                                                        RegExp("[a-zA-Z0-9@\b]")),
                                                  ],

                                                  onSubmitted: (value) {
                                                    _fieldFocusChange(
                                                        context, _pass2focusNode,
                                                        _carNofocusNode);
                                                  },
                                                ),
                                              )
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),

                              Padding(
                                padding: padding22,
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      padding: inputPadding2,
                                      //height: height,
                                      decoration:BoxDecoration(
                                        color: Color.fromRGBO(24, 121, 232, 1),
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(40),
                                            bottomLeft: Radius.circular(40)
                                        ),
                                      ),
                                      child: Row(
                                        children: <Widget>[
                                          Flexible(
                                              child: SingleChildScrollView(
                                                scrollDirection: Axis.vertical,
                                                reverse: true,
                                                padding: EdgeInsets.only(top: 3,bottom: 0,left: 0),
                                                child: TextField(
                                                  textInputAction: TextInputAction.next,
                                                  focusNode: _carNofocusNode,
                                                  controller: carNoController,
                                                  style: textStyle,
                                                  autocorrect: false,
                                                  enableInteractiveSelection: false,
                                                  decoration: InputDecoration(
                                                      labelText: 'Car no',
                                                      labelStyle: labelHeading,
                                                      contentPadding: contentPadding2,
                                                      enabledBorder: OutlineInputBorder(
                                                        borderSide: BorderSide.none,
                                                      ),
                                                      isDense: true,
                                                      focusedBorder: OutlineInputBorder(
                                                        borderSide: BorderSide.none,
                                                      )
                                                  ),
                                                  keyboardAppearance: Brightness.dark,
                                                  keyboardType: TextInputType.text,
                                                  inputFormatters: <
                                                      TextInputFormatter>[
                                                    WhitelistingTextInputFormatter(
                                                        RegExp("[a-zA-Z0-9\b\\-]")),
                                                  ],
                                                  onSubmitted: (value) {
                                                    _fieldFocusChange(
                                                        context, _carNofocusNode,
                                                        _makefocusNode);
                                                  },
                                                ),
                                              )
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),

                              Padding(
                                padding: padding22,
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      padding: inputPadding2,
                                      //height: height,
                                      decoration:BoxDecoration(
                                        color: Color.fromRGBO(24, 121, 232, 1),
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(40),
                                            bottomLeft: Radius.circular(40)
                                        ),
                                      ),
                                      child: Row(
                                        children: <Widget>[
                                          Flexible(
                                              child: SingleChildScrollView(
                                                scrollDirection: Axis.vertical,
                                                reverse: true,
                                                padding: EdgeInsets.only(top: 3,bottom: 0,left: 0),
                                                child: TextField(
                                                  textCapitalization: TextCapitalization.sentences,
                                                  textInputAction: TextInputAction.next,
                                                  focusNode: _makefocusNode,
                                                  controller: makeController,
                                                  style: textStyle,
                                                  autocorrect: false,
                                                  enableInteractiveSelection: true,
                                                  decoration: InputDecoration(
                                                      labelText: 'Make',
                                                      hintText: 'eg: Jaguar',
                                                      hintStyle: labelHeading,
                                                      labelStyle: labelHeading,
                                                      contentPadding: contentPadding2,
                                                      enabledBorder: OutlineInputBorder(
                                                        borderSide: BorderSide.none,
                                                      ),
                                                      isDense: true,
                                                      focusedBorder: OutlineInputBorder(
                                                        borderSide: BorderSide.none,
                                                      )
                                                  ),
                                                  keyboardAppearance: Brightness.dark,
                                                  keyboardType: TextInputType.text,
                                                  inputFormatters: <
                                                      TextInputFormatter>[
                                                    WhitelistingTextInputFormatter(
                                                        RegExp("[a-zA-Z0-9 \b]")),
                                                  ],
                                                  onSubmitted: (value) {
                                                    _fieldFocusChange(
                                                        context, _makefocusNode,
                                                        _modelfocusNode);
                                                  },
                                                ),
                                              )
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Padding(
                                padding: padding22,
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      padding: inputPadding2,
                                      //height: height,
                                      decoration:BoxDecoration(
                                        color: Color.fromRGBO(24, 121, 232, 1),
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(40),
                                            bottomLeft: Radius.circular(40)
                                        ),
                                      ),
                                      child: Row(
                                        children: <Widget>[
                                          Flexible(
                                              child: SingleChildScrollView(
                                                scrollDirection: Axis.vertical,
                                                reverse: true,
                                                padding: EdgeInsets.only(top: 3,bottom: 0,left: 0),
                                                child: TextField(
                                                  textCapitalization: TextCapitalization.sentences,
                                                  textInputAction: TextInputAction.next,
                                                  focusNode: _modelfocusNode,
                                                  controller: modelController,
                                                  style: textStyle,
                                                  autocorrect: false,
                                                  enableInteractiveSelection: true,
                                                  decoration: InputDecoration(
                                                      labelText: 'Model',
                                                      labelStyle: labelHeading,
                                                      hintText: 'eg: Jaguar XJ',
                                                      hintStyle: labelHeading,
                                                      contentPadding: contentPadding2,
                                                      enabledBorder: OutlineInputBorder(
                                                        borderSide: BorderSide.none,
                                                      ),
                                                      isDense: true,
                                                      focusedBorder: OutlineInputBorder(
                                                        borderSide: BorderSide.none,
                                                      )
                                                  ),
                                                  keyboardAppearance: Brightness.dark,
                                                  keyboardType: TextInputType.text,
                                                  inputFormatters: <
                                                      TextInputFormatter>[
                                                    WhitelistingTextInputFormatter(
                                                        RegExp("[a-zA-Z0-9 \b]")),
                                                  ],

                                                  onSubmitted: (value) {
                                                    _fieldFocusChange(
                                                        context, _modelfocusNode,
                                                        _colorfocusNode);
                                                  },
                                                ),
                                              )
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),

                             /* Padding(
                                padding: padding2,
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                        padding: inputPadding,
                                        height: height,
                                        decoration: BoxDecoration(
                                          color: Color.fromRGBO(24, 121, 232, 1),
                                          borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(40),
                                              bottomLeft: Radius.circular(40)
                                          ),
                                        ),
                                        child: Container(
                                          height: height,
                                          child: Stack(
                                            fit: StackFit.loose,
                                            children: <Widget>[
                                              Align(
                                                alignment: Alignment.centerRight,
                                                child: TextField(
                                                  showCursor: false,
                                                  controller: makeController,
                                                  style: textStyle,
                                                  decoration: InputDecoration(
                                                      labelText: 'Make',
                                                      labelStyle: labelHeading,
                                                      contentPadding: contentPadding,
                                                      enabledBorder: OutlineInputBorder(
                                                        borderSide: BorderSide
                                                            .none,
                                                      ),
                                                      isDense: true,
                                                      focusedBorder: OutlineInputBorder(
                                                        borderSide: BorderSide
                                                            .none,
                                                      )
                                                  ),
                                                  keyboardAppearance: Brightness
                                                      .dark,
                                                  keyboardType: TextInputType
                                                      .text,
                                                  inputFormatters: <
                                                      TextInputFormatter>[
                                                    WhitelistingTextInputFormatter(
                                                        RegExp("[a-zA-Z\\-]")),
                                                  ],
                                                  onSubmitted: (value) {
                                                    _fieldFocusChange(
                                                        context, _makefocusNode,
                                                        _modelfocusNode);
                                                  },
                                                ),
                                              ),

                                              GestureDetector(
                                                onTap: () {
                                                  _dropMakeList(context);
                                                  //_fieldFocusChange(context,_makefocusNode,_modelfocusNode);
                                                },
                                                child: Focus(
                                                  focusNode: _makefocusNode,
                                                  child: Container(
                                                    padding: EdgeInsets.only(
                                                        right: 15, top: 5),
                                                    color: Colors.transparent,
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment
                                                          .end,
                                                      children: <Widget>[
                                                        Icon(
                                                          Icons
                                                              .keyboard_arrow_down,
                                                          color: Colors.white,
                                                          size: 25,
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              )

                                            ],
                                          ),
                                        )
                                    )
                                  ],
                                ),
                              ),
                              Padding(
                                padding: padding2,
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                        padding: inputPadding,
                                        height: height,
                                        decoration: BoxDecoration(
                                          color: Color.fromRGBO(24, 121, 232, 1),
                                          borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(40),
                                              bottomLeft: Radius.circular(40)
                                          ),
                                        ),
                                        child: Container(
                                          height: height,
                                          child: Stack(
                                            fit: StackFit.loose,
                                            children: <Widget>[
                                              Align(
                                                alignment: Alignment.centerRight,
                                                child: TextField(
                                                  controller: modelController,
                                                  style: textStyle,
                                                  showCursor: false,
                                                  decoration: InputDecoration(
                                                      focusColor: Colors
                                                          .transparent,
                                                      labelText: 'Model',
                                                      labelStyle: labelHeading,
                                                      contentPadding: contentPadding,
                                                      enabledBorder: OutlineInputBorder(
                                                        borderSide: BorderSide
                                                            .none,
                                                      ),
                                                      isDense: true,
                                                      focusedBorder: OutlineInputBorder(
                                                        borderSide: BorderSide
                                                            .none,
                                                      )
                                                  ),
                                                  keyboardAppearance: Brightness
                                                      .dark,
                                                  keyboardType: TextInputType
                                                      .text,
                                                  inputFormatters: <
                                                      TextInputFormatter>[
                                                    WhitelistingTextInputFormatter(
                                                        RegExp("[a-zA-Z\\-]")),
                                                  ],

                                                  onSubmitted: (value) {
                                                    _fieldFocusChange(
                                                        context, _modelfocusNode,
                                                        _colorfocusNode);
                                                  },
                                                ),
                                              ),

                                              GestureDetector(
                                                onTap: () {
                                                  _dropModelList(context);
                                                  // _fieldFocusChange(context,_modelfocusNode,_colorfocusNode);
                                                },
                                                child: Focus(
                                                  focusNode: _modelfocusNode,
                                                  child: Container(
                                                    padding: EdgeInsets.only(
                                                        right: 15, top: 5),
                                                    color: Colors.transparent,
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment
                                                          .end,
                                                      children: <Widget>[
                                                        Icon(
                                                          Icons
                                                              .keyboard_arrow_down,
                                                          color: Colors.white,
                                                          size: 25,
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        )
                                    )
                                  ],
                                ),
                              ),*/

                              Padding(
                                padding: padding22,
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      padding: inputPadding2,
                                      //height: height,
                                      decoration:BoxDecoration(
                                        color: Color.fromRGBO(24, 121, 232, 1),
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(40),
                                            bottomLeft: Radius.circular(40)
                                        ),
                                      ),
                                      child: Row(
                                        children: <Widget>[
                                          Flexible(
                                              child: SingleChildScrollView(
                                                scrollDirection: Axis.vertical,
                                                reverse: true,
                                                padding: EdgeInsets.only(top: 3,bottom: 0,left: 0),
                                                child: TextField(
                                                  textCapitalization: TextCapitalization.sentences,
                                                  textInputAction: TextInputAction.done,
                                                  focusNode: _colorfocusNode,
                                                  controller: colorController,
                                                  style: textStyle,
                                                  autocorrect: false,
                                                  enableInteractiveSelection: true,
                                                  decoration: InputDecoration(
                                                      labelText: 'Color',
                                                      labelStyle: labelHeading,
                                                      contentPadding: contentPadding2,
                                                      enabledBorder: OutlineInputBorder(
                                                        borderSide: BorderSide.none,
                                                      ),
                                                      isDense: true,
                                                      focusedBorder: OutlineInputBorder(
                                                        borderSide: BorderSide.none,
                                                      )
                                                  ),
                                                  keyboardAppearance: Brightness.dark,
                                                  keyboardType: TextInputType.text,
                                                  inputFormatters: <
                                                      TextInputFormatter>[
                                                    WhitelistingTextInputFormatter(
                                                        RegExp("[a-zA-Z0-9 \b]")),
                                                  ],
                                                ),
                                              )
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),

                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          bottomNavigationBar: Container(
            padding:
                io.Platform.isIOS
                  ? EdgeInsets.only(right: 15, left: 10 , bottom: 20, top: 20)
                  : EdgeInsets.only(right: 15, left: 10),
            color: Colors.white,
            height: io.Platform.isIOS ? 90 : 80,
            width: MediaQuery
                .of(context)
                .size
                .width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        Navigator.popUntil( context, ModalRoute.withName('/login'));
                      },
                      child: Container(
                        width: 100,
                        padding: EdgeInsets.only(
                            top: 10, bottom: 10, left: 15),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                                Radius.circular(50))
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment
                              .spaceBetween,
                          children: <Widget>[
                            Icon(Icons.arrow_back,
                              color: Color.fromRGBO(28, 129, 245, 1),),
                            Text( " Sign In ", style: signInButton ),
                          ],
                        ),
                      ),
                    ),
                    GestureDetector(
                      behavior: HitTestBehavior.opaque,
                      onTap: () async{
                        FocusScope.of(context).requestFocus(new FocusNode());
                        InternetConnectivity _internetConnectivity = InternetConnectivity();
                        var cnChk = await _internetConnectivity.checkConnectivity();
                        if(cnChk){
                          clickFunction();
                        }else{
                          _flushPopUp(
                              message: Common.internetErrorMessage,
                              bgColor: Color.fromRGBO(178,0,0,1),
                              duration: Duration(milliseconds: 2500)
                          );
                        }
                      },
                      child: Container(
                        width: 170,
                        padding: EdgeInsets.only(
                            top: 10, bottom: 10, right: 10, left: 15),
                        decoration: BoxDecoration(
                            color: Color.fromRGBO(255, 165, 0, 1),
                            borderRadius: BorderRadius.all(
                                Radius.circular(50))
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment
                                  .spaceBetween,
                              children: <Widget>[
                                Expanded(child: Text(
                                  "Register Now ", style: registerButton,
                                  textAlign: TextAlign.center,)),
                                Icon(Icons.arrow_forward,
                                  color: Colors.white,)
                              ],
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    BackButtonInterceptor.remove(myInterceptor);
    super.dispose();
  }

  _thankYouBottomSheet() {
    showwDialog(
        context: context,
        builder: (context) =>
        WillPopScope(
          onWillPop: () {
            Navigator.pushReplacement( context, MaterialPageRoute(  builder: (context) => Login()) ,result: true);
            /*return new Future.value(
                Navigator.pushReplacement( context, MaterialPageRoute(  builder: (context) => Login()) ,result: true)
            );*/

          },
          child: SafeArea(
            top: false,
            bottom: false,
            child: Scaffold(
              //appBar: AppBar(backgroundColor: Colors.red,),
              backgroundColor: Color.fromRGBO(24, 121, 232, 0.5),
              bottomNavigationBar: GestureDetector(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: Container(
                  width: MediaQuery
                      .of(context)
                      .size
                      .width,
                  height: MediaQuery
                      .of(context)
                      .size
                      .height / 2,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30)
                      )
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Flexible(
                              child: Image.asset(
                                'assets/check_registration_popup.png',
                                width: 100,
                                height: 100,
                              ),
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'Thank You',
                              style: signInBottomThankYou,
                              textAlign: TextAlign.center,
                              softWrap: true,
                              maxLines: 3,
                              //textScaleFactor: 1.5,
                            )
                          ],
                        ),
                      ),
                      Container(
                        width: 320,
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  '${Common.registrationCompletMessage}',
                                  style: signInBottomThankYouSubHeading,
                                  textAlign: TextAlign.center,
                                  softWrap: true,
                                  maxLines: 2,
                                  //textScaleFactor: 1.5,
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5),
                        child: Container(
                          decoration: BoxDecoration(
                              color: Color.fromRGBO(255, 165, 0, 1),
                              borderRadius: BorderRadius.all(
                                  Radius.circular(50))
                          ),
                          width: 200,
                          height: 50,

                          child: FlatButton(
                            shape: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.all(
                                    Radius.circular(50))
                            ),
                            child: Text(
                              'OK',
                              textAlign: TextAlign.center,
                              style: signInBottomThankYouButtonOk,),
                            onPressed: () {
                              Navigator.pushReplacement( context, MaterialPageRoute(  builder: (context) => Login() ) );
                            },
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        )
    );
  }

  _flushPopUp({String message ,Color bgColor, Duration duration}){
    Flushbar(
      message:  "${message}",
      flushbarPosition: FlushbarPosition.TOP,
      flushbarStyle: FlushbarStyle.FLOATING,
      borderRadius: 10,
      backgroundColor: bgColor,
      margin: EdgeInsets.all(10),
      duration: duration,
    ).show(context);

  }

  /*_dropMakeList(BuildContext context) {
    List<String> make = [
      "Hundai", "Honda", "Maruti", "Suzuki", "Hero",
      "Hundai", "Honda", "Maruti", "Suzuki", "Hero",
      "Hundai", "Honda", "Maruti", "Suzuki", "Hero"
    ];


    showModalBottomSheet(
        elevation: 0,
        context: context,
        builder: (BuildContext context) {
          return Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(30),
                      topLeft: Radius.circular(30)
                  )
              ),
              //padding: EdgeInsets.symmetric(horizontal: 15,),
              child: Stack(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 60, left: 25, right: 25, bottom: 20),
                    child: ListView.builder(
                        itemCount: make.length,
                        itemBuilder: (context, index) {
                          return Container(
                            margin: EdgeInsets.symmetric(horizontal: 10),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      makeController.text = make[index];
                                    });
                                    Navigator.pop(context);
                                  },
                                  child: Container(
                                    margin: EdgeInsets.symmetric(vertical: 5),
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: Text(
                                                make[index],
                                                style: TextStyle(
                                                    fontSize: 15,
                                                    fontWeight: FontWeight.w500,
                                                    fontFamily: 'AdelleRegular'
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Divider(
                                  thickness: 0.5,
                                  color: Color.fromRGBO(194, 194, 194, 1),
                                )
                              ],
                            ),
                          );
                        }),
                  ),
                  Container(
                    height: 60,
                    padding: EdgeInsets.symmetric(vertical: 10,),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(30),
                            topLeft: Radius.circular(30)
                        )
                    ),

                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Choose your make",
                          style: TextStyle(
                              color: Colors.blue,
                              fontSize: 18,
                              fontWeight: FontWeight.w800,
                              fontFamily: 'AdelleRegular'
                          ),
                        )
                      ],
                    ),
                  )
                ],
              )
          );
        },
        backgroundColor: Colors.transparent
    );
  }

  _dropModelList(BuildContext context) {
    List<String> model = [
      "Verna", "i10", "i20", "Weganar", "Amage",
      "Maruti800", "Oddy", "Ferari", "Suzuki", "Hero",
      "Hundai", "Honda", "Maruti", "Suzuki", "Hero"
    ];


    showModalBottomSheet(
        elevation: 0,
        context: context,
        builder: (BuildContext context) {
          return Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(30),
                      topLeft: Radius.circular(30)
                  )
              ),
              //padding: EdgeInsets.symmetric(horizontal: 15,),
              child: Stack(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 60, left: 25, right: 25, bottom: 20),
                    child: ListView.builder(
                        itemCount: model.length,
                        itemBuilder: (context, index) {
                          return Container(
                            margin: EdgeInsets.symmetric(horizontal: 10),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      modelController.text = model[index];
                                    });
                                    Navigator.pop(context);
                                  },
                                  child: Container(
                                    margin: EdgeInsets.symmetric(vertical: 5),
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: Text(
                                                model[index],
                                                style: TextStyle(
                                                    fontSize: 15,
                                                    fontWeight: FontWeight.w500,
                                                    fontFamily: 'AdelleRegular'
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Divider(
                                  thickness: 0.5,
                                  color: Color.fromRGBO(194, 194, 194, 1),
                                )
                              ],
                            ),
                          );
                        }),
                  ),
                  Container(
                    height: 60,
                    padding: EdgeInsets.symmetric(vertical: 10,),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(30),
                            topLeft: Radius.circular(30)
                        )
                    ),

                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "Choose your model",
                              style: TextStyle(
                                  color: Colors.blue,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w800,
                                  fontFamily: 'AdelleRegular'
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              )
          );
        },
        backgroundColor: Colors.transparent
    );
  }*/

  Future signInfoMethod2() async {
    try{

    HttpService _httpService = HttpService();

    var signInData = await _httpService.signindata(
        fname: fnameController.text,
        lname: lnameController.text,
        carNo: carNoController.text,
        email: emailController.text,
        password: pass1Controller.text,
        password2: pass2Controller.text,
        makeName: makeController.text,
        model: modelController.text,
        color: colorController.text );

          if(signInData.statusCode == 200){
            var jsonData = SignInModel.fromJson2(jsonDecode(signInData.body));
            if (jsonData.status == 0) {
              _messageBottomSheet(errrorMessage: jsonData.message);
             // return jsonData;
            } else {
              clearfield();
              _thankYouBottomSheet();
              //return jsonData;
            }
          }else if(signInData.statusCode == 401){
            // login
          }else{
            _messageBottomSheet(errrorMessage: Common.serverError);
          }

     } catch(e){
     print(e.toString());
     }
  }

  clickFunction() async {
    bool chk = validateField();
    if (chk) {
      await signInfoMethod2();
    }
  }

  validateField() {
    if (fnameController.text == "") {
      _messageBottomSheet(errrorMessage: '${Common.enterNameString}');
      return false;
    } else if (fnameController.text.trim().length == 0) {
      _messageBottomSheet(errrorMessage: '${Common.enterNameString}');
      return false;
    } /*else if (lnameController.text == "") {
      _messageBottomSheet(errrorMessage: "Please enter Last name");
      return false;
    } else if (carNoController.text == "") {
      _messageBottomSheet(errrorMessage: "Please enter Car number");
      return false;
    }*/ else if (emailController.text == "") {
      _messageBottomSheet(errrorMessage: '${Common.enterEmailString}');
      return false;
    }else if (emailController.text.trim().length == 0) {
      _messageBottomSheet(errrorMessage: '${Common.enterEmailString}');
      return false;
    }  else if (!validator.email(emailController.text)) {
      _messageBottomSheet(errrorMessage: '${Common.enterValidEmailString}');
      return false;
    }else if (pass1Controller.text == "") {
      _messageBottomSheet(errrorMessage: '${Common.enterPasswordString}');
      return false;
    }else if (pass1Controller.text.trim().length == 0) {
      _messageBottomSheet(errrorMessage: '${Common.enterPasswordString}');
      return false;
    } else if (pass1Controller.text.length < Common.minLenPass) {
      _messageBottomSheet(errrorMessage: '${Common.minLenPassMessage}');
      return false;
    } else if (pass2Controller.text == "") {
      _messageBottomSheet(errrorMessage: '${Common.enterConfirmPasswordString}');
      return false;
    } else if (pass2Controller.text.trim().length == 0) {
      _messageBottomSheet(errrorMessage: '${Common.enterConfirmPasswordString}');
      return false;
    } else if (pass2Controller.text.length < Common.minLenPass) {
      _messageBottomSheet(errrorMessage: '${Common.minLenPassMessage}');
      return false;
    } else if (pass1Controller.text != pass2Controller.text) {
      _messageBottomSheet(errrorMessage: '${Common.enterPasswordNotMatchedString}');
      return false;
    } else if (makeController.text == "") {
      _messageBottomSheet(errrorMessage: '${Common.enterMakeString}');
      return false;
    } else if (makeController.text.trim().length == 0) {
      _messageBottomSheet(errrorMessage: '${Common.enterMakeString}');
      return false;
    } else if (modelController.text == "") {
      _messageBottomSheet(errrorMessage: '${Common.enterModelString}');
      return false;
    } else if (modelController.text.trim().length == 0) {
      _messageBottomSheet(errrorMessage: '${Common.enterModelString}');
      return false;
    } else if (colorController.text == "") {
      _messageBottomSheet(errrorMessage: '${Common.enterColorString}');
      return false;
    } else if (colorController.text.trim().length == 0) {
      _messageBottomSheet(errrorMessage: '${Common.enterColorString}');
      return false;
    } else {
      return true;
    }
  }

  _messageBottomSheet({String errrorMessage}){
   Flushbar(
      message:  "$errrorMessage",
      flushbarPosition: FlushbarPosition.TOP,
      flushbarStyle: FlushbarStyle.FLOATING,
      borderRadius: 10,
      margin: EdgeInsets.all(10),
     duration:  Duration(milliseconds: 1000),
    )..show(context);

  }

  clearfield() {
    fnameController.clear();
    lnameController.clear();
    carNoController.clear();
    emailController.clear();
    pass1Controller.clear();
    pass2Controller.clear();
    modelController.clear();
    makeController.clear();
    colorController.clear();
  }
}