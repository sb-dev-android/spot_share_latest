import 'package:flutter/material.dart';
import 'package:peer_to_peer/stylesheet.dart';
import 'package:flutter/services.dart';
import 'package:peer_to_peer/history.dart';
import 'package:peer_to_peer/swapRequest.dart';
import 'package:peer_to_peer/home.dart';
import 'package:peer_to_peer/Location.dart';
import 'package:peer_to_peer/Login.dart';
import 'package:peer_to_peer/setting.dart';
import 'package:peer_to_peer/successfull_match.dart';
import 'package:latlong/latlong.dart';
import 'package:geocoder/geocoder.dart';
import 'package:peer_to_peer/designModelClass/podoclasses/models.dart';
import 'package:peer_to_peer/designModelClass/podoclasses/httpservices.dart';
import 'package:peer_to_peer/designModelClass/podoclasses/helper.dart';
import 'package:peer_to_peer/designModelClass/podoclasses/common.dart';
import 'dart:convert';
import 'designModelClass/customAppbar.dart' as apbar;
import 'package:flushbar/flushbar.dart';
import 'package:peer_to_peer/profileScreen.dart';
import 'dart:async';

class SavedLocation extends StatelessWidget {
  final bool status;
  SavedLocation(this.status);
  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      routes: <String, WidgetBuilder> {
        '/login': (BuildContext context) => new Login(),
        '/home': (BuildContext context) => new Home(status),
        '/location': (BuildContext context) => new Location(status),
      },
      debugShowCheckedModeBanner: false,
      home: SaveLocationDrawerAnimation(status),
    );
  }
}


class SaveLocationDrawerAnimation extends StatefulWidget {
  final bool status;
  SaveLocationDrawerAnimation(this.status);
  @override
  _SaveLocationDrawerAnimationState createState() => _SaveLocationDrawerAnimationState();
}

class _SaveLocationDrawerAnimationState extends State<SaveLocationDrawerAnimation> {

  bool isCollapsed = true;
  double screenWidth;
  double screenHeight;
  Duration _duration = const Duration(milliseconds: 300);

  Helper helper = Helper();


  @override
  Widget build(BuildContext context) {

    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarIconBrightness:Brightness.dark,
        statusBarColor: Colors.transparent,
      ),
    );

    Size size = MediaQuery.of(context).size;
    screenHeight = size.height;
    screenWidth = size.width;

    return SafeArea(
      top: false,
      bottom: false,
      child: Scaffold(
        body: Stack(fit: StackFit.expand,
          children: <Widget>[
            menu(context),
            dashBoard(context),
          ],
        ),
      ),
    );
  }

  Widget menu(context){

    var listTilePadding = EdgeInsets.only(
        bottom: (MediaQuery.of(context).size.width * 0.1) - 30,
        right: 25
    );

    return SafeArea(
      top: false,
      child: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
                top: MediaQuery.of(context).size.height * 0.1 + 20
            ),
            child: Container(
              padding: EdgeInsets.only(left: 15,right: 60),
              child: ListView(
                shrinkWrap: true,
                children: <Widget>[
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("Home",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage(  "assets/icn_home.png" ),
                        color: Colors.blueAccent,

                      ),
                      onTap: (){
                        Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context)=>Home(widget.status)
                            )
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("Swap Request",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage(  "assets/icn_swap_request.png",  ),
                        color: Colors.blueAccent,

                      ),
                      onTap: (){
                        Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context)=>SwapRequest(widget.status)
                            )
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("History",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage(  "assets/icn_history.png", ),
                        color: Colors.blueAccent,

                      ),
                      onTap: (){
                        Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context)=>History(widget.status)
                            )
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("Saved Location",style: widget.status? activeSideMenuText: sideMenuText,),
                      trailing: Image(
                        image: AssetImage(  "assets/icn_saved_location.png" ),
                        color: Colors.blueAccent,
                      ),
                      onTap: (){
                        Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context)=>SavedLocation(widget.status)
                            )
                        );

                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("Settings",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage(  "assets/icn_settings.png" ),
                        color: Colors.blueAccent,
                      ),
                      onTap: (){
                        Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context)=>Setting(widget.status)
                            )
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("About Us",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage(  "assets/icn_about_us.png" ),
                        color: Colors.blueAccent,
                      ),
                      onTap: (){  },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("Contact Us",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage(  "assets/icn_contact_us.png",  ),
                        color: Colors.blueAccent,
                      ),
                      onTap: (){

                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("Rate Us",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage(  "assets/icn_rate_us.png", ),
                        color: Colors.blueAccent,
                      ),
                      onTap: (){
                        rateUs(context);
                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("Sign Out",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage(  "assets/icn_signout.png", ),
                        color: Colors.blueAccent,
                      ),
                      onTap: (){
                        Navigator.of(context).pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
                       // Navigator.pushNamedAndRemoveUntil(context,'/login', ModalRoute.withName('/home'));
                        helper.clearOauth();
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 15,right: 75,),
            margin: EdgeInsets.only(top: 20),
            height: 80,
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border(
                    bottom: BorderSide(
                      color: Colors.black.withOpacity(0.5),
                      width: 0.5,
                    )
                )
            ),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment:  MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "${Common.profileCarUser}",
                      style: textProfileNameStyle,
                    ),
                    IconButton(
                      icon: Icon(Icons.arrow_forward),
                      onPressed: (){
                        Navigator.push(context, MaterialPageRoute(
                            builder: (context)=>ProfileScreen()
                        ));
                      },
                    )
                  ],
                ),
                Row(
                  children: <Widget>[
                    Text(
                      "${Common.profileEmail}",
                      style: textProfileEmailStyle,
                    )
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget dashBoard(context){

    return AnimatedPositioned(
      top: isCollapsed ? 0: 0.0 * screenHeight,
      bottom: isCollapsed ? 0 : 0.0 * screenWidth,
      right: isCollapsed ? 0 : -4 * screenWidth,
      left: isCollapsed ? 0 : 0.8 * screenWidth,

      duration: _duration,

      child: Material(
        elevation: 8,
        child: Scaffold(
          appBar: apbar.AppBar(
            elevation: Common.elevation,
            backgroundColor: Colors.white,
            centerTitle: true,
            leading: InkWell(
              child: Image.asset("assets/icn_menu_home.png"),
              onTap: (){

                if(isCollapsed){
                  setState(() {
                    isCollapsed =false;
                  });
                }else{
                  setState(() {
                    isCollapsed=true;
                  });
                }

              },
            ),
            title: Text("Saved Location", style: appbarHeading,),
          ),
          body: RefreshIndicator(
            child: Container(
                child: _futureList(),
            ),
            onRefresh: () async{
              initState();
            },
          ),
        ),
      ),
    );
  }

  Future<Null> _onRefresh() {
    Completer<Null> completer = new Completer<Null>();
    Timer timer = new Timer(new Duration(seconds: 3), () {
      completer.complete();
    });
    return completer.future;
  }

  Widget _futureList(){
    return  FutureBuilder(
      future: _bookmarkmethod(),
      builder: (BuildContext context,AsyncSnapshot snapshot){
        if(snapshot.hasData){
          if(snapshot.data.length == 0){
            return Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      RefreshIndicator(
                        child: Container(
                          height: MediaQuery.of(context).size.height * 0.7 ,
                          width:MediaQuery.of(context).size.width * 0.9,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Image(
                                image: AssetImage(
                                  "assets/icn_no_request_available.png",
                                ),
                                width:MediaQuery.of(context).size.width * 0.9,
                              ),
                            ],
                          ),
                        ),
                        onRefresh: () async{
                          initState();
                        },
                      ),
                    ],
                  ),
                  /*Center(
                                  child: Text(
                                    "No request found"
                                  ),
                                ),*/
                ],
              ),
            );
          }else{
            return RefreshIndicator(
              child: ListView.builder(
                  itemCount: [...snapshot.data].length,
                  itemBuilder: (BuildContext context,int index)  {
                    return  Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 10,
                              bottom: 10,
                              left: 15,
                              right: 15
                          ),
                          child: GestureDetector(
                            onTap: () async {
                              await navigateMethod(
                                  address: snapshot.data[index].address,
                                  bookmarkId: snapshot.data[index].bookmarkId
                              );
                            },
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              //height: 170,
                              decoration: BoxDecoration(
                                  color: Color.fromRGBO(238, 241, 242, 1),
                                  borderRadius: BorderRadius.all(Radius.circular(30))
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.only(left: 20,top: 20,bottom: 5),
                                    child: Column(
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.only(left:4.0),
                                          child: Row(
                                            children: <Widget>[
                                              Text("Location",style: listViewLabelText,),
                                            ],
                                          ),
                                        ),
                                        SizedBox(height: 5,),
                                        Row(
                                          children: <Widget>[
                                            Container(
                                              child: Text(
                                                snapshot.data[index].locality,
                                                style: listViewCarNo,
                                              ),
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                  Divider(color: Colors.grey,indent: 10,endIndent: 10,),
                                  Container(
                                    height: 70,
                                    child: Stack(
                                      children: <Widget>[
                                        Align(
                                          alignment:Alignment.bottomRight,

                                          child: Image.asset(
                                            'assets/list_arrow_next.png',
                                            width: 50,
                                            height: 50,
                                          ),
                                        ),
                                        Container(
                                          width: (MediaQuery.of(context).size.width * 0.8) - 15,
                                          padding:EdgeInsets.only(bottom: 20,left: 20),
                                          child: Text(
                                            snapshot.data[index].address,
                                            softWrap: true,
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                            style: savedLocationViewText,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    );
                  }
              ),
              onRefresh: _onRefresh,
            );
          }

        }
        else{
          return Container(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      height: MediaQuery.of(context).size.height * 0.7 ,
                      width:MediaQuery.of(context).size.width * 0.9,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Image(
                            image: AssetImage(
                              "assets/icn_no_request_available.png",
                            ),
                            width:MediaQuery.of(context).size.width * 0.9,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                /*Center(
                                  child: Text(
                                    "No request found"
                                  ),
                                ),*/
              ],
            ),
          );
        }
      },
    );
  }

  _messageBottomSheet({String errrorMessage}){

    Flushbar(
      message:  "$errrorMessage",
      flushbarPosition: FlushbarPosition.TOP,
      flushbarStyle: FlushbarStyle.FLOATING,
      borderRadius: 10,
      margin: EdgeInsets.all(10),
      duration:  Duration(milliseconds: 1000),
    )..show(context);

  }

  Future<List<BookmarkModel>> _bookmarkmethod() async {

    String oauth = await helper.getOauth();
    List<BookmarkModel> bookmarklist = <BookmarkModel>[];
    var jsonData;
    try {
      HttpService _httpService = HttpService();

      var bookmarkdata = await _httpService.bookmark(token:  oauth);

      if(bookmarkdata.statusCode == 200){
        jsonData = await json.decode(bookmarkdata.body);
        for(var i in jsonData["data"]){
          var locality = await getLocality(latLng: LatLng(i["latitude"],i["longitude"]));
          BookmarkModel dt = BookmarkModel(
              bookmarkId: i["_id"],
              status: jsonData["status"],
              message: jsonData["message"],
              address: i["address"],
              longitude: i["longitude"],
              latitude: i["latitude"],
              locality: locality  );

          bookmarklist.add(dt);
        }

        return bookmarklist;
      }else if(bookmarkdata.statusCode == 401){

        //_messageBottomSheet(errrorMessage: Common.authInvalid);
        Navigator.pushReplacement(context, MaterialPageRoute(
          builder: (context) => Login()
        ));
      }else{
        _messageBottomSheet(errrorMessage: Common.serverError);
      }

    }catch(e){
      print( e.toString());
    }
  }

  getLocality({LatLng latLng}) async {
    final coordinates = new Coordinates( latLng.latitude, latLng.longitude );
    var addresses = await Geocoder.local.findAddressesFromCoordinates(
        coordinates);
    var first = addresses.first;
    return first.locality;
  }

  navigateMethod({var address, var bookmarkId}) async {
    Navigator.push(context, MaterialPageRoute(
        builder: (context) => Location(
          widget.status,
          savedLocationDestination: address,//snapshot.data[index].address,
          chkBkmark: true,
          bookmarkId: bookmarkId //snapshot.data[index].bookmarkId,
        )
    ));
  }
}

