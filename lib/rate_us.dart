import 'package:flutter/material.dart';
import 'package:peer_to_peer/designModelClass/podoclasses/common.dart';
import 'package:peer_to_peer/stylesheet.dart';
import 'package:flutter/services.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class RateUs extends StatefulWidget {
  @override
  _RateUsState createState() => _RateUsState();
}

class _RateUsState extends State<RateUs> {

  @override
  void initState() {
    SystemChrome.setEnabledSystemUIOverlays([]);
    // TODO: implement initState
    super.initState();
  }
  var rating = 0.0;
  @override
  Widget build(BuildContext context) {
    /*SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarIconBrightness:Brightness.dark,
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        //systemNavigationBarDividerColor: Colors.black
      ),
    );*/
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Container(
        color: Colors.transparent,
        child: SafeArea(
         // top: false,
         // bottom: false,
          child: Scaffold(
            backgroundColor: Colors.transparent,
            bottomNavigationBar: Container(
              height: 330,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(30),
                      topLeft: Radius.circular(30)
                  )
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Rate Us',
                          style: signInBottomThankYou,
                          textAlign: TextAlign.center,
                          softWrap: true,
                          maxLines: 3,
                          //textScaleFactor: 1.5,
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(5),
                    child: Container(
                      width: 250,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            child: Text(
                              '${Common.rateUsMessage}',
                              style: signInBottomThankYouSubHeading,
                              textAlign: TextAlign.center,
                              softWrap: true,
                              maxLines: 3,
                              //textScaleFactor: 1.5,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SmoothStarRating(
                          allowHalfRating: false,
                          onRatingChanged: (v) {
                            rating = v;
                            setState(() {});
                          },
                          starCount: 5,
                          rating: rating,
                          size: 40.0,
                          filledIconData: Icons.star,
                          halfFilledIconData: Icons.blur_on,
                          //247, 255, 0, 1
                          color: Color.fromRGBO(220,237,0,1),
                          borderColor: Color.fromRGBO(226,226,226,1),
                          spacing:5.0
                      )
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(5),
                          child: Container(
                            decoration: BoxDecoration(
                                color: Color.fromRGBO(255, 165, 0, 1),
                                borderRadius: BorderRadius.all(Radius.circular(50))
                            ),
                            width: 150,
                            height: 50,

                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                GestureDetector(
                                  child: Container(
                                    width: 120,
                                    decoration: BoxDecoration(
                                        border: Border.all(color: Color.fromRGBO(255,255,255,0.1)),
                                        borderRadius: BorderRadius.all(Radius.circular(30))
                                    ),
                                    //padding: EdgeInsets.only(right: 15,left: 20,top: 10,bottom: 10),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Expanded(
                                          child: Text(
                                            "Submit",
                                            style: signInBottomThankYouButtonOk,
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  onTap: (){
                                    Navigator.pop(context);
                                  },
                                ),

                              ],
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(5),
                          child: Container(
                            decoration: BoxDecoration(
                                color: Colors.white,
                                //color: Color.fromRGBO(255, 165, 0, 1),
                                borderRadius: BorderRadius.all(Radius.circular(50)),
                                border: Border.all(
                                    color: Color.fromRGBO(255, 165, 0, 1),
                                    width: 0.5
                                )
                            ),
                            width: 150,
                            height: 50,

                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                GestureDetector(
                                  child: Container(
                                    width: 120,
                                    decoration: BoxDecoration(
                                        border: Border.all(color: Color.fromRGBO(255,255,255,0.1)),
                                        borderRadius: BorderRadius.all(Radius.circular(30))
                                    ),
                                    //padding: EdgeInsets.only(right: 15,left: 20,top: 10,bottom: 10),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        Expanded(
                                          child: Text(
                                            "Not Now",
                                            style: signInBottomThankYouButtonNo,
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  onTap: () async{
                                    FlutterLocalNotificationsPlugin notificationsPlugin = FlutterLocalNotificationsPlugin();
                                    //showNotification(notificationsPlugin,title: "bfghrhf",body: "fhhdbfhf",id: 0);
                                    Navigator.pop(context);
                                  },
                                ),

                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  notificationcheckmethod() async{
    FlutterLocalNotificationsPlugin notificationsPlugin = FlutterLocalNotificationsPlugin();
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'your channel id', 'your channel name', 'your channel description',
        importance: Importance.Max, priority: Priority.High, ticker: 'ticker');
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await notificationsPlugin.show(
        0, 'plain title', 'plain body', platformChannelSpecifics,
        payload: 'item x');

    await notificationsPlugin.show(
        0, 'plain title', 'plain body', platformChannelSpecifics);



  }


  NotificationDetails get _ongoing{
    final androidChannelSpecifies = AndroidNotificationDetails(
        'id',
        'name',
        'description',
        importance: Importance.Max,
        priority: Priority.High,
        ongoing: true,
        autoCancel: false
    );
    final iosChannel = IOSNotificationDetails();
    return NotificationDetails(androidChannelSpecifies,iosChannel);
  }


  Future showOnGoingNotification(FlutterLocalNotificationsPlugin notification,{
    @required String title,
    @required String body,
    int id = 0
  }) => showNotification(notification, title: title,body: body,id: id, type: _ongoing);


  Future showNotification(FlutterLocalNotificationsPlugin notification,{
    @required String title,
    @required String body,
    @required NotificationDetails type,
    int id = 0
  })=>notification.show(id, title, body, type);



  @override
  void dispose() {
    // TODO: implement dispose
    //SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    super.dispose();
  }
}
