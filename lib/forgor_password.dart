import 'package:flutter/material.dart';
import 'package:peer_to_peer/designModelClass/podoclasses/common.dart';
import 'stylesheet.dart';
import 'package:flutter/services.dart';
import 'package:peer_to_peer/Login.dart';
import 'package:flushbar/flushbar.dart';
import 'package:regexed_validator/regexed_validator.dart';
import 'designModelClass/podoclasses/internet_connectivity.dart';
import 'dart:convert';
import 'package:peer_to_peer/designModelClass/podoclasses/httpservices.dart';
import 'package:peer_to_peer/designModelClass/podoclasses/models.dart';
import 'package:peer_to_peer/Login.dart';



class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {

  TextEditingController emailController = new TextEditingController();

  EdgeInsets inputPadding2 = EdgeInsets.only(top: 8, bottom: 5, left: 5,);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: <String, WidgetBuilder>{
        '/login': (BuildContext context) => new Login(),
      },
      debugShowCheckedModeBanner: false,
      home: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: GestureDetector(
            onTap: (){
              Navigator.pop(context);
            },
            child: Container(
              color: Colors.transparent,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      FocusScope.of(context).requestFocus(new FocusNode());
                    },
                    child: SingleChildScrollView(
                      child: Container(
                        margin: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(bottom: 20,top:15,right: 5,left: 5),
                              decoration:BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.all(Radius.circular(20),),
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.all(20),
                                    child: Column(
                                      children: <Widget>[
                                        Container(
                                          decoration:BoxDecoration(
                                            border: Border(bottom: BorderSide(
                                                color: Colors.green,
                                                width: 0.5
                                            ))
                                          ),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: <Widget>[
                                              Text(
                                                '${Common.forgotPassword}',
                                                style: forgotPasswordHeading,

                                              )
                                            ],
                                          ),
                                        ),
                                        Container(
                                         // padding: const EdgeInsets.all(20),
                                          margin: EdgeInsets.all(10),
                                          child: Text(
                                              '${Common.pleaseEnterEmail}',
                                            style: forgotPasswordLabel,
                                            textAlign: TextAlign.center,
                                            maxLines: 3,
                                          ),
                                        ),

                                        Container(
                                          padding: inputPadding2,
                                          margin: EdgeInsets.only(bottom: 15,top:15),
                                          //height: 60,
                                          decoration:BoxDecoration(
                                            color: Color.fromRGBO(245, 245, 245, 1),
                                            borderRadius: BorderRadius.all(Radius.circular(40)),
                                          ),
                                          child: Row(
                                            children: <Widget>[
                                              Flexible(
                                                child: SingleChildScrollView(
                                                  scrollDirection: Axis.vertical,
                                                  padding: EdgeInsets.only(top: 3,bottom: 1),
                                                  reverse: true,
                                                  child: TextField(
                                                    enableSuggestions: false,
                                                    textInputAction: TextInputAction.done,
                                                    keyboardType: TextInputType.emailAddress,
                                                    controller: emailController,
                                                    autocorrect: false,
                                                    enableInteractiveSelection: true,
                                                    keyboardAppearance: Brightness.dark,
                                                    style: loginInputTxtSize,
                                                    decoration: InputDecoration(
                                                        hintText: 'Enter email',
                                                        hintStyle: loginPlaceHolder,
                                                        contentPadding: EdgeInsets.only(left: 11,right: 20,top: 8,bottom: 8),
                                                        enabledBorder: OutlineInputBorder(
                                                          borderSide: BorderSide.none,
                                                        ),
                                                        isDense: true,
                                                        focusedBorder:  OutlineInputBorder(
                                                          borderSide: BorderSide.none,
                                                        )
                                                    ),
                                                    inputFormatters: <TextInputFormatter>[
                                                      WhitelistingTextInputFormatter(RegExp("[a-zA-Z0-9.@\b]")),
                                                    ],

                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),

                                  Container(
                                    width: MediaQuery
                                        .of(context)
                                        .size
                                        .width,
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[

                                            GestureDetector(
                                              onTap: () async {

                                                InternetConnectivity _internetConnectivity = InternetConnectivity();

                                                var cnChk = await _internetConnectivity.checkConnectivity();

                                                if(cnChk){
                                                  var chk  = validateField();
                                                  if(chk){
                                                     await forgotPasswordApi(email: emailController.text.toString());
                                                  }
                                                }else{
                                                  _flushPopUp(
                                                      message: Common.internetErrorMessage,
                                                      bgColor: Color.fromRGBO(178,0,0,1),
                                                      duration: Duration(milliseconds: 2500)
                                                  );
                                                }

                                              },
                                              child: Container(
                                                width: MediaQuery.of(context).size.width -100,

                                                padding: EdgeInsets.all(13),

                                                //padding: EdgeInsets.only(top: 10, bottom: 10, right: 15, left: 15),
                                                decoration: BoxDecoration(
                                                    color: Color.fromRGBO(255, 165, 0, 1),
                                                    borderRadius: BorderRadius.all(
                                                        Radius.circular(50))
                                                ),
                                                child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    Row(
                                                      mainAxisAlignment: MainAxisAlignment
                                                          .spaceBetween,
                                                      children: <Widget>[
                                                        Expanded(child: Text(
                                                          "${Common.submitString}",
                                                          style: registerButton,
                                                          textAlign: TextAlign.center,)),

                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            )
                                          ],
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }


  _flushPopUp({String message ,Color bgColor, Duration duration}){
    Flushbar(
      message:  "${message}",
      flushbarPosition: FlushbarPosition.TOP,
      flushbarStyle: FlushbarStyle.FLOATING,
      borderRadius: 10,
      backgroundColor: bgColor,
      margin: EdgeInsets.all(10),
      duration: duration,
    ).show(context);

  }

_showLinkMessageBottomSheet() {
    Navigator.pop(context);
    popupAnimation(child: GestureDetector(
        onTap: (){
          Navigator.pop(context);
          Navigator.pushReplacement(context, MaterialPageRoute(
              builder: (context)=>Login()
          ));
        },
        child: ShowLinkMessage()
    ));

  }

  bool validateField(){
    if (emailController.text == "") {
      _messageBottomSheet(errrorMessage: Common.enterEmailString);
      return false;
    }else if (emailController.text.trim().length == 0) {
      _messageBottomSheet(errrorMessage: Common.enterEmailString);
      return false;
    }else if (!validator.email(emailController.text)) {
      _messageBottomSheet(errrorMessage: Common.enterValidEmailString);
      return false;
    } else{
      return true;
    }
  }

  _messageBottomSheet({String errrorMessage}){
    Flushbar(
      //title:  "Error",
      message:  "$errrorMessage",
      flushbarPosition: FlushbarPosition.TOP,
      flushbarStyle: FlushbarStyle.FLOATING,
      borderRadius: 10,
      margin: EdgeInsets.all(10),
      duration:  Duration(milliseconds: 1000),
    )..show(context);

  }

  popupAnimation({Widget child}){
    showGeneralDialog(
      useRootNavigator: true,
      barrierDismissible: true,
      barrierColor: Color.fromRGBO(24, 121, 232, 0.6),
      barrierLabel: '',
      context: context,
      pageBuilder: (
          BuildContext context,
          Animation<double> animation,
          Animation<double> secondaryAnimation,
          )=>child,
      transitionDuration: Duration(milliseconds: 500),
      transitionBuilder: (
          BuildContext context,
          Animation<double> animation,
          Animation<double> secondaryAnimation,
          Widget child,
          ) =>
          SlideTransition(
            position: Tween<Offset>(
              begin: const Offset(0, 1),
              end: Offset.zero,
            ).animate(animation),
            child: child,
          ),
    );
  }


  // api calling

  Future<BookmarkDeleteModel> forgotPasswordApi({String email}) async {

    try {
      HttpService _httpService = HttpService();

      var forgotPasswordData = await _httpService.forgotPasswordApi(email: email );


      if(forgotPasswordData.statusCode == 200){
        var jsonData = BookmarkDeleteModel.fromJson(jsonDecode(forgotPasswordData.body));

          if(jsonData.status == 1){
            _showLinkMessageBottomSheet();
          }else{
           await  _flushPopUp(
                message: jsonData.message,
                bgColor: Color.fromRGBO(178,0,0,1),
                duration: Duration(seconds: 2) );
          }

        return jsonData;
      }else{
        _messageBottomSheet(errrorMessage: Common.serverError);
      }
    }catch(e){
      print("Exception : >>>>> " + e.toString());
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}

class ShowLinkMessage extends StatefulWidget {
  @override
  _ShowLinkMessageState createState() => _ShowLinkMessageState();
}

class _ShowLinkMessageState extends State<ShowLinkMessage> {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: <String, WidgetBuilder>{
        '/login': (BuildContext context) => new Login(),
      },
      home: GestureDetector(
        onTap:(){
          Navigator.pop(context);
        },
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: Container(
              padding: EdgeInsets.all(5),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.all(15),
                    padding: EdgeInsets.all(15),
                    height: 200,
                    decoration:BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(20),),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          height: 120,
                          padding:EdgeInsets.only(top: 30,bottom: 20),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    '${Common.sentLinkMessage}',
                                    softWrap: true,
                                    style: TextStyle(
                                        color: Color.fromRGBO(130, 150, 251, 1),
                                        fontFamily: Common.fontAdelleLight,
                                        fontSize: 18),
                                    textAlign: TextAlign.center,
                                  ),
                                ],

                              ),
                            ],
                          ),
                        ),
                        GestureDetector(
                          onTap: () async {
                            Navigator.of(context).pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
                            //Navigator.popUntil( context, ModalRoute.withName('/login'));
                          },
                          child: Container(
                            width: 80,
                            padding: EdgeInsets.only(
                                top: 10, bottom: 10, right: 15, left: 15),
                            decoration: BoxDecoration(
                                color: Color.fromRGBO(255, 165, 0, 1),
                                borderRadius: BorderRadius.all(
                                    Radius.circular(50))
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment
                                      .spaceBetween,
                                  children: <Widget>[
                                    Expanded(child: Text(
                                      "OK", style: registerButton,
                                      textAlign: TextAlign.center,)),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

