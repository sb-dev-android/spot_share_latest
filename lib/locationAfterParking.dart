import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoder/geocoder.dart';
import 'package:peer_to_peer/history.dart';
import 'stylesheet.dart';
import 'designModelClass/animationfile.dart';
import 'package:location/location.dart' as loc;
import 'designModelClass/DialogBox.dart';
import 'designModelClass/customtimepicker.dart';
import 'designModelClass/podoclasses/httpservices.dart';
import 'designModelClass/podoclasses/models.dart';
import 'designModelClass/podoclasses/helper.dart';
import 'designModelClass/podoclasses/common.dart';
import 'dart:convert';
import 'dart:ui' as ui;
import 'dart:io' as platform;
import 'package:flushbar/flushbar.dart';
import 'designModelClass/podoclasses/location_permission.dart';
import 'designModelClass/podoclasses/internet_connectivity.dart';
import 'package:animating_location_pin/animating_location_pin.dart';
import 'designModelClass/customAppbar.dart' as apbar;
import 'package:peer_to_peer/Login.dart';
import 'dart:ui';
import 'dart:typed_data';
import 'package:connectivity/connectivity.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:permission_handler/permission_handler.dart';

double radiusCircle =100.0;
var lattitudeStore;
var longitudeStore;


class LocationAfterParking extends StatefulWidget {

  LocationAfterParking(/*this.status*/{Key key, this.analytics, this.observer})
      : super(key: key);
  final FirebaseAnalytics analytics;
  final FirebaseAnalyticsObserver observer;

  @override
  _LocationAfterParkingState createState() => _LocationAfterParkingState(analytics, observer);
}

class _LocationAfterParkingState extends State<LocationAfterParking> {

  _LocationAfterParkingState(this.analytics, this.observer);

  final FirebaseAnalyticsObserver observer;
  final FirebaseAnalytics analytics;

  InternetConnectivity _internetConnectivity = InternetConnectivity();
  final Connectivity _connectivity = Connectivity();
  static StreamSubscription<ConnectivityResult> connectivitySubscriptionVariable;


  String tempBookmarkId;
  Helper helper = Helper();

  static PermissionsService permissionsService = PermissionsService();
  bool _serviceEnabled;
  loc.Location location = new loc.Location();
  LatLng _latLng ;
  var address;
  var city;

  setIniTialCameraPosition() async{
     //await permissiobMethod();
    BitmapDescriptor userMarker =  await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(size: new Size(30,40)),
        //config,
        'assets/df.png')
        .then((onValue) {
      return onValue;
    });
    LatLng lastlatlng = await helper.getLastMapLocation();

    if(lastlatlng != null){
      _latLng = lastlatlng;

      setState(() {
        _lattitude = lastlatlng.latitude;
        _longitude = lastlatlng.longitude;

        _markers.add(Marker(
            markerId: MarkerId(_curPosition.toString()),
            position: LatLng(lastlatlng.latitude,lastlatlng.longitude),
            onTap: (){},
            icon: userMarker
        ));
      });

    }else{
      _latLng = Common.newYorklatlng;

      setState(() {
        _lattitude = Common.newYorklatlng.latitude;
        _longitude = Common.newYorklatlng.longitude;
      });
    }
  }

  permissiobMethod() async {
    _serviceEnabled = await location.serviceEnabled();
    if(_serviceEnabled){
      curPosition();
    }else{
      var permissionCheck = await permissionsService.requestLocationPermission();
      if(permissionCheck){
        curPosition();
      }else{
         await permissiobMethod();
         showDialog(
             context: context,
             builder: (context){
               return AlertDialog(
                 title: Text(
                     '${Common.internetErrorMessage}'
                 ),
                 actions: <Widget>[
                   FlatButton(
                     child: Text('Retry'),
                     onPressed: (){
                       Navigator.pushReplacement(context, MaterialPageRoute(
                           builder: (context)=> LocationAfterParking()
                       ));
                     },
                   ),
                 ],
               );
             }
         );
      }
    }
  }


  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  Completer<GoogleMapController> _mapController = Completer();
  GoogleMapController mapController;

  double zoomCamera  = Common.zoomCamera;


  Position _curPosition;

  static var _lattitude;
  static var _longitude;

  String cityText;


  bool cIcon = false;

  final Set<Marker> _markers = {};
  final Set<Marker> _customMarkers = {};

  final Set<Circle> _circleLocation = {};

  final Set<Polyline> _polyLines = {};
  Set<Polyline> get polyLines => _polyLines;

// addresss variable
  String adminArea;
  String subAdminArea;
  String locality;
  String subLocality;
  String countryName;
  bool searchBar = true;
  bool faverColor = false;

  StreamSubscription<loc.LocationData> _locationSubscription;
  loc.LocationData _locationData;
  Future<bool> onWillPop()async{
    return false;
  }

  @override
  void initState() {
    super.initState();

    connectionCheck();
    connectivitySubscription();

    permissiobMethod();

    _locationSubscription = location.onLocationChanged().handleError((err) {
      //permissiobMethod();
    }).listen((loc.LocationData currentLocation) async {
      _serviceEnabled = await location.serviceEnabled();
      print("Service enabled status of GPS ...>>> $_serviceEnabled");
      if(_serviceEnabled){
        _locationData = currentLocation;
        print('${_locationData.toString()}');
      }else{
        await permissiobMethod();
        showDialog(
            context: context,
            builder: (context){
              return AlertDialog(
                title: Text(
                    'GPS not active'
                ),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Retry'),
                    onPressed: (){
                      Navigator.pushReplacement(context, MaterialPageRoute(
                          builder: (context)=> LocationAfterParking()
                      ));
                    },
                  ),

                  FlatButton(
                    child: Text('Exit'),
                    onPressed: (){
                      platform.exit(0);
                    },
                  )
                ],
              );
            }
        );
      }
    });

    setIniTialCameraPosition();

  }

  Widget _googleMap(){
    return Container(
      child: Stack(
        children: <Widget>[
          Container(
            child:
            onMapCreated != null
                ? GoogleMap(
              markers: Set<Marker>.of(_markers),
              circles: Set.from(_circleLocation),
              polylines: _polyLines,
              myLocationButtonEnabled: false,
              mapType: MapType.normal,
              zoomGesturesEnabled: true,
              scrollGesturesEnabled: true,
              rotateGesturesEnabled: true,
              tiltGesturesEnabled: false,
              myLocationEnabled: false,
              compassEnabled: true,
              initialCameraPosition:CameraPosition(
                  target: LatLng(_lattitude, _longitude),
                  zoom: zoomCamera
              ),
              onMapCreated: onMapCreated,
              onCameraMove: (value){
                setState(() {
                  zoomCamera = value.zoom;
                });
              },
              onTap: (LatLng location){
                /*if(_markers.length>=1 && _circleLocation.length>=1)
                                {
                                  _markers.clear();
                                  _circleLocation.clear();
                                }*/
                _markers.clear();
                _circleLocation.clear();

                mapController.animateCamera(
                    CameraUpdate.newCameraPosition(CameraPosition(
                        target: location,
                        zoom: zoomCamera
                    )
                    )
                );
                setAppbarTitle(latLng: location);
                _onAddCircle(location);
                _onAddMarkerButtonPressed(location);
                //getPoints(location);
                setState(() {
                  lattitudeStore = location.latitude;
                  longitudeStore = location.longitude;
                });

              },
            )
                : Center(
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    AnimatingLocationPin(),
                    Text('Searching your location...')
                  ],
                ),
              ),
            ),
          ),

          _searchcurrentLocationwidget2()

        ],
      ),
    );
  }

  Widget _android_View(){
    return MaterialApp(
      routes: <String, WidgetBuilder> {
        '/History': (BuildContext context) => new History(true),
      },

      debugShowCheckedModeBanner: false,
      home: Container(
        color: Colors.white,
        child: SafeArea(
          top: false,
          /*bottom: false,*/
          child: Scaffold(
            key: _scaffoldKey,
            appBar: apbar.AppBar(
              elevation: Common.elevation,
              backgroundColor: Colors.white,
              leading: IconButton(
                icon:Image(
                  image: AssetImage(
                      "assets/icn_back.png"
                  ),
                ),
                onPressed: (){
                  Navigator.of(context).pop();
                },
              ),
              centerTitle: true,
              title:Text(
                Common.leavingForParkingHeading, style: appbarHeading,),
              actions: <Widget>[
                IconButton(
                  icon: faverColor
                      ? Icon( Icons.star,
                    color: Colors.green[300],
                    size: 35,)
                      : Image(  image: AssetImage( "assets/icn_favorite.png" ),
                    color: Colors.black,  ),
                  onPressed: (){

                    faverColor
                        ? deleteFaveriouteLocationMthod(bookmarkId: tempBookmarkId)
                        : addFaveriouteLocationMethod();
                  },
                ),
              ],
            ),

            body:   _longitude == null && _lattitude == null
                ? Center(
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    AnimatingLocationPin(),
                    Text('Searching your location...')
                  ],
                ),
              ),
            )
                :  _googleMap(),
          ),
        ),
      ),
    );

  }

  Widget _searchcurrentLocationwidget2(){
    return Align(
        alignment: Alignment.bottomCenter,
        child: Padding(
            padding: const EdgeInsets.only(bottom: 20),
            child:
            platform.Platform.isIOS
                ?  FlatButton(
              child: Container(
                width: 215,
                height: 50,
                //padding: EdgeInsets.all(12),
                decoration: BoxDecoration(
                    color: Color.fromRGBO(28, 129, 247, 1),
                    borderRadius: BorderRadius.all(Radius.circular(30))
                ),
                child: CupertinoActionSheetAction(
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(right: 10),
                        child: Image(
                          image: AssetImage(
                              "assets/icn_search_location.png"
                          ),
                        ),
                      ),
                      Text(
                        "${Common.searchCurrentLocation}",
                        style: locationSearchLocationButton,
                      ),
                    ],
                  ),
                  onPressed: (){
                    animateCamera(latLng: LatLng(_curPosition.latitude,_curPosition.longitude));
                  },
                ),
              ),
              onPressed:  (){
                animateCamera(latLng: LatLng(_curPosition.latitude,_curPosition.longitude));
              },
            )
                :  FlatButton(
              child: Container(
                width: 205,
                padding: EdgeInsets.all(12),
                decoration: BoxDecoration(
                    color: Color.fromRGBO(28, 129, 247, 1),
                    borderRadius: BorderRadius.all(Radius.circular(30))
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 5),
                      child: Image(
                        image: AssetImage(
                            "assets/icn_search_location.png"
                        ),
                      ),
                    ),
                    Text(
                      "Search Current Location",
                      style: locationSearchLocationButton,
                    ),
                  ],
                ),
              ),
              onPressed: (){
                animateCamera(
                    latLng: LatLng(_curPosition.latitude,_curPosition.longitude)
                );
              },
            )
        )
    );
  }

  Widget _searchcurrentLocationwidget(){
    return Align(
      alignment: Alignment.bottomCenter,
      child: Padding(
          padding: const EdgeInsets.only(bottom: 20),
          child:
          platform.Platform.isIOS
              ?  GestureDetector(
            onTap: (){
              animateCamera(latLng: LatLng(_curPosition.latitude,_curPosition.longitude));
            },
            child: Container(
              width: 215,
              height: 50,
              //padding: EdgeInsets.all(12),
              decoration: BoxDecoration(
                  color: Color.fromRGBO(28, 129, 247, 1),
                  borderRadius: BorderRadius.all(Radius.circular(30))
              ),
              child: CupertinoActionSheetAction(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: Image(
                        image: AssetImage(
                            "assets/icn_search_location.png"
                        ),
                      ),
                    ),
                    Text(
                      "${Common.searchCurrentLocation}",
                      style: locationSearchLocationButton,
                    ),
                  ],
                ),
                onPressed: (){
                  animateCamera(latLng: LatLng(_curPosition.latitude,_curPosition.longitude));
                },
              ),
            ),
          )
              :  GestureDetector(
            behavior: HitTestBehavior.deferToChild,
            onTap: (){
              animateCamera(latLng: LatLng(_curPosition.latitude,_curPosition.longitude));
            },
            child: Container(
              width: 205,
              padding: EdgeInsets.all(12),
              decoration: BoxDecoration(
                  color: Color.fromRGBO(28, 129, 247, 1),
                  borderRadius: BorderRadius.all(Radius.circular(30))
              ),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 5),
                    child: Image(
                      image: AssetImage(
                          "assets/icn_search_location.png"
                      ),
                    ),
                  ),
                  Text(
                    "Search Current Location",
                    style: locationSearchLocationButton,
                  ),
                ],
              ),
            ),
          )
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarIconBrightness:Brightness.dark,
        statusBarColor: Colors.transparent,
      ),
    );
    return _android_View();
  }

  _flushPopUp({String message ,Color bgColor, Duration duration}){
    Flushbar(
      message:  "${message}",
      flushbarPosition: FlushbarPosition.TOP,
      flushbarStyle: FlushbarStyle.FLOATING,
      borderRadius: 10,
      backgroundColor: bgColor,
      margin: EdgeInsets.all(10),
      duration: duration,
    ).show(context);

  }

  _bottomPersistentSheet({LatLng latLng}) async {
    _scaffoldKey.currentState.showBottomSheet((BuildContext context) {
      return
        WillPopScope(
          onWillPop: onWillPop,
          child: GestureDetector(
            onTap: (){
              Navigator.pop(context);
            },
            child: SafeArea(
              top: false,
              child: Scaffold(
                backgroundColor: Colors.transparent,
                bottomNavigationBar: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    BS(latLng: latLng),
                    platform.Platform.isIOS
                        ? Container(
                      color: Color.fromRGBO(28, 129, 245, 1),
                      child: Container(
                        padding: EdgeInsets.only(bottom: 25,right: 10,left: 10,top: 20),
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(25),
                                topRight: Radius.circular(25)
                            )
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment:MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                GestureDetector(
                                  child: Container(
                                    padding:EdgeInsets.all(12),
                                    decoration: BoxDecoration(
                                        color: Color.fromRGBO(255, 165, 0, 1),
                                        borderRadius: BorderRadius.all(Radius.circular(50))
                                    ),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text(
                                          "Departing Now ?",
                                          style: bottomSheetMyLocationRadiusButton,
                                          //style: leavingParkingDepartureButton,
                                        ),
                                      ],
                                    ),
                                  ),
                                  onTap: ()async{

                                    var cnChk = await _internetConnectivity.checkConnectivity();
                                    if(cnChk){
                                      var chkFuture = await parkingAddMethod(lat: latLng.latitude,lng: latLng.longitude);
                                      if(chkFuture.status == 1 ){
                                        loadingScreen();
                                      }
                                    }else{
                                      _flushPopUp(
                                          message: Common.internetErrorMessage,
                                          bgColor: Color.fromRGBO(178,0,0,1),
                                          duration: Duration(milliseconds: 2500)
                                      );
                                    }

                                  },
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    //color: Color.fromRGBO(255, 165, 0, 1),
                                      borderRadius: BorderRadius.all(Radius.circular(50))
                                  ),
                                  child: FlatButton(
                                    child: Row(
                                      children: <Widget>[
                                        Text(
                                          "Select Departure Time ",
                                          style: leavingParkingDepartureButton,
                                          //style: bottomSheetMyLocationRadiusButton,
                                        ),
                                      ],
                                    ),
                                    onPressed: (){
                                      _updateDeparture();
                                    },
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    )
                    : Container(
                      color: Color.fromRGBO(28, 129, 245, 1),
                      child: Container(
                        padding: EdgeInsets.symmetric(vertical: 10,horizontal: 8),
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(25),
                                topRight: Radius.circular(25)
                            )
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment:MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                GestureDetector(
                                  child: Container(
                                    padding:EdgeInsets.all(12),
                                    decoration: BoxDecoration(
                                        color: Color.fromRGBO(255, 165, 0, 1),
                                        borderRadius: BorderRadius.all(Radius.circular(50))
                                    ),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text(
                                          "Departing Now ?",
                                          style: bottomSheetMyLocationRadiusButton,
                                          //style: leavingParkingDepartureButton,
                                        ),
                                      ],
                                    ),
                                  ),
                                  onTap: ()async{
                                    var cnChk = await _internetConnectivity.checkConnectivity();
                                    if(cnChk){
                                      var chkFuture = await parkingAddMethod(lat: latLng.latitude,lng: latLng.longitude);
                                      if(chkFuture.status == 1 ){
                                        loadingScreen();
                                      }
                                     /* Future.delayed(Duration(seconds: 5)).whenComplete((){
                                        Navigator.pop(context);
                                      });*/
                                    }else{
                                      _flushPopUp(
                                          message: Common.internetErrorMessage,
                                          bgColor: Color.fromRGBO(178,0,0,1),
                                          duration: Duration(milliseconds: 2500)
                                      );
                                    }

                                  },
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    //color: Color.fromRGBO(255, 165, 0, 1),
                                      borderRadius: BorderRadius.all(Radius.circular(50))
                                  ),
                                  child: FlatButton(
                                    child: Row(
                                      children: <Widget>[
                                        Text(
                                          "Select Departure Time ",
                                          style: leavingParkingDepartureButton,
                                          //style: bottomSheetMyLocationRadiusButton,
                                        ),
                                      ],
                                    ),
                                    onPressed: (){
                                      _updateDeparture();
                                    },
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        );
    },
        backgroundColor: Colors.transparent,

    );
  }

  _updateDeparture(){
    _scaffoldKey.currentState.showBottomSheet<void>(
          (BuildContext context){
        return GestureDetector(
          onTap: (){
            Navigator.pop(context);
          },
          child: SafeArea(
            top: false,
            child: Scaffold(
              backgroundColor: Color.fromRGBO(24, 121, 232, 0.5),
              bottomNavigationBar: GestureDetector(
                onTap: (){ },
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height/2,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30)
                      )
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              child: Text(
                                'Update Departure Time',
                                style: signInBottomThankYouSubHeading,
                                textAlign: TextAlign.center,
                                softWrap: true,
                                maxLines: 3,
                                //textScaleFactor: 1.5,
                              ),
                            )
                          ],
                        ),
                      ),

                      CustomPicker(
                        updateTime: (value){
                          setState(() {
                            Common.departureTime = value;
                          });
                        },
                      ),

                      Padding(
                        padding: const EdgeInsets.all(5),
                        child: Container(
                          decoration: BoxDecoration(
                              color: Color.fromRGBO(255, 165, 0, 1),
                              borderRadius: BorderRadius.all(Radius.circular(50))
                          ),
                          width: 200,
                          child: FlatButton(
                            shape: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.all(Radius.circular(50))
                            ),
                            child: Text(
                              'Save',
                              textAlign: TextAlign.center,
                              style: signInBottomThankYouButtonOk,),
                            onPressed: (){
                              getAddressLocation();
                              _homePageAfterDeparture();

                            },
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
      backgroundColor: Color.fromRGBO(24, 121, 232, 0.3),
    );

   /* showwDialog(
        context: context,
        builder: (context){
          return SafeArea(
            top: false,
            child: Scaffold(
              backgroundColor: Color.fromRGBO(24, 121, 232, 0.5),
              bottomNavigationBar: GestureDetector(
                onTap: (){ },
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height/2,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30)
                      )
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              child: Text(
                                'Update Departure Time',
                                style: signInBottomThankYouSubHeading,
                                textAlign: TextAlign.center,
                                softWrap: true,
                                maxLines: 3,
                                //textScaleFactor: 1.5,
                              ),
                            )
                          ],
                        ),
                      ),

                      CustomPicker(
                        updateTime: (value){
                          setState(() {
                            Common.departureTime = value;
                          });
                        },
                      ),

                      Padding(
                        padding: const EdgeInsets.all(5),
                        child: Container(
                          decoration: BoxDecoration(
                              color: Color.fromRGBO(255, 165, 0, 1),
                              borderRadius: BorderRadius.all(Radius.circular(50))
                          ),
                          width: 200,
                          child: FlatButton(
                            shape: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.all(Radius.circular(50))
                            ),
                            child: Text(
                              'Save',
                              textAlign: TextAlign.center,
                              style: signInBottomThankYouButtonOk,),
                            onPressed: (){
                              getAddressLocation();
                              _homePageAfterDeparture();

                            },
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
        }
    );*/
  }

  _homePageAfterDeparture(){
    _scaffoldKey.currentState.showBottomSheet<void>(
      (BuildContext context){
        return GestureDetector(
          onTap: (){
            Navigator.pop(context);
          },
          child: SafeArea(
            child: Scaffold(
              backgroundColor: Colors.transparent,
              bottomNavigationBar: GestureDetector(
                onTap: (){
                  Navigator.of(context).pop();
                },
                child: GestureDetector(
                  onTap: (){
                    Navigator.of(context).pop();
                  },
                  child: Container(
                    height: 230,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        color: Color.fromRGBO(28, 129, 245, 1),
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(25),
                            topRight: Radius.circular(25)
                        )
                    ),
                    child: Column(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(left: 15,right: 15,top: 15,bottom: 10),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[

                                  Expanded(
                                    child: Container(
                                      //width: MediaQuery.of(context).size.width * 0.4,
                                      //padding: EdgeInsets.all(),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Container(
                                              padding: EdgeInsets.only(left: 5),
                                              child: Text(
                                               Common.myLocationString,
                                                style: bottomSheetMyLocation,
                                              ),
                                            ),
                                          ),
                                          IconButton(
                                              icon: Image(
                                                  image:
                                                  //cIcon?
                                                  AssetImage("assets/icn_arrow_up.png")
                                                //: AssetImage("assets/icn_arrow_down.png"),
                                              ),
                                              onPressed: () {
                                               Navigator.pop(context);
                                              }
                                          ),

                                        ],
                                      ),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(Radius.circular(50)),
                                        color: Color.fromRGBO(0, 99, 216, 1),
                                      ),
                                    ),
                                  ),
                                ],
                              ),

                              city == null
                                  ? Container(
                                padding: EdgeInsets.only(left: 10,right: 15,top: 15,bottom: 10),
                                child: Row(
                                  children: <Widget>[
                                   CircularProgressIndicator()
                                  ],
                                ),
                              )
                                  : Container(
                                padding: EdgeInsets.only(left: 10,right: 15,top: 15,bottom: 10),
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Text(
                                          "$city",
                                          style:bottomSheetMyLocationHeading,
                                          softWrap: true),
                                    )
                                  ],
                                ),
                              ),

                              address == null
                                  ? Row(
                                children: <Widget>[
                                  Container(
                                    width: MediaQuery.of(context).size.width * 0.8,
                                    padding: EdgeInsets.only(left: 10,right: 15,top: 10,bottom: 10),
                                    child: Row(
                                      children: <Widget>[
                                        CircularProgressIndicator()
                                      ],
                                    ),
                                  ),
                                ],
                              )
                                  : Row(
                                children: <Widget>[
                                  Container(
                                    width: MediaQuery.of(context).size.width * 0.8,
                                    padding: EdgeInsets.only(left: 10,right: 15,top: 10,bottom: 10),
                                    child: Row(
                                      children: <Widget>[
                                        Expanded(
                                          child: Text(
                                            "$address",
                                            style:bottomSheetMyLocationAddress,
                                            softWrap: true,
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),


                              Container(
                                //padding: EdgeInsets.only(left: 10,right: 10,top: 10,bottom: 10),
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Text(
                                          'Departing within ${Common.departureTime} minute',
                                          style:TextStyle(
                                              color: Color.fromRGBO(232, 240, 34, 1),
                                              fontFamily: 'AdelleLight',
                                              fontSize: 16
                                          ),
                                          softWrap: true),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(5),
                                      child: Container(
                                        padding: EdgeInsets.all(5),
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.all(Radius.circular(50))
                                        ),
                                        height: 30,

                                        child: FlatButton(
                                          shape: OutlineInputBorder(
                                              borderSide: BorderSide.none,
                                              borderRadius: BorderRadius.all(Radius.circular(50))
                                          ),
                                          child: Text(
                                            'Change',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(color: Colors.blue),),
                                          onPressed: (){
                                            _updateDeparture();
                                          },
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      },
      backgroundColor: Colors.transparent,
    );

    /*showwDialog(
        context: context,
      builder: (context){
          return SafeArea(
            child: Scaffold(
              backgroundColor: Colors.transparent,
              bottomNavigationBar: GestureDetector(
                onTap: (){
                  Navigator.of(context).pop();
                },
                child: GestureDetector(
                  onTap: (){
                    Navigator.of(context).pop();
                  },
                  child: Container(
                    height: 230,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        color: Color.fromRGBO(28, 129, 245, 1),
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(25),
                            topRight: Radius.circular(25)
                        )
                    ),
                    child: Column(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(left: 15,right: 15,top: 15,bottom: 10),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[

                                  Expanded(
                                    child: Container(
                                      //width: MediaQuery.of(context).size.width * 0.4,
                                      //padding: EdgeInsets.all(),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Container(
                                              padding: EdgeInsets.only(left: 5),
                                              child: Text(
                                                Common.myLocationString,
                                                style: bottomSheetMyLocation,
                                              ),
                                            ),
                                          ),
                                          IconButton(
                                              icon: Image(
                                                  image:
                                                  //cIcon?
                                                  AssetImage("assets/icn_arrow_up.png")
                                                //: AssetImage("assets/icn_arrow_down.png"),
                                              ),
                                              onPressed: () {
                                                Navigator.pop(context);
                                              }
                                          ),

                                        ],
                                      ),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(Radius.circular(50)),
                                        color: Color.fromRGBO(0, 99, 216, 1),
                                      ),
                                    ),
                                  ),
                                ],
                              ),

                              city == null
                                  ? Container(
                                padding: EdgeInsets.only(left: 10,right: 15,top: 15,bottom: 10),
                                child: Row(
                                  children: <Widget>[
                                    CircularProgressIndicator()
                                  ],
                                ),
                              )
                                  : Container(
                                padding: EdgeInsets.only(left: 10,right: 15,top: 15,bottom: 10),
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Text(
                                          "$city",
                                          style:bottomSheetMyLocationHeading,
                                          softWrap: true),
                                    )
                                  ],
                                ),
                              ),

                              address == null
                                  ? Row(
                                children: <Widget>[
                                  Container(
                                    width: MediaQuery.of(context).size.width * 0.8,
                                    padding: EdgeInsets.only(left: 10,right: 15,top: 10,bottom: 10),
                                    child: Row(
                                      children: <Widget>[
                                        CircularProgressIndicator()
                                      ],
                                    ),
                                  ),
                                ],
                              )
                                  : Row(
                                children: <Widget>[
                                  Container(
                                    width: MediaQuery.of(context).size.width * 0.8,
                                    padding: EdgeInsets.only(left: 10,right: 15,top: 10,bottom: 10),
                                    child: Row(
                                      children: <Widget>[
                                        Expanded(
                                          child: Text(
                                            "$address",
                                            style:bottomSheetMyLocationAddress,
                                            softWrap: true,
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),


                              Container(
                                //padding: EdgeInsets.only(left: 10,right: 10,top: 10,bottom: 10),
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Text(
                                          'Departing within ${Common.departureTime} minute',
                                          style:TextStyle(
                                              color: Color.fromRGBO(232, 240, 34, 1),
                                              fontFamily: 'AdelleLight',
                                              fontSize: 16
                                          ),
                                          softWrap: true),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(5),
                                      child: Container(
                                        padding: EdgeInsets.all(5),
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.all(Radius.circular(50))
                                        ),
                                        height: 30,

                                        child: FlatButton(
                                          shape: OutlineInputBorder(
                                              borderSide: BorderSide.none,
                                              borderRadius: BorderRadius.all(Radius.circular(50))
                                          ),
                                          child: Text(
                                            'Change',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(color: Colors.blue),),
                                          onPressed: (){
                                            _updateDeparture();
                                          },
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
      }
    );*/
  }

  animateCamera({LatLng latLng})async{
    mapController.animateCamera(
        CameraUpdate.newCameraPosition(CameraPosition(
            target: latLng,
            zoom: zoomCamera ) )
    );
  }

  void curPosition() async{
    _curPosition = await Geolocator().getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    LatLng lastlatlng = await helper.getLastMapLocation();

     await helper.setLastMapLocation(lastmapLocation: LatLng(_curPosition.latitude,_curPosition.longitude));

    animateCamera(latLng:LatLng(_curPosition.latitude,_curPosition.longitude) );
    BitmapDescriptor userMarker = await _userAssetIcon(context);

    if(lattitudeStore!= null || longitudeStore != null){
      _markers.add(Marker(
          markerId: MarkerId(_curPosition.toString()),
          position: LatLng(lattitudeStore,longitudeStore),

          icon: BitmapDescriptor.defaultMarker
      ));
    }
    setState(() {
      _lattitude = _curPosition.latitude;
      _longitude = _curPosition.longitude;
      _markers.clear();

      _markers.add(Marker(
          markerId: MarkerId(_curPosition.toString()),
          position: LatLng(_curPosition.latitude,_curPosition.longitude),
          icon: userMarker
      ));
    });
    if(lattitudeStore != null || longitudeStore != null){
     setState(() {
       _markers.clear();
       _markers.add(Marker(
           markerId: MarkerId(_curPosition.toString()),
           position: LatLng(lattitudeStore,longitudeStore),
           icon: BitmapDescriptor.defaultMarker
       ));
     });
     _markers.add(Marker(
         markerId: MarkerId(_curPosition.toString()),
         position: LatLng(_curPosition.latitude,_curPosition.longitude),
         icon: userMarker
     ));
    }
  }

  onMapCreated(GoogleMapController controller) async {
    BitmapDescriptor userMarker = await _userAssetIcon(context);

    _mapController.complete(controller);
    setState(() {
      mapController = controller;
    });

    setState(() {
      _markers.add(Marker(
          markerId: MarkerId(_curPosition.toString()),
          position: LatLng(_curPosition.latitude,_curPosition.longitude),
          onTap: (){},
          icon: userMarker
      ));
    });
  }


  void _onAddMarkerButtonPressed(LatLng latlang) async{

    BitmapDescriptor userMarker = await _userAssetIcon(context);
    setState(() {
      _markers.add(Marker(
          markerId: MarkerId(_curPosition.toString()),
          position: LatLng(_lattitude,_longitude),
          onTap: (){},
          icon: userMarker
      ));

      _markers.add(Marker(
        markerId: MarkerId(latlang.toString()),
        position: latlang,
        onTap: (){
          _bottomPersistentSheet(latLng: latlang);
          getAddressLocation(latLng: latlang);
          },
        infoWindow: InfoWindow( title: countryName, ),
        icon: BitmapDescriptor.defaultMarker,
      ));

    });
    _bottomPersistentSheet(latLng: latlang);
    getAddressLocation(latLng: latlang);
  }

  void _onAddCircle(LatLng latlang) {
    setState(() {
      _circleLocation.add(
          Circle(
            circleId : CircleId(latlang.toString()),
            center: latlang,
            radius: radiusCircle,
            fillColor: Color.fromRGBO(128, 142, 253, 0.2),
            strokeWidth: 3,
            visible: true,
            strokeColor: Color.fromRGBO(128, 142, 253, 1),
          )
      );
    }
    );
  }



  loadingScreen(){
    showwDialog(
        context: context,
        builder: (context)=>ForLeavingParkingMatch()
    );
    Future.delayed(Duration(seconds: 5),(){});
  }

  //api calling

  Future<ParkingAddModel> parkingAddMethod({double lat,double lng}) async {

    String oauth = await helper.getOauth();
    var locality = await helper.getLocality(latLng: LatLng(lat,lng));

    try {
      HttpService _httpService = HttpService();

      var leaveparkingData = await _httpService.parkingAdd(
          token:  oauth,
          latLng:LatLng(lattitudeStore, longitudeStore),//
          address: "$locality"
      );
      if(leaveparkingData.statusCode == 200){

      }else if(leaveparkingData.statusCode == 401){
        Navigator.pushReplacement(context, MaterialPageRoute(
          builder: (context)=> Login()
        ));

      }else{
        _flushPopUp(
            message: Common.serverError,
            bgColor: Color.fromRGBO(178,0,0,1),
            duration: Duration(milliseconds: 2500)
        );
      }
      var jsonData = ParkingAddModel.fromJson2(jsonDecode(leaveparkingData.body));
      return jsonData;
     }catch(e){
      print(e.toString());
     }
  }

  Future<List<BookmarkModel>> _bookmarkmethod() async {

    String oauth = await helper.getOauth();
    var jsonData;
    try {
      HttpService _httpService = HttpService();

      var bookmarkdata = await _httpService.bookmark(token:  oauth);

      if(bookmarkdata.statusCode == 200){
        jsonData = await json.decode(bookmarkdata.body);
      }

      print("jason data is ________============ $jsonData");

      List<BookmarkModel> bookmarklist = <BookmarkModel>[];

      for(var i in jsonData["data"]){
        var locality = await helper.getLocality(latLng: LatLng(i["latitude"],i["longitude"]));
        BookmarkModel dt = BookmarkModel(
            status: jsonData["status"],
            message: jsonData["message"],
            address: i["address"],
            longitude: i["longitude"],
            latitude: i["latitude"],
            locality: locality
        );

        bookmarklist.add(dt);
      }
      //TODO : return
      return bookmarklist;

    }catch(e){
      print("Exception : >>>>> " + e.toString());
    }
  }

  addFaveriouteLocationMethod()async{
    if(lattitudeStore == null && longitudeStore == null){
      return;
    }
    else{
      var chkFuture = await bookmarkAddApi(latLng: LatLng(lattitudeStore,longitudeStore));
      if(chkFuture.status == 1 ){
        setState(() {
          faverColor= true;
          tempBookmarkId = chkFuture.bookmarkId;
        });
        _flushPopUp(
            message: Common.bookmarkAddedsuccessfully,
            bgColor: Colors.green,
            duration: Duration(milliseconds: 2500));
      }else{

      }
    }
  }

  deleteFaveriouteLocationMthod({String bookmarkId, bool fever})async{
    var chkFuture = await bookmarkDeleteApi( bookmarkId: bookmarkId );
    if(chkFuture.status == 1){
      _flushPopUp(
          message: Common.bookmarkDeletesuccessfully,
          bgColor: Colors.green,
          duration: Duration(milliseconds: 2500));
      setState(() {
        faverColor= false;
      });
    }
  }


  _messageBottomSheet({String errrorMessage}){

    Flushbar(
      message:  "$errrorMessage",
      flushbarPosition: FlushbarPosition.TOP,
      flushbarStyle: FlushbarStyle.FLOATING,
      borderRadius: 10,
      margin: EdgeInsets.all(10),
      duration:  Duration(milliseconds: 1000),
    )..show(context);

  }

  void addUserLocationIcon() async{
    BitmapDescriptor userMarkericon = await _userAssetIcon(context);
    _customMarkers.add(Marker(
        markerId: MarkerId(_curPosition.toString()),
        position: LatLng(_curPosition.latitude,_curPosition.longitude),
        onTap: (){},
        icon: userMarkericon
    ));

   // BitmapDescriptor.fromAssetImage(configuration, assetName)
  }

  Future<BitmapDescriptor> _userAssetIcon(BuildContext context) async {
    final Completer<BitmapDescriptor> bitmapIcon = Completer<BitmapDescriptor>();
    final ImageConfiguration config = createLocalImageConfiguration(context);
    print("Tag_map, config set");
    const AssetImage("assets/df.png")
        .resolve(config)
        .addListener(ImageStreamListener((ImageInfo image, bool sync) async {
      print("Tag_map, image set");
      final ByteData bytes =
      await image.image.toByteData(format: ui.ImageByteFormat.png);
      print("Tag_map, bytes set");
      final BitmapDescriptor bitmap =
      BitmapDescriptor.fromBytes(bytes.buffer.asUint8List());
      bitmapIcon.complete(bitmap);
    }));

    return await bitmapIcon.future;

    //var config = createLocalImageConfiguration(context, size: Size(65, 130));
   /* return await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(size: Size(0.5,0.5)),
        //config,
        'assets/df.png')
        .then((onValue) {
      return onValue;
    });*/
  }

  getAddressLocation({LatLng latLng}) async {
    try{
      final coordinates = new Coordinates( lattitudeStore, longitudeStore );
      var addresses = await Geocoder.local.findAddressesFromCoordinates(
          coordinates);
      var first = addresses.first;
      setState(() {
        city = first.locality;
        address = "${first.addressLine}";
      });
    }catch(e){
      print(e.toString());
    }
  }

  setAppbarTitle({LatLng latLng,String title}) async{
    var heading_temp = await helper.getLocality(latLng: latLng);
    setState(() {
      if(latLng != null){
        setState(() {
          Common.leavingForParkingHeading = heading_temp;
        });
      }
      else if(title != null) {
        setState(() {
          Common.leavingForParkingHeading = title;
        });
      }
    });
  }


  // Internet connection check

  connectionCheck() async {
    var cnChk = await _internetConnectivity.checkConnectivity();
    if(cnChk){
      return ;
    }else{
      showDialog(
          context: context,
          builder: (context){
            return AlertDialog(
              title: Text(
                  '${Common.internetErrorMessage}'
              ),
              actions: <Widget>[
                FlatButton(
                  child: Text('Retry'),
                  onPressed: (){
                    Navigator.pushReplacement(context, MaterialPageRoute(
                        builder: (context)=> LocationAfterParking()
                    ));
                  },
                )
              ],
            );
          }
      );
    }
  }

  connectivitySubscription() async{
    connectivitySubscriptionVariable =
    await _connectivity.onConnectivityChanged.listen(
            (ConnectivityResult result){
          print(" connectivity reulsts  $result");
          if (result == ConnectivityResult.mobile) {
          } else if (result == ConnectivityResult.wifi) {
          }  else if (result == ConnectivityResult.none) {
            showDialog(
                context: context,
                builder: (context){
                  return AlertDialog(
                    title: Text(
                        '${Common.internetErrorMessage}'
                    ),
                    actions: <Widget>[
                      FlatButton(
                        child: Text('Retry'),
                        onPressed: (){
                          Navigator.pushReplacement(context, MaterialPageRoute(
                              builder: (context)=> LocationAfterParking()
                          ));
                        },
                      )
                    ],
                  );
                }
            );
          }else{
            showDialog(
                context: context,
                builder: (context){
                  return AlertDialog(
                    title: Text(
                        '${Common.internetErrorMessage}'
                    ),
                    actions: <Widget>[
                      FlatButton(
                        child: Text('Retry'),
                        onPressed: (){
                          Navigator.pushReplacement(context, MaterialPageRoute(
                              builder: (context)=> LocationAfterParking()
                          ));
                        },
                      )
                    ],
                  );
                }
            );
          }

        });
  }

  connectivitySubscriptionCancel(){
    connectivitySubscriptionVariable.cancel();  }


  // api calling method

  Future<BookmarkAddModel> bookmarkAddApi({LatLng latLng}) async {
    String oauth = await helper.getOauth();
    var addrline = await helper.getAddressLocation(latLng: latLng);
    try {
      HttpService _httpService = HttpService();

      var addBookmarkData = await _httpService.bookmarkAdd(
          token:  oauth,
          latLng: latLng,
          address: "$addrline"
      );

      if(addBookmarkData.statusCode == 200){
        var jsonData = BookmarkAddModel.fromJson(jsonDecode(addBookmarkData.body));
        setState(() {
          tempBookmarkId = jsonData.bookmarkId;
        });
        return jsonData;

      }else  if(addBookmarkData.statusCode == 401){
        //_messageBottomSheet(errrorMessage: Common.authInvalid);
        Navigator.pushReplacement(context,
            MaterialPageRoute(
                builder: (context)=> Login()));
      }else{
        _messageBottomSheet(errrorMessage: Common.serverError);
      }
    }catch(e){
      print("Exception : >>>>> " + e.toString());
    }
  }

  Future<BookmarkDeleteModel> bookmarkDeleteApi({String bookmarkId}) async {
    String oauth = await helper.getOauth();
    try {
      HttpService _httpService = HttpService();

      var deleteBookmarkData = await _httpService.bookmarkDelete( token:  oauth, bookmarkId: bookmarkId );

      print("delete bookmark response  ${deleteBookmarkData.body}");

      if(deleteBookmarkData.statusCode == 200){
        var jsonData = BookmarkDeleteModel.fromJson(jsonDecode(deleteBookmarkData.body));
        return jsonData;
      }else  if(deleteBookmarkData.statusCode == 401){
        //_messageBottomSheet(errrorMessage: Common.authInvalid);
        Navigator.pushReplacement(context,
            MaterialPageRoute(
                builder: (context)=> Login()));
      }else{
        _messageBottomSheet(errrorMessage: Common.serverError);
      }
    }catch(e){
      print("Exception : >>>>> " + e.toString());
    }
  }


  @override
  void dispose() {
   // _scaffoldKey.currentState.dispose();
    Common.leavingForParkingHeading  = 'Leaving for Parking';
    super.dispose();
  }

}


class IconWidget extends StatefulWidget {

  final VoidCallback callbackChange;

  IconWidget({this.callbackChange});
  @override
  _IconWidgetState createState() => _IconWidgetState();
}

class _IconWidgetState extends State<IconWidget> {

  static bool iconStatus = true;

  changeIcon(){
    setState(() {
      if(iconStatus){
        setState(()=> iconStatus = false);
      }else{
        {
          setState(()=> iconStatus = true);
        }
      }
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return iconStatus
        ? IconButton(
      icon: Image(image: AssetImage("assets/icn_arrow_up.png")),
      onPressed: (){
        //setState(()=> iconStatus = false);
        changeIcon();
        widget.callbackChange();
      },
    )
        : IconButton(
      icon:Image(image: AssetImage("assets/icn_arrow_down.png")),
      onPressed: (){
        widget.callbackChange();
        changeIcon();
        //setState(()=> iconStatus= true);
      },
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}



class BS extends StatefulWidget {
  final LatLng latLng;
  BS({this.latLng});
  _BS createState() => _BS();
}

class _BS extends State<BS> {

  var address;
  var city;

  static bool _showSecond = false;
  changeIcon(){
    setState(() {
      if(_showSecond){
        setState(()=> _showSecond = false);
      }else{
        {
          setState(()=> _showSecond = true);
        }
      }
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    getAddressLocation();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      child: AnimatedCrossFade(
          firstChild: GestureDetector(
            onTap: (){

            },
            child: Container(
              height: 80,
              decoration: BoxDecoration(
                  color: Color.fromRGBO(28, 129, 245, 1),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30),
                  )
              ),
              padding: EdgeInsets.only(left: 15,right: 15,top: 15,bottom: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[

                      Expanded(
                        child: Container(
                          //width: MediaQuery.of(context).size.width * 0.4,
                          //padding: EdgeInsets.all(),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  padding: EdgeInsets.only(left: 5),
                                  child: Text(
                                    Common.myLocationString,
                                    style: bottomSheetMyLocation,
                                  ),
                                ),
                              ),
                              IconWidget(
                                callbackChange: (){
                                  changeIcon();
                                },
                              )

                            ],
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(50)),
                            color: Color.fromRGBO(0, 99, 216, 1),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          secondChild: GestureDetector(
            onTap: (){},
            child: Container(
              decoration: BoxDecoration(
                  color: Color.fromRGBO(28, 129, 245, 1),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30),
                  )
              ),
              height: 235,
              padding: EdgeInsets.only(left: 15,right: 15,top: 15,bottom: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[

                      Expanded(
                        child: Container(
                          //width: MediaQuery.of(context).size.width * 0.4,
                          //padding: EdgeInsets.all(),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  padding: EdgeInsets.only(left: 5),
                                  child: Text(
                                    Common.myLocationString,
                                    style: bottomSheetMyLocation,
                                  ),
                                ),
                              ),

                              IconWidget(
                                callbackChange: (){
                                  changeIcon();
                                },
                              )

                            ],
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(50)),
                            color: Color.fromRGBO(0, 99, 216, 1),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 10,right: 15,top: 15,bottom: 10),
                    child: Row(
                      children: <Widget>[
                        city == null
                            ? CircularProgressIndicator()
                            : Expanded(
                          child: Text('${city??"Unknown"}',
                              style:bottomSheetMyLocationHeading,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                              softWrap: true),
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 10,right: 15,top: 10,bottom: 10),
                    child: Row(
                      children: <Widget>[
                        address == null
                            ? Container()
                            : Expanded(
                          child: Text('$address',
                              style:bottomSheetMyLocationAddress,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 2,
                              softWrap: true),
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 10,right: 15,top: 10,bottom: 10),
                    child: Row(
                      children: <Widget>[
                        Flexible(
                          child: Text('Free Parking Available',
                              style:TextStyle(
                                  color: Color.fromRGBO(232, 240, 34, 1),
                                  fontFamily: 'AdelleLight',
                                  fontSize: 16
                              ),
                              softWrap: true),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          crossFadeState: _showSecond
              ? CrossFadeState.showSecond
              : CrossFadeState.showFirst,
          duration: Duration(milliseconds: 70)),
      duration: Duration(milliseconds: 70),
    );
  }

  getAddressLocation() async {
    try{
      final coordinates = new Coordinates( widget.latLng.latitude, widget.latLng.longitude );
      var addresses = await Geocoder.local.findAddressesFromCoordinates(
          coordinates);
      var first = addresses.first;
      setState(() {
        city = first.locality;
        address = "${first.addressLine}";
      });
    }catch(e){
      print(e.toString());
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}





