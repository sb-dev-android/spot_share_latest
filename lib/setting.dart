import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:location/location.dart';
import 'package:peer_to_peer/designModelClass/podoclasses/common.dart';
import 'package:peer_to_peer/stylesheet.dart';
import 'package:peer_to_peer/Login.dart';
import 'package:peer_to_peer/home.dart';
import 'package:peer_to_peer/swapRequest.dart';
import 'package:peer_to_peer/savedLocation.dart';
import 'package:peer_to_peer/history.dart';
import 'package:peer_to_peer/successfull_match.dart';
import 'package:peer_to_peer/designModelClass/podoclasses/location_permission.dart';
import 'dart:async';
import 'package:permission_handler/permission_handler.dart';
import 'package:peer_to_peer/designModelClass/podoclasses/helper.dart';
import 'designModelClass/customAppbar.dart' as apbar;
import 'package:peer_to_peer/profileScreen.dart';

class Setting extends StatelessWidget {
  final bool status;
  Setting(this.status);
  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      routes: <String, WidgetBuilder> {
        '/login': (BuildContext context) => new Login(),
        '/home': (BuildContext context) => new Home(status),
      },
      debugShowCheckedModeBanner: false,
      home: SettingHome(status),
    );
  }
}

class SettingHome extends StatefulWidget {
  final bool status;
  SettingHome(this.status);
  @override
  _SettingHomeState createState() => _SettingHomeState();
}

class _SettingHomeState extends State<SettingHome> {

  final PermissionHandler _permissionHandler = PermissionHandler();
  Helper helper =  Helper();
  Geolocator geolocator = Geolocator()..forceAndroidLocationManager = true;
  GeolocationStatus geolocationStatus ;
  Location location = new Location();
  bool _serviceEnabled;
  LocationData _locationData;
  StreamSubscription<LocationData> _locationSubscription;
  static PermissionsService permissionsService = PermissionsService();

  chkLocationServiceStatus() async{
    _serviceEnabled = await location.serviceEnabled();
    if(_serviceEnabled){
      setState(() {
        _locationButton = _serviceEnabled;
      });
      }else{
       setState(() {
         _locationButton = _serviceEnabled;
       });
    }
  }

  openSettingsMenu() async {
    var resultSettingsOpening = false;
    try {
      _permissionHandler.openAppSettings()..whenComplete((){
        chkLocationServiceStatus();
        _locationSubscription = location.onLocationChanged().handleError((err) async{
          print('Location listening error code >>>>>>>>>>> ${err.code}');
          _serviceEnabled = await location.serviceEnabled();
          _locationButton = _serviceEnabled;
        }).listen((LocationData currentLocation) async {
          _serviceEnabled = await location.serviceEnabled();
          if(_serviceEnabled){
            _locationButton = _serviceEnabled;
          }else{
            _locationButton = _serviceEnabled;
          }
        });
      });

    } catch (e) {
      resultSettingsOpening = false;
    }

  }


  bool _locationButton =false;
  AssetImage _img = AssetImage("assets/icn_notification_off.png");
  AssetImage _img2 = AssetImage("assets/icn_notification_on.png");

  bool isCollapsed = true;
  double screenWidth;
  double screenHeight;
  Duration _duration = const Duration(milliseconds: 300);


  _setLocation() async{
    if(_locationButton){
      setState(() {
        _serviceEnabled = _locationButton;
        _locationButton = _locationButton;
      });
    }else{
      var permissionCheck = await  permissionsService.requestLocationPermission();

       if(permissionCheck){
         setState(() {
           _locationButton = true;
           _serviceEnabled = true;
         });
       }else{
         setState(() {
           _locationButton = false;
           _serviceEnabled = false;
         });
       }
    }
  }

  @override
  void initState()  {
    // TODO: implement initState
    chkLocationServiceStatus();
    super.initState();

    _locationSubscription = location.onLocationChanged().handleError((err) async{
      print('Location listening error code >>>>>>>>>>> ${err.code}');
      _serviceEnabled = await location.serviceEnabled();
      _locationButton = _serviceEnabled;
    }).listen((LocationData currentLocation) async {
      _serviceEnabled = await location.serviceEnabled();
      if(_serviceEnabled){
        _locationButton = _serviceEnabled;
      }else{
        _locationButton = _serviceEnabled;
      }
    });
  }


  @override
  Widget build(BuildContext context) {

    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarIconBrightness:Brightness.dark,
        statusBarColor: Colors.transparent,
      ),
    );

    Size size = MediaQuery.of(context).size;
    screenHeight = size.height;
    screenWidth = size.width;


    return SafeArea(
      top: false,
      bottom: false,
      child: Scaffold(
        body: Stack(fit: StackFit.expand,
          children: <Widget>[
            menu(context),
            dashBoard(context),
          ],
        ),
      ),
    );
  }

  Widget menu(context){

    var listTilePadding = EdgeInsets.only(
        bottom: (MediaQuery.of(context).size.width * 0.1) - 30,
        right: 25
    );

    return SafeArea(
      top: false,
      child: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
                top: MediaQuery.of(context).size.height * 0.1 + 20
            ),
            child: Container(
              padding: EdgeInsets.only(left: 15,right: 60),
              child: ListView(
                shrinkWrap: true,
                children: <Widget>[
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("Home",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage(
                          "assets/icn_home.png",
                        ),
                        color: Colors.blueAccent,

                      ),
                      onTap: (){
                        Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context)=>Home(widget.status)
                            )
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("Swap Request",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage(
                          "assets/icn_swap_request.png",
                        ),
                        color: Colors.blueAccent,

                      ),
                      onTap: (){
                        Navigator.of(context).push( MaterialPageRoute(  builder: (context)=>SwapRequest(widget.status)  )
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("History",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage(
                          "assets/icn_history.png",
                        ),
                        color: Colors.blueAccent,
                      ),
                      onTap: (){
                        Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context)=>History(widget.status)
                            )
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("Saved Location",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage(
                          "assets/icn_saved_location.png",
                        ),
                        color: Colors.blueAccent,
                      ),
                      onTap: (){
                        Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context)=>SavedLocation(widget.status)
                            )
                        );

                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("Settings",style: widget.status? activeSideMenuText: sideMenuText,),
                      trailing: Image(
                        image: AssetImage(  "assets/icn_settings.png"  ),
                        color: Colors.blueAccent,
                      ),
                      onTap: (){  },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("About Us",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage(  "assets/icn_about_us.png" ),
                        color: Colors.blueAccent,
                      ),
                      onTap: (){

                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("Contact Us",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage(  "assets/icn_contact_us.png" ),
                        color: Colors.blueAccent,
                      ),
                      onTap: (){

                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("Rate Us",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage( "assets/icn_rate_us.png", ),
                        color: Colors.blueAccent,
                      ),
                      onTap: (){
                        rateUs(context);
                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("Sign Out",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage(  "assets/icn_signout.png" ),
                        color: Colors.blueAccent,
                      ),
                      onTap: (){
                        Navigator.of(context).pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
                        // Navigator.pushNamedAndRemoveUntil(context,'/login', ModalRoute.withName('/home'));
                        helper.clearOauth();
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 15,right: 75,),
            margin: EdgeInsets.only(top: 20),
            height: 80,
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border(
                    bottom: BorderSide(
                      color: Colors.black.withOpacity(0.5),
                      width: 0.5,
                    )
                )
            ),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment:  MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "${Common.profileCarUser}",
                      style: textProfileNameStyle,
                    ),
                    IconButton(
                      icon: Icon(Icons.arrow_forward),
                      onPressed: (){
                        Navigator.push(context, MaterialPageRoute(
                            builder: (context)=>ProfileScreen()
                        ));
                      },
                    )
                  ],
                ),
                Row(
                  children: <Widget>[
                    Text(
                      "${Common.profileEmail}",
                      style: textProfileEmailStyle,
                    )
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget dashBoard(context){

    return AnimatedPositioned(
      top: isCollapsed ? 0: 0.0 * screenHeight,
      bottom: isCollapsed ? 0 : 0.0 * screenWidth,
      right: isCollapsed ? 0 : -4 * screenWidth,
      left: isCollapsed ? 0 : 0.8 * screenWidth,

      duration: _duration,

      child: Material(
        elevation: 8,
        child: Scaffold(
            appBar: apbar.AppBar(
              backgroundColor: Colors.white,
              centerTitle: true,
              elevation: Common.elevation,
              leading:IconButton(
                icon: Image(
                  image: AssetImage(  "assets/icn_menu_home.png"  ),
                ),
                onPressed: (){

                  setState(() {
                    if(isCollapsed){
                      setState(() {
                        isCollapsed =false;
                      });
                    }else{
                      setState(() {
                        isCollapsed=true;
                      });
                    }
                  });
                },
              ),
              title: Text(  "Settings",  style: appbarHeading  ),
            ),
            body: Container(
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          child: ListTile(
                            leading: Container(
                                padding: EdgeInsets.only(top: 5),
                                child: bullet(context)),
                            title: Container(child: Text("Location",style: settingText,)),
                            trailing: GestureDetector(
                              child: Container(
                                width: 55,
                                height:30,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(Radius.circular(20)),
                                    image: DecorationImage(
                                        image: _locationButton ? _img2 : _img,
                                        fit: BoxFit.cover
                                    )
                                ),
                              ),
                              onTap: ()async{ openSettingsMenu(); },
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Container(

                            child:  ListTile(
                              leading: Container(
                                  padding: EdgeInsets.only(top: 5),
                                  child: bullet(context)),
                              title: Text("Cache Memory",style: settingText,),
                            )
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top:8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        GestureDetector(
                          child: Container(
                            height: 50,
                            width: 300,
                            decoration: BoxDecoration(
                                color: Color.fromRGBO(28,129,245,1),
                                borderRadius: BorderRadius.all(Radius.circular(30))
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text( "Clear Cache Memory", style: settingTextClearCache )
                                  ],
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            )
        ),
      ),
    );
  }
  Widget bullet( BuildContext context){
    return new Container(
      height: 10.0,
      width: 10.0,
      decoration: new BoxDecoration(
        color: Color.fromRGBO(28,129,245,1),
        shape: BoxShape.circle,
      ),
    );
  }
  Widget listView(){
    return ListView(
      children: <Widget>[
        ListTile(
          leading: Container(
              padding: EdgeInsets.only(top: 5),
              child: bullet(context)),
          title: Container(child: Text("Location",style: settingText,)),
          trailing: Container(
            width: 50,
            height:30,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(20)),
                border: Border.all(color: Colors.black)
            ),
          ),
        ),

        ListTile(
          leading: Container(
              padding: EdgeInsets.only(top: 5),
              child: bullet(context)),
          title: Text("Cache Memory",style: settingText,),
        )
      ],
    );
  }



}

