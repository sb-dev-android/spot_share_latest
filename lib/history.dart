import 'package:flutter/material.dart';
import 'stylesheet.dart';
import 'package:flutter/services.dart';
import 'swapRequest.dart';
import 'savedLocation.dart';
import 'home.dart';
import 'setting.dart';
import 'Login.dart';
import 'designModelClass/podoclasses/common.dart';
import 'successfull_match.dart';
import 'package:peer_to_peer/designModelClass/podoclasses/helper.dart';
import 'designModelClass/customAppbar.dart' as apbar;
import 'package:peer_to_peer/profileScreen.dart';


class History extends StatelessWidget {
  final bool status;
  History(this.status);
  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      routes: <String, WidgetBuilder> {
        '/login': (BuildContext context) => new Login(),
        '/home': (BuildContext context) => new Home(status),
      },
      debugShowCheckedModeBanner: false,
      home: HistoryDrawerAnimation(status),
    );
  }
}

class HistoryDrawerAnimation extends StatefulWidget {
  final bool status;

  HistoryDrawerAnimation(this.status);

  @override
  _HistoryDrawerAnimationState createState() => _HistoryDrawerAnimationState();
}

class _HistoryDrawerAnimationState extends State<HistoryDrawerAnimation> {

  bool isCollapsed = true;
  double screenWidth;
  double screenHeight;
  Duration _duration = const Duration(milliseconds: 300);
  Helper helper =  Helper();

  @override
  Widget build(BuildContext context) {

    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarIconBrightness:Brightness.dark,
        statusBarColor: Colors.transparent,
      ),
    );

    Size size = MediaQuery.of(context).size;
    screenHeight = size.height;
    screenWidth = size.width;

    return SafeArea(
      top: false,
      bottom: false,
      child: Scaffold(
        body: Stack(fit: StackFit.expand,
          children: <Widget>[
            menu(context),
            dashBoard(context),
          ],
        ),
      ),
    );
  }

  Widget menu(context){

    var listTilePadding = EdgeInsets.only(
        bottom: (MediaQuery.of(context).size.width * 0.1) - 30,
        right: 25
    );

    return SafeArea(
      top: false,
      child: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
                top: MediaQuery.of(context).size.height * 0.1 + 20
            ),
            child: Container(
              padding: EdgeInsets.only(left: 15,right: 60),
              child: ListView(
                shrinkWrap: true,
                children: <Widget>[
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("Home",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage(
                          "assets/icn_home.png",
                          // "p2p parking slice/home page and sidemenu/sidemenu/icn_home.png",
                          // package: "p2p parking slice/home page and sidemenu/sidemenu"
                        ),
                        color: Colors.blueAccent,

                      ),
                      onTap: (){
                        Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context)=>Home(widget.status)
                            )
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("Swap Request",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage(
                          "assets/icn_swap_request.png",
                          //"p2p parking slice/home page and sidemenu/sidemenu/icn_swap_request.png",
                          // package: "p2p parking slice/home page and sidemenu/sidemenu"
                        ),
                        color: Colors.blueAccent,

                      ),
                      onTap: (){
                        Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context)=>SwapRequest(widget.status)
                            )
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("History",style: widget.status? activeSideMenuText: sideMenuText,),
                      trailing: Image(
                        image: AssetImage(
                          "assets/icn_history.png",
                          //"p2p parking slice/home page and sidemenu/sidemenu/icn_history.png",
                          // package: "p2p parking slice/home page and sidemenu/sidemenu"
                        ),
                        color: Colors.blueAccent,

                      ),
                      onTap: (){
                        Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context)=>History( widget.status)
                            )
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("Saved Location",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage(
                          "assets/icn_saved_location.png",
                          //"p2p parking slice/home page and sidemenu/sidemenu/icn_saved_location.png",
                          // package: "p2p parking slice/home page and sidemenu/sidemenu"
                        ),
                        color: Colors.blueAccent,
                      ),
                      onTap: (){
                        Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context)=>SavedLocation(widget.status)
                            )
                        );

                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("Settings",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage(
                          "assets/icn_settings.png",
                          // "p2p parking slice/home page and sidemenu/sidemenu/icn_settings.png",
                          // package: "p2p parking slice/home page and sidemenu/sidemenu"
                        ),
                        color: Colors.blueAccent,
                      ),
                      onTap: (){
                        Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context)=>Setting(widget.status)
                            )
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("About Us",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage(
                            "assets/icn_about_us.png"
                          // "p2p parking slice/home page and sidemenu/sidemenu/icn_about_us.png",
                          //package: "p2p parking slice/home page and sidemenu/sidemenu"
                        ),
                        color: Colors.blueAccent,
                      ),
                      onTap: (){

                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("Contact Us",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage(
                          "assets/icn_contact_us.png",
                          //"p2p parking slice/home page and sidemenu/sidemenu/icn_contact_us.png",
                          //package: "p2p parking slice/home page and sidemenu/sidemenu"
                        ),
                        color: Colors.blueAccent,
                      ),
                      onTap: (){

                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("Rate Us",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage(
                          "assets/icn_rate_us.png",
                          //  "p2p parking slice/home page and sidemenu/sidemenu/icn_rate_us.png",
                          // package: "p2p parking slice/home page and sidemenu/sidemenu"
                        ),
                        color: Colors.blueAccent,
                      ),
                      onTap: (){
                        rateUs(context);
                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("Sign Out",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage(
                          "assets/icn_signout.png",
                          //"p2p parking slice/home page and sidemenu/sidemenu/icn_signout.png",
                          //package: "p2p parking slice/home page and sidemenu/sidemenu"
                        ),
                        color: Colors.blueAccent,
                      ),
                      onTap: (){

                        helper.clearOauth();
                        Navigator.of(context).pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
                        //Navigator.pushNamedAndRemoveUntil(context,'/login', ModalRoute.withName('/home'));

                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 15,right: 75,),
            margin: EdgeInsets.only(top: 20),
            height: 80,
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border(
                    bottom: BorderSide(
                      color: Colors.black.withOpacity(0.5),
                      width: 0.5,
                    )
                )
            ),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment:  MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "${Common.profileCarUser}",
                      style: textProfileNameStyle,
                    ),
                    IconButton(
                      icon: Icon(Icons.arrow_forward),
                      onPressed: (){
                        Navigator.push(context, MaterialPageRoute(
                            builder: (context)=>ProfileScreen()
                        ));
                      },
                    )
                  ],
                ),
                Row(
                  children: <Widget>[
                    Text(
                      "${Common.profileEmail}",
                      style: textProfileEmailStyle,
                    )
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget dashBoard(context){

    return AnimatedPositioned(
      top: isCollapsed ? 0: 0.0 * screenHeight,
      bottom: isCollapsed ? 0 : 0.0 * screenWidth,
      right: isCollapsed ? 0 : -4 * screenWidth,
      left: isCollapsed ? 0 : 0.8 * screenWidth,

      duration: _duration,

      child: Material(
        elevation: 8,
        child: Scaffold(
          appBar: apbar.AppBar(
            elevation: Common.elevation,
            backgroundColor: Colors.white,
            centerTitle: true,
            leading: InkWell(
              child: Image(
                image: AssetImage(
                    "assets/icn_menu_home.png"
                ),
              ),
              onTap: (){

                if(isCollapsed){
                  setState(() {
                    isCollapsed =false;
                  });
                }else{
                  setState(() {
                    isCollapsed=true;
                  });
                }

              },
            ),
            title: Text("History", style: appbarHeading,),
          ),

          body: Container(
            child:RefreshIndicator(
              child: ListView.builder(
                  itemBuilder: (BuildContext context,int index){
                    return
                      Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 1),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.all(5),
                                  height: 30,
                                  width: MediaQuery.of(context).size.width*0.4 + 13,
                                  decoration: BoxDecoration(
                                      color: Colors.blue,
                                      borderRadius: BorderRadius.all(Radius.circular(10))
                                  ),

                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text("24 September 2019",
                                            style: TextStyle(
                                                color: Colors.white
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(10),
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              //height: 170,
                              decoration: BoxDecoration(
                                  color: Color.fromRGBO(238, 241, 242, 1),
                                  borderRadius: BorderRadius.all(Radius.circular(30))
                              ),
                              child: Column(
                                children: <Widget>[

                                  Container(
                                    //padding: EdgeInsets.only(left: 10,top: 20,),
                                    child: Column(
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              top: 15,
                                              bottom: 5,
                                              left: 18,
                                              right: 10

                                          ),
                                          child: Row(
                                            children: <Widget>[
                                              Text("Location",style: historyListViewLabel,),
                                            ],
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              bottom: 5,
                                              left: 18,
                                              right: 10

                                          ),
                                          child: Row(
                                            children: <Widget>[
                                              Container(
                                                //height: 40,
                                                child: Text(
                                                  "King David Tascos",
                                                  style: listViewHistoryLocation,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Divider(color: Colors.grey,indent: 10,endIndent: 10,thickness: 0.3,),
                                        Padding(
                                          padding: const EdgeInsets.only(top: 5,left: 10,bottom: 5),
                                          child: Column(
                                            children: <Widget>[
                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: <Widget>[
                                                  Expanded(
                                                    child: Padding(
                                                      padding: const EdgeInsets.only(left: 5),
                                                      child: Container(
                                                        width: MediaQuery.of(context).size.width * 0.1,
                                                        child: Text(
                                                          "Coenties Slip, New York, NY 10005,\n"
                                                              "United States",
                                                          softWrap: true,
                                                          maxLines: 2,
                                                          style: historyListViewLocation,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              )
                                            ],
                                          ),
                                        ),
                                        Container(
                                          decoration: BoxDecoration(
                                              color: Color.fromRGBO(217, 239, 237, 1),
                                              borderRadius: BorderRadius.only(
                                                  bottomLeft: Radius.circular(30),
                                                  bottomRight: Radius.circular(30)
                                              )
                                          ),
                                          child: Column(
                                            children: <Widget>[
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 5,
                                                    left: 18,
                                                    right: 18

                                                ),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: <Widget>[
                                                    Text("Arrival Time :",style: historyListViewLabel,),
                                                    Text("5.00 PM",style: historylistViewLabelText,)
                                                  ],
                                                ),
                                              ),

                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 5,
                                                    left: 18,
                                                    right: 18

                                                ),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: <Widget>[
                                                    Text("Departure Time :",style: historyListViewLabel,),
                                                    Text("8.00 PM",style: historylistViewLabelText,)
                                                  ],
                                                ),
                                              ),

                                              Padding(
                                                padding: const EdgeInsets.only(
                                                  top: 5,
                                                  left: 18,
                                                  right: 18,
                                                  bottom: 15,

                                                ),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: <Widget>[
                                                    Text("Swaping Car No. :",
                                                      style: historyListViewLabel,),
                                                    Text(
                                                      "GTK-5268",
                                                      style: historylistViewLabelText,)
                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      );
                  }
              ),
              onRefresh: () async{
               /* Navigator.pushReplacement(context, MaterialPageRoute(
                  builder: (context)=> History(true)
                ));*/
              // build(context);
                initState();
              },
            )
          ),
        ),
      ),
    );
  }

}

