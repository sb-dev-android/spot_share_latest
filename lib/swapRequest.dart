import 'package:flutter/material.dart';
import 'package:swipe_stack/swipe_stack.dart';
import 'package:peer_to_peer/stylesheet.dart';
import 'package:peer_to_peer/history.dart';
import 'package:peer_to_peer/savedLocation.dart';
import 'package:flutter/services.dart';
import 'package:peer_to_peer/setting.dart';
import 'package:peer_to_peer/successfull_match.dart';
import 'package:peer_to_peer/locationAfterParking.dart';
import 'package:peer_to_peer/home.dart';
import 'package:peer_to_peer/Login.dart';
import 'package:peer_to_peer/designModelClass/customtimepicker.dart';
import 'package:peer_to_peer/designModelClass/podoclasses/common.dart';
import 'package:peer_to_peer/designModelClass/DialogBox.dart';
import 'package:peer_to_peer/designModelClass/podoclasses/helper.dart';
import 'package:peer_to_peer/designModelClass/podoclasses/notification_handler.dart';
import 'dart:io';
import 'dart:convert';
import 'designModelClass/customAppbar.dart' as apbar;
import 'designModelClass/podoclasses/httpservices.dart';
import 'designModelClass/podoclasses/models.dart';
import 'package:flushbar/flushbar.dart';
import 'package:peer_to_peer/profileScreen.dart';


class SwapRequest extends StatelessWidget {
  final bool status;
  final Widget bottomNavigate;

  SwapRequest(this.status,{ this.bottomNavigate});
  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      routes: <String, WidgetBuilder> {
        '/login': (BuildContext context) => new Login(),
        '/home': (BuildContext context) => new Home(status),
        '/LocationAfterParking': (BuildContext context) => new LocationAfterParking(),
      },
      debugShowCheckedModeBanner: false,
      home: SwapRequestDrawerAnimation(status,),
    );
  }
}

class SwapRequestDrawerAnimation extends StatefulWidget {

  final bool status;
  SwapRequestDrawerAnimation(this.status,);
  @override
  _SwapRequestDrawerAnimationState createState() => _SwapRequestDrawerAnimationState();
}

class _SwapRequestDrawerAnimationState extends State<SwapRequestDrawerAnimation> {

  Helper helper = Helper();
  bool isCollapsed = true;
  double screenWidth;
  double screenHeight;
  Duration _duration = const Duration(milliseconds: 300);

  GlobalKey<ScaffoldState> _scaffold = GlobalKey<ScaffoldState>();

  var caruser;
  getusercar()async{
    setState(() async{
      caruser = await helper.getCarUser();
    });
  }



  @override
  void initState() {
    // TODO: implement initState
    getusercar();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarIconBrightness:Brightness.dark,
        statusBarColor: Colors.transparent,
      ),
    );

    Size size = MediaQuery.of(context).size;
    screenHeight = size.height;
    screenWidth = size.width;

    return MaterialApp(
      debugShowCheckedModeBanner: false,

      routes: <String, WidgetBuilder> {
        '/login': (BuildContext context) => new Login(),
        '/home': (BuildContext context) => new Home(widget.status),
        '/LocationAfterParking': (BuildContext context) => new LocationAfterParking(),
      },

      home: Container(
        color: Colors.white,
        child: SafeArea(
          top: false,
          bottom: false,
          child: Scaffold(
            body: Stack(fit: StackFit.expand,
              children: <Widget>[
                menu(context),
                dashBoard(context),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget menu(context){

    var listTilePadding = EdgeInsets.only(
        bottom: (MediaQuery.of(context).size.width * 0.1) - 30,
        right: 25
    );

    return SafeArea(
      //top: false,
      //bottom: false,
      child: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
                top: MediaQuery.of(context).size.height * 0.1 + 20
            ),
            child: Container(
              padding: EdgeInsets.only(left: 15,right: 60),
              child: ListView(
                shrinkWrap: true,
                children: <Widget>[
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("Home",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage("assets/icn_home.png",),
                        color: Colors.blueAccent,

                      ),
                      onTap: (){
                        Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context)=>Home(widget.status)
                            )
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("Swap Request",style: widget.status? activeSideMenuText: sideMenuText,),
                      trailing: Image(
                        image: AssetImage("assets/icn_swap_request.png",),
                        color: Colors.blueAccent,

                      ),
                      onTap: (){
                        Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context)=>SwapRequest(widget.status)
                            )
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("History",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage("assets/icn_history.png",),
                        color: Colors.blueAccent,

                      ),
                      onTap: (){
                        Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context)=>History(widget.status)
                            )
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("Saved Location",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage("assets/icn_saved_location.png",),
                        color: Colors.blueAccent,
                      ),
                      onTap: (){
                        Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context)=>SavedLocation(widget.status)
                            )
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("Settings",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage("assets/icn_settings.png",),
                        color: Colors.blueAccent,
                      ),
                      onTap: (){
                        Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context)=>Setting(widget.status)
                            )
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("About Us",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage("assets/icn_about_us.png"),
                        color: Colors.blueAccent,
                      ),
                      onTap: (){
                        //TODO: About us
                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("Contact Us",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage("assets/icn_contact_us.png",),
                        color: Colors.blueAccent,
                      ),
                      onTap: (){
                        //TODO: Contact us
                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("Rate Us",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage("assets/icn_rate_us.png",),
                        color: Colors.blueAccent,
                      ),
                      onTap: (){
                        rateUs(context);
                      },
                    ),
                  ),
                  Padding(
                    padding: listTilePadding,
                    child: ListTile(
                      title: Text("Sign Out",style: sideMenuText,),
                      trailing: Image(
                        image: AssetImage("assets/icn_signout.png",),
                        color: Colors.blueAccent,
                      ),
                      onTap: (){
                        Navigator.of(context).pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
                        //Navigator.pushNamedAndRemoveUntil(context,'/login', ModalRoute.withName('/home'));
                        helper.clearOauth();
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 15,right: 75,),
            margin: EdgeInsets.only(top: 20),
            height: 80,
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border(
                    bottom: BorderSide(
                      color: Colors.black.withOpacity(0.5),
                      width: 0.5,
                    )
                )
            ),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment:  MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "Hi ${Common.profileCarUser}",
                      style: textProfileNameStyle,
                    ),
                    IconButton(
                      icon: Icon(Icons.arrow_forward),
                      onPressed: (){
                        Navigator.push(context, MaterialPageRoute(
                            builder: (context)=>ProfileScreen()
                        ));
                      },
                    )
                  ],
                ),
                Row(
                  children: <Widget>[
                    Text(
                      "${Common.profileEmail}",
                      style: textProfileEmailStyle,
                    )
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget dashBoard(context){


    return AnimatedPositioned(
      top: isCollapsed ? 0: 0.0 * screenHeight,
      bottom: isCollapsed ? 0 : 0.0 * screenWidth,
      right: isCollapsed ? 0 : -4 * screenWidth,
      left: isCollapsed ? 0 : 0.8 * screenWidth,

      duration: _duration,

      child: Material(
        elevation: 8,
        child: Container(
          child: SafeArea(
            top: false,
            //bottom: false,
            child: Scaffold(
              key: _scaffold,
              appBar: apbar.AppBar(
                backgroundColor: Colors.white,
                centerTitle: true,
                elevation: Common.elevation,
                leading:IconButton(
                  icon: Image(
                    image: AssetImage("assets/icn_menu_home.png"),
                  ),
                  onPressed: (){
                    setState(() {
                      if(isCollapsed){
                        setState(() {
                          isCollapsed =false;
                        });
                      }else{
                        setState(() {
                          isCollapsed=true;
                        });
                      }
                    });
                  },
                ),
                title: Text(
                  "Swap Request",
                  style: appbarHeading,
                ),
              ),
              body: Container(
                padding: EdgeInsets.all(3),
                child: RefreshIndicator(
                  child: Stack(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(
                            top:
                            MediaQuery.of(context).size.height >640.0
                                ? (MediaQuery.of(context).size.height * 0.1)/2
                                : MediaQuery.of(context).size.height <680.0
                                ? (MediaQuery.of(context).size.height * 0.01)+10
                                : MediaQuery.of(context).size.height * 0.1
                        ),
                        child: Container(
                            child: RequestList()
                        ),
                      ),

                      Align(
                        alignment: Alignment.bottomCenter,
                        child:
                        Platform.isIOS
                            ?  Container(
                          height: 90,
                          decoration: BoxDecoration(
                            //color: Color.fromRGBO(255, 165, 0, 1),
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                  topRight : Radius.circular(30),
                                  topLeft : Radius.circular(30)
                              ),
                              border: Border.all(width: 0.2,color: Colors.black)
                          ),
                          child: Column(
                            children: <Widget>[
                              Stack(
                                children: <Widget>[
                                  Container(
                                    decoration: BoxDecoration(
                                      //color: Color.fromRGBO(255, 165, 0, 1),
                                      color: Colors.white,
                                      borderRadius: BorderRadius.only(
                                          topRight : Radius.circular(30),
                                          topLeft : Radius.circular(30)
                                      ),

                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: (){
                                      _updateDeparture();
                                    },
                                    child: Container(
                                      height: 70,
                                      decoration: BoxDecoration(
                                        //color: Color.fromRGBO(255, 165, 0, 1),
                                        color: Colors.white,
                                        borderRadius: BorderRadius.only(
                                            topRight : Radius.circular(30),
                                            topLeft : Radius.circular(30)
                                        ),
                                        //border: Border.all(width: 0.1,color: Colors.black)
                                      ),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text(
                                            "${Common.updateDepartingTime}",
                                            style: updateDepartureTimeButtonNo,
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        )
                            : GestureDetector(
                          onTap: (){
                            _updateDeparture();
                          },
                          child: Container(
                            height: 70,
                            decoration: BoxDecoration(
                              //color: Color.fromRGBO(255, 165, 0, 1),
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                    topRight : Radius.circular(30),
                                    topLeft : Radius.circular(30)
                                ),
                                border: Border.all(width: 0.1,color: Colors.black)
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  "${Common.updateDepartingTime}",
                                  style: updateDepartureTimeButtonNo,
                                )
                              ],
                            ),
                          ),
                        ),

                      ),

                    ],
                  ),
                  onRefresh: () async{
                    // build(context);
                    initState();
                  },
                )
              ),
            ),
          ),
        ),
      ),
    );
  }

  _updateDeparture(){
    return showwDialog(
      context: context,
      builder: (BuildContext context){
        return GestureDetector(
          onTap: (){
            Navigator.pop(context);
          },
          child: Scaffold(
            backgroundColor: Color.fromRGBO(24, 121, 232, 0.5),
            bottomNavigationBar: GestureDetector(
              onTap: (){

              },
              child: GestureDetector(
                onTap: (){  },
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height/2,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30)
                      )
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              child: Text(
                                "${Common.updateDepartingTime}",
                                style: signInBottomThankYouSubHeading,
                                textAlign: TextAlign.center,
                                softWrap: true,
                                maxLines: 3,
                                //textScaleFactor: 1.5,
                              ),
                            )
                          ],
                        ),
                      ),

                      CustomPicker(
                        updateTime: (value){
                          Common.departureTime = value;
                        },
                      ),

                      Padding(
                        padding: const EdgeInsets.all(5),
                        child: Container(
                          decoration: BoxDecoration(
                              color: Color.fromRGBO(255, 165, 0, 1),
                              borderRadius: BorderRadius.all(Radius.circular(50))
                          ),
                          width: 200,
                          child: FlatButton(
                            shape: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.all(Radius.circular(50))
                            ),
                            child: Text(
                              'Save',
                              textAlign: TextAlign.center,
                              style: signInBottomThankYouButtonOk,),
                            onPressed: (){
                              Navigator.pop(context);
                            },
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }


}

class RequestList extends StatefulWidget {
  @override
  _RequestListState createState() => _RequestListState();
}

class _RequestListState extends State<RequestList> {

  static var othercar;
  var caruser;
  getusercar()async{
    setState(() async{
      caruser = await helper.getCarUser();
    });
  }
  Helper helper = Helper();
  final GlobalKey<SwipeStackState> _swipeKey = GlobalKey<SwipeStackState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getusercar();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
                      future: parkingRequestListMethod(),
                      builder: (BuildContext context,AsyncSnapshot snapshot){
                        if(snapshot.hasData){
                          return SwipeStack(
                            key: _swipeKey,
                            children: [...snapshot.data].map((index) {
                                return SwiperItem(
                                    builder: (SwiperPosition position, double progress) {
                                      print('position : $position >>>>>>>   progress: $progress');
                                      return
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            MediaQuery.of(context).size.height >640.0
                                                ?  Container(
                                              margin: EdgeInsets.symmetric(
                                                  vertical: MediaQuery.of(context).size.height * 0.1 -40
                                              ),
                                              child: Material(
                                                color: Colors.transparent,
                                                elevation:5,
                                                borderRadius: BorderRadius.all(Radius.circular(50)),
                                                child: Container(
                                                    width:MediaQuery.of(context).size.width * 0.8,
                                                    height:
                                                    MediaQuery.of(context).size.height >640.0
                                                        ? 500.0
                                                        : MediaQuery.of(context).size.height * 0.7 + 20,
                                                    padding: EdgeInsets.symmetric(horizontal: 8,vertical: 3),
                                                    decoration: BoxDecoration(
                                                        color: Color.fromRGBO(28, 129, 245, 1),
                                                        borderRadius: BorderRadius.all(Radius.circular(50))
                                                    ),
                                                    child: Column(
                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                      children: <Widget>[
                                                        Container(
                                                          child: Column(
                                                            children: <Widget>[
                                                              Padding(
                                                                padding: const EdgeInsets.only(top:9,bottom: 3, left: 20),
                                                                child: Row(
                                                                  children: <Widget>[
                                                                    Text("Car Number",style: swaplistViewLabel,)
                                                                  ],
                                                                ),
                                                              ),
                                                              Padding(
                                                                padding: const EdgeInsets.only(bottom: 3,left: 20),
                                                                child: Row(
                                                                  children: <Widget>[
                                                                    Text(
                                                                      index.carNumber,
                                                                      //"BGY-3891",
                                                                      style: TextStyle(
                                                                          color: Color.fromRGBO(247, 255, 0, 1),
                                                                          fontSize: 18
                                                                      ),
                                                                    )
                                                                  ],
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                        Divider(thickness: 0.2, color: Colors.white,),
                                                        Container(
                                                          child: Column(
                                                            children: <Widget>[
                                                              Padding(
                                                                padding: const EdgeInsets.only(top: 3,bottom: 3,left: 20),
                                                                child: Row(
                                                                  children: <Widget>[
                                                                    Expanded(
                                                                        child: Text("Location",style: swaplistViewLabel,)
                                                                    )
                                                                  ],
                                                                ),
                                                              ),
                                                              Padding(
                                                                padding: const EdgeInsets.only(bottom: 3,left: 20),
                                                                child: Row(
                                                                  children: <Widget>[
                                                                    Expanded(
                                                                      child: Text(
                                                                        index.location,
                                                                        //"Block A, Conenties Slip New york"
                                                                        //   ", NY 10005",
                                                                        style: swapRequestText,
                                                                        maxLines: 3,
                                                                        softWrap: true,
                                                                      ),
                                                                    )
                                                                  ],
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                        Divider(thickness: 0.2, color: Colors.white,),
                                                        Container(
                                                          child: Column(
                                                            children: <Widget>[
                                                              Padding(
                                                                padding: const EdgeInsets.only(top: 3,bottom: 3.0,left: 20),
                                                                child: Row(
                                                                  children: <Widget>[
                                                                    Text("Name",style: swaplistViewLabel,)
                                                                  ],
                                                                ),
                                                              ),
                                                              Padding(
                                                                padding: const EdgeInsets.only(bottom: 3,left: 20),
                                                                child: Row(
                                                                  children: <Widget>[
                                                                    Text(
                                                                      "${ index.fname} ${ index.lname}",
                                                                      //"Alex Parker",
                                                                      style: swapRequestText,)
                                                                  ],
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                        Divider(thickness: 0.2, color: Colors.white,),
                                                        Container(
                                                          child: Column(
                                                            children: <Widget>[
                                                              Padding(
                                                                padding: const EdgeInsets.only(top: 3,bottom: 3,left: 20),
                                                                child: Row(
                                                                  children: <Widget>[
                                                                    Text("Make",style: swaplistViewLabel,)
                                                                  ],
                                                                ),
                                                              ),
                                                              Padding(
                                                                padding: const EdgeInsets.only(bottom: 3,left: 20),
                                                                child: Row(
                                                                  children: <Widget>[
                                                                    Text(
                                                                      index.make,
                                                                      //"Hyundai",
                                                                      style: swapRequestText,)
                                                                  ],
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                        Divider(thickness: 0.2, color: Colors.white,),
                                                        Container(
                                                          child: Column(
                                                            children: <Widget>[
                                                              Padding(
                                                                padding: const EdgeInsets.only(top: 3,bottom: 3,left: 20),
                                                                child: Row(
                                                                  children: <Widget>[
                                                                    Text("Model",style: swaplistViewLabel,)
                                                                  ],
                                                                ),
                                                              ),
                                                              Padding(
                                                                padding: const EdgeInsets.only(bottom: 3,left: 20),
                                                                child: Row(
                                                                  children: <Widget>[
                                                                    Text(
                                                                      index.model,
                                                                      //"i20",
                                                                      style: swapRequestText,)
                                                                  ],
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                        Divider(thickness: 0.2, color: Colors.white,),
                                                        Container(
                                                          child: Column(
                                                            children: <Widget>[
                                                              Padding(
                                                                padding: const EdgeInsets.only(top: 3,bottom: 3,left: 20),
                                                                child: Row(
                                                                  children: <Widget>[
                                                                    Text("Color",style: swaplistViewLabel,)
                                                                  ],
                                                                ),
                                                              ),
                                                              Padding(
                                                                padding: const EdgeInsets.only(bottom: 2,left: 20),
                                                                child: Row(
                                                                  children: <Widget>[
                                                                    Text(
                                                                      index.color,
                                                                      //"White",
                                                                      style: swapRequestText,)
                                                                  ],
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                        Divider(thickness: 0.2, color: Colors.white,),
                                                        Container(
                                                          padding: EdgeInsets.all(7),
                                                          child: Column(
                                                            children: <Widget>[
                                                              Row(
                                                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                children: <Widget>[
                                                                  FlatButton(
                                                                    child: Text(
                                                                      "Accept",
                                                                      style: TextStyle(
                                                                          color: Color.fromRGBO(26,128,246,1)
                                                                      ),
                                                                    ),
                                                                    color: Colors.white,
                                                                    shape: RoundedRectangleBorder(
                                                                        borderRadius:BorderRadius.all(Radius.circular(30)) ),
                                                                    onPressed: () {
                                                                      acceptRequest(
                                                                          parkingId: index.parkingId,
                                                                          othercar: index.carNumber,
                                                                          id: index.id);
                                                                      _swipeKey.currentState.swipeLeft();

                                                                    },
                                                                  ),
                                                                  FlatButton(
                                                                    child: Text(
                                                                      "Decline",
                                                                      style: TextStyle(
                                                                          color: Colors.white
                                                                      ),
                                                                    ),
                                                                    shape: RoundedRectangleBorder(
                                                                        borderRadius:BorderRadius.all(Radius.circular(30)),
                                                                        side: BorderSide(
                                                                            color: Colors.white,
                                                                            width: 0.5
                                                                        )
                                                                    ),
                                                                    onPressed: (){
                                                                      declineRequest(id: index.id);
                                                                      _swipeKey.currentState.swipeRight();
                                                                    },
                                                                  ),
                                                                ],
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    )
                                                ),
                                              ),
                                            )
                                                :  Material(
                                              color: Colors.transparent,
                                              elevation:5,
                                              borderRadius: BorderRadius.all(Radius.circular(50)),
                                              child: Container(
                                                  height: MediaQuery.of(context).size.height >640.0
                                                      ? 500.0
                                                      : MediaQuery.of(context).size.height * 0.7 + 20,
                                                  width:MediaQuery.of(context).size.width * 0.8 - 20,

                                                  padding: EdgeInsets.symmetric(horizontal: 10,vertical: 3),
                                                  decoration: BoxDecoration(
                                                      color: Color.fromRGBO(28, 129, 245, 1),
                                                      borderRadius: BorderRadius.all(Radius.circular(50))
                                                  ),
                                                  child: Column(
                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                    children: <Widget>[
                                                      Container(
                                                        child: Column(
                                                          children: <Widget>[
                                                            Padding(
                                                              padding: const EdgeInsets.only(top:9,bottom: 3, left: 20),
                                                              child: Row(
                                                                children: <Widget>[
                                                                  Text("Car Number",style: swaplistViewLabel,)
                                                                ],
                                                              ),
                                                            ),
                                                            Padding(
                                                              padding: const EdgeInsets.only(bottom: 3,left: 20),
                                                              child: Row(
                                                                children: <Widget>[
                                                                  Text(
                                                                    index.carNumber,
                                                                    //"BGY-3891",
                                                                    style: TextStyle(
                                                                        color: Color.fromRGBO(247, 255, 0, 1),
                                                                        fontSize: 18
                                                                    ),
                                                                  )
                                                                ],
                                                              ),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                      Divider(thickness: 0.2, color: Colors.white,),
                                                      Container(
                                                        child: Column(
                                                          children: <Widget>[
                                                            Padding(
                                                              padding: const EdgeInsets.only(top: 3,bottom: 3,left: 20),
                                                              child: Row(
                                                                children: <Widget>[
                                                                  Expanded(
                                                                      child: Text("Location",style: swaplistViewLabel,)
                                                                  )
                                                                ],
                                                              ),
                                                            ),
                                                            Padding(
                                                              padding: const EdgeInsets.only(bottom: 3,left: 20),
                                                              child: Row(
                                                                children: <Widget>[
                                                                  Expanded(
                                                                    child: Text(
                                                                      index.location,
                                                                      style: swapRequestText,
                                                                      maxLines: 3,
                                                                      softWrap: true,
                                                                    ),
                                                                  )
                                                                ],
                                                              ),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                      Divider(thickness: 0.2, color: Colors.white,),
                                                      Container(
                                                        child: Column(
                                                          children: <Widget>[
                                                            Padding(
                                                              padding: const EdgeInsets.only(top: 3,bottom: 3.0,left: 20),
                                                              child: Row(
                                                                children: <Widget>[
                                                                  Text("Name",style: swaplistViewLabel,)
                                                                ],
                                                              ),
                                                            ),
                                                            Padding(
                                                              padding: const EdgeInsets.only(bottom: 3,left: 20),
                                                              child: Row(
                                                                children: <Widget>[
                                                                  Text(
                                                                    "${ index.fname} ${ index.lname}",
                                                                    //"Alex Parker",
                                                                    style: swapRequestText,)
                                                                ],
                                                              ),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                      Divider(thickness: 0.2, color: Colors.white,),
                                                      Container(
                                                        child: Column(
                                                          children: <Widget>[
                                                            Padding(
                                                              padding: const EdgeInsets.only(top: 3,bottom: 3,left: 20),
                                                              child: Row(
                                                                children: <Widget>[
                                                                  Text("Make",style: swaplistViewLabel,)
                                                                ],
                                                              ),
                                                            ),
                                                            Padding(
                                                              padding: const EdgeInsets.only(bottom: 3,left: 20),
                                                              child: Row(
                                                                children: <Widget>[
                                                                  Text(
                                                                    index.make,
                                                                    //"Hyundai",
                                                                    style: swapRequestText,)
                                                                ],
                                                              ),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                      Divider(thickness: 0.2, color: Colors.white,),
                                                      Container(
                                                        child: Column(
                                                          children: <Widget>[
                                                            Padding(
                                                              padding: const EdgeInsets.only(top: 3,bottom: 3,left: 20),
                                                              child: Row(
                                                                children: <Widget>[
                                                                  Text("Model",style: swaplistViewLabel,)
                                                                ],
                                                              ),
                                                            ),
                                                            Padding(
                                                              padding: const EdgeInsets.only(bottom: 3,left: 20),
                                                              child: Row(
                                                                children: <Widget>[
                                                                  Text(
                                                                    index.model,
                                                                    //"i20",
                                                                    style: swapRequestText,)
                                                                ],
                                                              ),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                      Divider(thickness: 0.2, color: Colors.white,),
                                                      Container(
                                                        child: Column(
                                                          children: <Widget>[
                                                            Padding(
                                                              padding: const EdgeInsets.only(top: 3,bottom: 3,left: 20),
                                                              child: Row(
                                                                children: <Widget>[
                                                                  Text("Color",style: swaplistViewLabel,)
                                                                ],
                                                              ),
                                                            ),
                                                            Padding(
                                                              padding: const EdgeInsets.only(bottom: 2,left: 20),
                                                              child: Row(
                                                                children: <Widget>[
                                                                  Text(
                                                                    index.color,
                                                                    //"White",
                                                                    style: swapRequestText,)
                                                                ],
                                                              ),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                      Divider(thickness: 0.2, color: Colors.white,),
                                                      Container(
                                                        padding: EdgeInsets.all(7),
                                                        child: Column(
                                                          children: <Widget>[
                                                            Row(
                                                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                              children: <Widget>[
                                                                FlatButton(
                                                                  child: Text(
                                                                    "Accept",
                                                                    style: TextStyle(
                                                                        color: Color.fromRGBO(26,128,246,1)
                                                                    ),
                                                                  ),
                                                                  color: Colors.white,
                                                                  shape: RoundedRectangleBorder(
                                                                      borderRadius:BorderRadius.all(Radius.circular(30)) ),
                                                                  onPressed: () {
                                                                    acceptRequest(parkingId: index.parkingId,othercar: index.carNumber,id: index.id);
                                                                    _swipeKey.currentState.swipeLeft();
                                                                    Navigator.pop(context, "/LocationAfterParking");
                                                                  },
                                                                ),
                                                                FlatButton(
                                                                  child: Text(
                                                                    "Decline",
                                                                    style: TextStyle(
                                                                        color: Colors.white
                                                                    ),
                                                                  ),
                                                                  shape: RoundedRectangleBorder(
                                                                      borderRadius:BorderRadius.all(Radius.circular(30)),
                                                                      side: BorderSide(
                                                                          color: Colors.white,
                                                                          width: 0.5
                                                                      )
                                                                  ),
                                                                  onPressed: (){
                                                                    declineRequest(id: index.id);
                                                                    _swipeKey.currentState.swipeRight();
                                                                  },
                                                                ),
                                                              ],
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                    ],
                                                  )
                                              ),
                                            ),
                                          ],
                                        );
                                    }
                                );

                            }).toList(),
                            padding: EdgeInsets.symmetric(horizontal: 30),
                            visibleCount: 3,
                            translationInterval: 10,
                            maxAngle: 90,
                            threshold: 20,
                            stackFrom: StackFrom.Right,
                          );
                        }
                        else{
                          return Container(
                            child: Column(
                              //  mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Container(
                                      height: MediaQuery.of(context).size.height * 0.7 ,
                                      width:MediaQuery.of(context).size.width * 0.9,
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          Image(
                                            image: AssetImage(
                                              "assets/icn_no_request_available.png",
                                            ),
                                          width:MediaQuery.of(context).size.width * 0.9,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                /*Center(
                                  child: Text(
                                    "No request found"
                                  ),
                                ),*/
                              ],
                            ),
                          );
                        }
                      },
                    );
  }


  forSwapRequestSuccessfullMatch(context ,{var othercar}){
    showModalBottomSheet(
      elevation: 0,
      backgroundColor: Colors.transparent,
      context: context,
      useRootNavigator: true,
      builder: (BuildContext context){
        return GestureDetector(
          onTap: (){
           // Navigator.of(context).pop();
          },
          child: Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.only(top: 20),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30)
                )
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Successfully Match',
                        style: signInBottomThankYou,
                        textAlign: TextAlign.center,
                        softWrap: true,
                        maxLines: 3,
                        //textScaleFactor: 1.5,
                      )
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 25),
                  width: 200,
                  height: 100,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("assets/img_other_user.png"),
                      )
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            child: Text(
                              "$othercar",
                              //'BGY-3891',
                              style: successFullCarNo,
                              textAlign: TextAlign.center,
                              softWrap: true,
                              maxLines: 3,
                              //textScaleFactor: 1.5,
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 25),
                  width: 200,
                  height: 100,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("assets/img_self_user.png"),
                      )
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            child: Text(
                              "$caruser",
                              //'GTK-2099',
                              style: successFullCarNo,
                              textAlign: TextAlign.center,
                              softWrap: true,
                              maxLines: 3,
                              //textScaleFactor: 1.5,
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(5),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Color.fromRGBO(255, 165, 0, 1),
                        borderRadius: BorderRadius.all(Radius.circular(50))
                    ),
                    width: 200,
                    height: 50,

                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        GestureDetector(
                          child: Container(
                            width: 180,
                            decoration: BoxDecoration(
                                border: Border.all(color: Color.fromRGBO(255,255,255,0.1)),
                                borderRadius: BorderRadius.all(Radius.circular(30))
                            ),
                            //padding: EdgeInsets.only(right: 15,left: 20,top: 10,bottom: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Expanded(
                                  child: Text(
                                    "Swap Now",
                                    style: loginSignInButton,
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                Icon(Icons.arrow_forward,color: Colors.white)
                              ],
                            ),
                          ),
                          onTap: (){
                            Navigator.pop(context,true);
                            forSwapRequestSuccessfullySwap(context,othercar: othercar);
                          },
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }

  forSwapRequestSuccessfullySwap(context ,{var othercar}){
    showModalBottomSheet(
      backgroundColor: Colors.transparent,
      context: context,
      builder: (BuildContext context){
        return GestureDetector(
          onTap: (){
            //  Navigator.of(context).pop();
          },
          child: Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.only(top: 20),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30)
                )
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Successfully Swap?',
                        style: signInBottomThankYou,
                        textAlign: TextAlign.center,
                        softWrap: true,
                        maxLines: 3,
                        //textScaleFactor: 1.5,
                      )
                    ],
                  ),
                ),


                Padding(
                  padding: const EdgeInsets.all(5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: 300,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              child: Text(
                                'You have successfully swap with \" $othercar \"',
                                style: signInBottomThankYouSubHeading,
                                textAlign: TextAlign.center,
                                softWrap: true,
                                maxLines: 3,
                                //textScaleFactor: 1.5,
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(5),
                  child: Container(
                    decoration: BoxDecoration(
                      //color: Color.fromRGBO(255, 165, 0, 1),
                        borderRadius: BorderRadius.all(Radius.circular(50)),
                        border: Border.all(
                          color: Color.fromRGBO(255, 165, 0, 1),
                          width: 0.5,
                        )
                    ),
                    width: 200,
                    height: 50,

                    child: FlatButton(
                      shape: OutlineInputBorder(
                          borderSide: BorderSide.none,
                          borderRadius: BorderRadius.all(Radius.circular(50))
                      ),
                      child: Text(
                        'NO',
                        textAlign: TextAlign.center,
                        style: signInBottomThankYouButtonNo,),
                      onPressed: (){
                        Navigator.pop(context);
                      },
                    ),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(5),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Color.fromRGBO(255, 165, 0, 1),
                        borderRadius: BorderRadius.all(Radius.circular(50))
                    ),
                    width: 200,
                    height: 50,

                    child: FlatButton(
                      shape: OutlineInputBorder(
                          borderSide: BorderSide.none,
                          borderRadius: BorderRadius.all(Radius.circular(50))
                      ),
                      child: Text(
                        'Yes',
                        textAlign: TextAlign.center,
                        style: signInBottomThankYouButtonOk,),
                      onPressed: (){
                        Navigator.pop(context);
                      },
                    ),
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }

  acceptRequest({String parkingId,var othercar,String id})async{
    var fcm = await helper.getFcm();
    var res = await acceptRequestMethod(parkingId: parkingId ,id: id);

    if(res["status"] == 1){
      await FirebaseNotifications().sendAndRetrieveMessage(serverToken: fcm);
        forSwapRequestSuccessfullMatch(context,othercar: othercar);
    }else{

    }
  }

  declineRequest({String id})async{
    var res = await declineRequestMethod(id: id);  }

  // Todo: api calling methods

  Future<List<ParkingRequestListModel>> parkingRequestListMethod() async {

    try {
      String oauth = await helper.getOauth();
      HttpService _httpService = HttpService();

      var requestListData = await _httpService.parkingRequestList( token:  oauth  );


      List<ParkingRequestListModel> parkingRequestListModellist =[];

      if(requestListData.statusCode == 200){
        var jsonData = await jsonDecode(requestListData.body);
        for(var i in jsonData["data"]){
          ParkingRequestListModel serlist = ParkingRequestListModel(
            status: jsonData["status"],
            message: jsonData["message"],
            parkingId: i["parking_id"],
            id: i["_id"],
            location: i["address"],
            carNumber: i["car_number"],
            model: i["model"],
            make: i["make"],
            color: i["color"],
            fname: i["first_name"],
            lname: i["last_name"],
          );

          parkingRequestListModellist.add(serlist);
        }

      } else if(requestListData.statusCode == 401){
       /* _flushPopUp(message: Common.authInvalid,
            bgColor: Color.fromRGBO(178,0,0,1),
            duration: Duration(milliseconds: 2500));*/
        Navigator.pushReplacement(context,
          MaterialPageRoute(
            builder: (context) => Login()
          )
        );
      }else{
        _flushPopUp(
            message: Common.serverError,
            bgColor: Color.fromRGBO(178,0,0,1),
            duration: Duration(milliseconds: 2500)
        );
      }



      return parkingRequestListModellist;

    }catch(e){
      print("Exception : >>>>> " + e.toString());
    }
  }

  Future acceptRequestMethod({String parkingId, String id}) async {

    try {
      String oauth = await helper.getOauth();
      HttpService _httpService = HttpService();

      var acceptRequestInfo = await _httpService.acceptRequest( token:  oauth , parkingId: parkingId ,id: id);

      if(acceptRequestInfo.statusCode == 200){
        var jsonData = await jsonDecode(acceptRequestInfo.body);

        return jsonData;

      } else if(acceptRequestInfo.statusCode == 401){
        Navigator.pushReplacement(context,
            MaterialPageRoute(
                builder: (context) => Login()
            )
        );
      }else{
        _flushPopUp(
            message: Common.serverError,
            bgColor: Color.fromRGBO(178,0,0,1),
            duration: Duration(milliseconds: 2500)
        );
      }


    }catch(e){
      print("Exception : >>>>> " + e.toString());
    }
  }

  Future declineRequestMethod({String id}) async {
    try {
      String oauth = await helper.getOauth();
      HttpService _httpService = HttpService();

      var declineRequestInfo = await _httpService.declineRequest( token:  oauth , id: id );

      if(declineRequestInfo.statusCode == 200){
        var jsonData = await jsonDecode(declineRequestInfo.body);

        return jsonData;

      } else if(declineRequestInfo.statusCode == 401){
        Navigator.pushReplacement(context,
            MaterialPageRoute(
                builder: (context) => Login()
            )
        );
      }else{
        _flushPopUp(
            message: Common.serverError,
            bgColor: Color.fromRGBO(178,0,0,1),
            duration: Duration(milliseconds: 2500)
        );
      }

    }catch(e){
      print("Exception : >>>>> " + e.toString());
    }
  }

  // message showing popup

  _flushPopUp({String message ,Color bgColor, Duration duration}){
    Flushbar(
      message:  "${message}",
      flushbarPosition: FlushbarPosition.TOP,
      flushbarStyle: FlushbarStyle.FLOATING,
      borderRadius: 10,
      backgroundColor: bgColor,
      margin: EdgeInsets.all(10),
      duration: duration,
    ).show(context);

  }

}










