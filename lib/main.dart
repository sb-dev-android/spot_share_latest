import 'dart:async';
import 'dart:io';
import 'package:flushbar/flushbar_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:peer_to_peer/Login.dart';
import 'package:peer_to_peer/designModelClass/podoclasses/common.dart';
import 'package:peer_to_peer/home.dart';
import 'package:peer_to_peer/designModelClass/podoclasses/helper.dart';
import 'package:peer_to_peer/designModelClass/podoclasses/internet_connectivity.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flushbar/flushbar.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:notifier/notifier.dart';



void main() {

  WidgetsFlutterBinding.ensureInitialized();
  Crashlytics.instance.enableInDevMode = true;

  // Pass all uncaught errors to Crashlytics.
  FlutterError.onError = Crashlytics.instance.recordFlutterError;

  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown
  ]);

  runZoned(() {
    runApp(NotifierProvider(
      child: MyApp(),
    ));
  }, onError: Crashlytics.instance.recordError);
}




alert({BuildContext context}){
  showDialog(
      context: context,
      builder: (context){
        return AlertDialog(
          title: Text(
              '${Common.internetErrorMessage}'
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Retry'),
              onPressed: (){
                Navigator.pushReplacement(context, MaterialPageRoute(
                    builder: (context)=> MyApp()
                ));
              },
            )
          ],
        );
      }
  );
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  static FirebaseAnalytics analytics = FirebaseAnalytics();
  static FirebaseAnalyticsObserver observer = FirebaseAnalyticsObserver(analytics: analytics);



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarIconBrightness:Brightness.dark,
      ),
    );
    return MaterialApp(
      initialRoute: '/',
      routes: <String, WidgetBuilder> {
        '/login': (BuildContext context) => new Login(),
        '/home': (BuildContext context) => new Home(true),
      },
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: SafeArea(
          top: false,
          bottom: false,
          child: MyHomePage(observer: observer,analytics: analytics,)
      ),

      navigatorObservers: [
        FirebaseAnalyticsObserver(analytics: analytics),
      ],
    );
  }
}






class MyHomePage extends StatefulWidget {

  final FirebaseAnalytics analytics;
  final FirebaseAnalyticsObserver observer;

  MyHomePage({Key key,this.analytics, this.observer}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState(analytics, observer);
}

class _MyHomePageState extends State<MyHomePage> {

  _MyHomePageState(this.analytics, this.observer);

  final FirebaseAnalyticsObserver observer;
  final FirebaseAnalytics analytics;

  var chkOuth ;
  Helper helper = Helper();
  InternetConnectivity _internetConnectivity = InternetConnectivity();
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();


  connectionCheck() async {
    var cnChk = await _internetConnectivity.checkConnectivity();
    if(cnChk){
      //f_base();
      //TODO: chk outh
      getOauth();
      Future.delayed(Duration(seconds: 3),(){ navigate(); });
    }else{
      showDialog(
          context: context,
          builder: (context){
            return AlertDialog(
              title: Text(
                  '${Common.internetErrorMessage}'
              ),
              actions: <Widget>[
                FlatButton(
                  child: Text('Retry'),
                  onPressed: (){
                    Navigator.pushReplacement(context, MaterialPageRoute(
                        builder: (context)=> MyApp()
                    ));
                  },
                )
              ],
            );
          }
      );
    }
  }

  getOauth() async {
    //  String stringValue = prefs.getString('OauthValue');
    String stringValue = await helper.getOauth();
    setState(() {
      chkOuth = stringValue;
    });
  }

  validateAuth() async{
    var valRes = await helper.validateAuth();
    if(valRes['status'] == 1){
      return true;
    }else{
      return false;
    }
  }

  navigate()async{
    var chkValidate = await validateAuth();
    if(chkOuth == null){
      Navigator.pushReplacementNamed(context, '/login');
    }else{

      if(chkValidate){
        Navigator.pushReplacementNamed(context, '/home');
      }else{
        helper.clearOauth();
        Navigator.pushReplacementNamed(context, '/login');
      }
    }
  }








  StreamSubscription<MessageBean> _subscription;

  void _firebasehndleUi(Map<String, dynamic> message) {
     MessageBean item ;
     setState(() {
       item = _itemForMessage(message);
     });
    // Clear away dialogs
    _subscription = item.onChanged.listen((MessageBean item) {
      if (!mounted) {
        _subscription.cancel();
      } else {
        _flushPopUp(
            message: Common.internetErrorMessage,
            bgColor: Colors.white,
            duration: Duration(milliseconds: 2500)
        );
      }
    });
  }
  _flushPopUp({String message ,Color bgColor, Duration duration}){
    Flushbar(
      message:  "${message}",
      flushbarPosition: FlushbarPosition.TOP,
      flushbarStyle: FlushbarStyle.FLOATING,
      borderRadius: 10,
      backgroundColor: bgColor,
      margin: EdgeInsets.all(10),
      duration: duration,
    ).show(context);

  }

  @override
  void initState(){



    //setUpFirebase();
    // TODO: implement initState
    super.initState();

    // new NotificationHandler().initializeFcmNotification();
    connectionCheck();

  }


  @override
  Widget build(BuildContext context) {

    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        //statusBarColor: Colors.white,
        statusBarIconBrightness:Brightness.dark,
      ),
    );

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          key: _scaffoldKey,
          backgroundColor: Colors.white,
          body: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image:
                    AssetImage("assets/Splash.png"),
                    fit: BoxFit.cover
                )
            ),
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
          )
      ),
    );
  }
}



final Map<String, MessageBean> _items = <String, MessageBean>{};
MessageBean _itemForMessage(Map<String, dynamic> message) {
  //If the message['data'] is non-null, we will return its value, else return map message object
  final dynamic data = message['data'] ?? message;
  final String itemId = data['id'];
  final MessageBean item = _items.putIfAbsent(
      itemId, () => MessageBean(itemId: itemId))
    ..status = data['status'];
  return item;
}

//Model class to represent the message return by FCM
class MessageBean {
  MessageBean({this.itemId});
  final String itemId;

  StreamController<MessageBean> _controller =
  StreamController<MessageBean>.broadcast();
  Stream<MessageBean> get onChanged => _controller.stream;

  String _status;
  String get status => _status;
  set status(String value) {
    _status = value;
    _controller.add(this);
  }

  static final Map<String, Route<void>> routes = <String, Route<void>>{};
  Route<void> get route {
    final String routeName = '/detail/$itemId';
    return routes.putIfAbsent(
      routeName,
          () => MaterialPageRoute<void>(
        settings: RouteSettings(name: routeName),
        builder: (BuildContext context) => AlertDialog(
          title: Text("welcome alert"),
        ),
      ),
    );
  }
}




class ShowNotificationIcon {

  static void show(BuildContext context) async {
    OverlayState overlayState = Overlay.of(context);
    OverlayEntry overlayEntry = new OverlayEntry(builder: _build);

    overlayState.insert(overlayEntry);

    await new Future.delayed(const Duration(seconds: 2));

    overlayEntry.remove();
  }

  static Widget _build(BuildContext context){
    return new Positioned(
      top: 50.0,
      left: 50.0,
      child: new Material(
        color: Colors.transparent,
        child: new Icon(Icons.warning, color: Colors.purple),
      ),
    );
  }
}




