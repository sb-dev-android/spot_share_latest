import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'dart:async' show Future;
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:peer_to_peer/designModelClass/podoclasses/models.dart';
import 'package:peer_to_peer/rate_us.dart';
import 'stylesheet.dart';
import 'savedLocation.dart';
import 'package:peer_to_peer/designModelClass/animationfile.dart';
import 'home.dart';
import 'history.dart';
import 'package:peer_to_peer/designModelClass/slidershape.dart';
import 'package:peer_to_peer/designModelClass/DialogBox.dart';
import 'package:peer_to_peer/designModelClass/podoclasses/helper.dart';
import 'package:latlong/latlong.dart' as ltlg;
import 'package:geocoder/geocoder.dart';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'dart:io' as platform;
import 'package:peer_to_peer/designModelClass/podoclasses/common.dart';
import 'package:peer_to_peer/designModelClass/circular_animation.dart';
import 'package:flutter/cupertino.dart';
import 'package:peer_to_peer/designModelClass/podoclasses/location_permission.dart';
import 'package:location/location.dart' as loc;
import 'package:flutter/foundation.dart';
import 'package:animating_location_pin/animating_location_pin.dart';
import 'designModelClass/customAppbar.dart' as apbar;
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'designModelClass/podoclasses/google_prediction_api.dart';
import 'package:peer_to_peer/designModelClass/podoclasses/maprouteservices.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flushbar/flushbar.dart';
import 'designModelClass/podoclasses/httpservices.dart';
import 'package:peer_to_peer/Login.dart';
import 'package:peer_to_peer/designModelClass/podoclasses/internet_connectivity.dart';
import 'package:connectivity/connectivity.dart';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';

import 'designModelClass/crashTesting/appbaranimation/my_painter.dart';


var lattitudeStore;
var longitudeStore;
bool faverColor = false;

bool isFirstTap = false;

class Location extends StatefulWidget {

  String savedLocationDestination;
  final bool status;
  bool chkBkmark;
  String bookmarkId;

  final FirebaseAnalytics analytics;
  final FirebaseAnalyticsObserver observer;

  Location(this.status,{
    this.savedLocationDestination,
    this.chkBkmark,this.bookmarkId,
    this.analytics, this.observer
  });
  @override
  _LocationState createState() => _LocationState(analytics, observer);
}

class _LocationState extends State<Location> with SingleTickerProviderStateMixin{

  _LocationState(this.analytics, this.observer);
  final FirebaseAnalyticsObserver observer;
  final FirebaseAnalytics analytics;
  final Connectivity _connectivity = Connectivity();
  static StreamSubscription<ConnectivityResult> connectivitySubscriptionVariable;

  String tempBookmarkId;
  InternetConnectivity _internetConnectivity = InternetConnectivity();
  FocusNode _typeHeadeFocusNode = new FocusNode();
  Helper helper = Helper();
  GoogleMapsServices googleMapsServices = GoogleMapsServices();
  static PermissionsService permissionsService = PermissionsService();
  bool _serviceEnabled;
  loc.Location location = new loc.Location();
  loc.LocationData _locationData;
  StreamSubscription<loc.LocationData> _locationSubscription;

  permissiobMethod() async {
    _serviceEnabled = await location.serviceEnabled();
    if(_serviceEnabled){
      curPosition();
    }else{
      var permissionCheck = await permissionsService.requestLocationPermission();
      if(permissionCheck){
        curPosition();
      }
    }
  }


  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  Completer<GoogleMapController> _mapController = Completer();
  GoogleMapController mapController;

  Position _curPosition;
  LatLng _latLng ;

  static LatLng  selectedCarLocation;
  static var radiusValue ;


  Set<Marker> _markers = {};
  Set<Marker> _customMarkers = {};
  final Set<Circle> _circleLocation = {};
  // this will hold the generated polylines
    Set<Polyline> _polyLines = {};
    Set<Polyline> get polyLines => _polyLines;
  // this will hold each polyline coordinate as Lat and Lng pairs
    List<LatLng> polylineCoordinates = [];
  // this is the key object - the PolylinePoints
  // which generates every polyline between start and finish
    PolylinePoints polylinePoints = PolylinePoints();

// addresss  variable
  bool searchBar = true;
  var searchAddress;


  bool search = false;
  TextEditingController _typeAheadController = TextEditingController();

  double initvalue = 1;
  var radiovalue= 2;

  List<LatLng> latLngList =<LatLng>[] ;

  bool searchParking = false;
//-------user info
  static var userCarNo;
  static var userName;
  bool _showSecond = false;

  setUserInfo()async {
    var car = await helper.getCarUser();
    var uname = await helper.getUserName();
    setState(() {
      userCarNo = car;
      userName = uname;
    });
  }

  Future<bool> onWillPop() {
    return Future.value(true);
  }

  setIniTialCameraPosition() async{
    BitmapDescriptor userMarker = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(size: Common.customMrkerSize),
        'assets/2.0x/df.png')
        .then((onValue) {
      return onValue;
    });
    LatLng lastlatlng = await helper.getLastMapLocation();

    if(lastlatlng != null){
      _latLng = lastlatlng;
     /* setState(() {
        _markers.add(Marker(
            markerId: MarkerId(_curPosition.toString()),
            position: LatLng(lastlatlng.latitude,lastlatlng.longitude),
            onTap: (){},
            icon: userMarker
        ));
      });*/
      } else{
      _latLng = Common.newYorklatlng;
    }
  }

  getCoordinateOfAddress({String qry}) async{

    _curPosition = await Geolocator().getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    setState(() {
      faverColor = true;
    });

    var addresses = await Geocoder.local.findAddressesFromQuery(qry);
    var first = addresses.first;

    setState(() {
      lattitudeStore = first.coordinates.latitude;
      longitudeStore = first.coordinates.longitude;
    });

    animateCamera(latLng: LatLng(first.coordinates.latitude,first.coordinates.longitude));
    _onAddMarkerButtonPressed(latlang: LatLng(first.coordinates.latitude,first.coordinates.longitude));
    _onAddCircle(latlang: LatLng(first.coordinates.latitude,first.coordinates.longitude) );
    setAppbarTitle(latLng: LatLng(first.coordinates.latitude,first.coordinates.longitude));
  }

  resetfever(){
    setState(() {
      faverColor = false;
    });
  }

  List<dynamic> _placePredictions = [];


 Widget _appBar(){
   return
     search
         ? AppbarAnimation(
       appbar: apbar.AppBar(
         elevation: Common.elevation,
         automaticallyImplyLeading: false,
         titleSpacing: NavigationToolbar.kMiddleSpacing,
         backgroundColor: Colors.white,
         title: Container(
           padding: EdgeInsets.all(1),
           height: 45,
           decoration: BoxDecoration(
               borderRadius: BorderRadius.all(Radius.circular(50)),
               border: Border.all(
                   color: Colors.black,
                   width: 0.2
               )
           ),
           child: Row(
             mainAxisAlignment: MainAxisAlignment.center,
             children: <Widget>[
               Padding(
                 padding: const EdgeInsets.only(left:10),
                 child: Image(
                   image: AssetImage(
                       "assets/icn_search_home.png"
                   ),
                 ),
               ),
               Flexible(
                   child: /*serhelper.CustomSearchScaffold()*/
                   TypeAheadField(
                     hideOnLoading: true,
                     keepSuggestionsOnLoading: false,
                     getImmediateSuggestions: false,
                     hideOnEmpty: true,
                     animationDuration: Duration(seconds: 0),
                     textFieldConfiguration: TextFieldConfiguration(
                         focusNode: _typeHeadeFocusNode,
                         textInputAction: TextInputAction.search,
                         decoration: InputDecoration(
                             isDense: true,
                             hintText: 'Search Location...',
                             contentPadding: EdgeInsets.only(left:5,bottom: 0),
                             border: OutlineInputBorder(
                                 borderSide: BorderSide.none
                             )
                         ),
                         controller: this._typeAheadController
                     ),
                     suggestionsCallback: (pattern) async {
                       // Prediction p = await PlacesAutocomplete.show(context: context, apiKey: Common.googleMapKey);
                       // displayPrediction(p);
                       Completer<List<String>> completer = new Completer();
                       _autocompletePlace(_typeAheadController.text);
                       List<Place> _plList  = [];
                       if (_placePredictions.length > 0)
                         for (var prediction in _placePredictions)
                         {
                           _plList.add(Place.fromJSON(prediction));
                         }

                       List<String> lst = [];
                       for(var i in _plList){
                         lst.add(i.description);
                       }
                       completer.complete(lst);
                       return completer.future;
                     },
                     itemBuilder: (context, suggestion){
                       if(_typeAheadController.text == null){
                         return null;
                       }
                       return Container(
                         width: MediaQuery.of(context).size.width * 0.8,
                         child: ListTile(
                           title: Text(suggestion),
                         ),
                       );
                     },
                     hideSuggestionsOnKeyboardHide: true,
                     onSuggestionSelected: (suggestion) {
                       this._typeAheadController.text = suggestion;
                       setState(() {
                         Common.lookingForParkingHeading = suggestion;
                       });

                       setState(() {
                         Common.lookingForParkingHeading = _typeAheadController.text;
                         searchAddress = _typeAheadController.text;
                         search=false;
                         faverColor = false;
                         _markers.clear();
                         _circleLocation.clear();
                         latLngList.clear();
                         _customMarkers.clear();
                         faverColor = false;
                       });
                       searchAddressNavigate();
                       this._typeAheadController.clear();
                     },
                     suggestionsBoxDecoration: SuggestionsBoxDecoration(
                       color: Colors.white,
                     ),
                   )
               ),
               IconButton(
                 icon: Image(
                   image: AssetImage(
                       "assets/icn_arrow_next.png"
                   ),
                 ),
                 splashColor: Colors.transparent,
                 highlightColor: Colors.transparent,
                 hoverColor: Colors.transparent,
                 onPressed: (){
                   if(_typeAheadController.text.trim().length != 0){
                     setState(() {
                       setState(() {
                         Common.lookingForParkingHeading = _typeAheadController.text;
                         searchAddress = _typeAheadController.text;
                         search=false;
                         faverColor = false;
                         _markers.clear();
                         _circleLocation.clear();
                         latLngList.clear();
                         _customMarkers.clear();
                         faverColor = false;
                       });
                       searchAddressNavigate();
                       _typeAheadController.clear();
                     });
                   }
                   else{
                     setState(() {
                       Common.lookingForParkingHeading = "Looking for Parking";
                       //searchAddress = _typeAheadController.text;
                       search=false;
                       faverColor = false;
                       _markers.clear();
                       _circleLocation.clear();
                       latLngList.clear();
                       _customMarkers.clear();
                       faverColor = false;
                     });
                     // searchAddressNavigate();
                     _typeAheadController.clear();
                   }
                 },
               )
             ],
           ),
         ),
         actions: <Widget>[
           IconButton(
             icon: Image(
               image: AssetImage(
                   "assets/icn_close.png"
               ),
             ),
             splashColor: Colors.transparent,
             highlightColor: Colors.transparent,
             hoverColor: Colors.transparent,
             onPressed: (){
               cancelSearch();
               setState(() {
                 search = false;
               });
             },
           )
         ],
       ),
     )
         :AppbarAnimation(
       appbar: apbar.AppBar(
         elevation: Common.elevation,
         backgroundColor: Colors.white,
         leading: IconButton(
           icon:Image(
             image: AssetImage(
                 "assets/icn_back.png"
             ),
           ),
           onPressed: (){
             Navigator.of(context).pop();
           },
         ),
         centerTitle: true,
         title: Text( '${Common.lookingForParkingHeading}',style: appbarHeading,),
         actions: <Widget>[
           GestureDetector(
             child: IconButton(
               splashColor: Colors.transparent,
               highlightColor: Colors.transparent,
               hoverColor: Colors.transparent,
               icon: Image(
                 image: AssetImage(
                     "assets/icn_search_home.png"
                 ),
               ),
               onPressed: (){
                 setState(() {
                   search = true;
                 });
               },
             ),
             onTapUp: onSearchTapUp,
           ),
           IconButton(
             icon: faverColor
                 ? Icon( Icons.star, color: Colors.green[300], size: 35,)
                 : Image(  image: AssetImage(  "assets/icn_favorite.png" ),
               color: Colors.black, ),
             onPressed: (){
               faverColor
                   ? widget.bookmarkId != null
                   ? deleteFaveriouteLocationMthod(bookmarkId: widget.bookmarkId)
                   : deleteFaveriouteLocationMthod(bookmarkId: tempBookmarkId)
                   : addFaveriouteLocationMethod();
             },
           ),
         ],
       ),
     );

     search
       ? apbar.AppBar(
     elevation: Common.elevation,
     automaticallyImplyLeading: false,
     titleSpacing: NavigationToolbar.kMiddleSpacing,
     backgroundColor: Colors.white,
     title: Container(
       padding: EdgeInsets.all(1),
       height: 45,
       decoration: BoxDecoration(
           borderRadius: BorderRadius.all(Radius.circular(50)),
           border: Border.all(
               color: Colors.black,
               width: 0.2
           )
       ),
       child: Row(
         mainAxisAlignment: MainAxisAlignment.center,
         children: <Widget>[
           Padding(
             padding: const EdgeInsets.only(left:10),
             child: Image(
               image: AssetImage(
                   "assets/icn_search_home.png"
               ),
             ),
           ),
           Flexible(
               child: /*serhelper.CustomSearchScaffold()*/
               TypeAheadField(
                 hideOnLoading: true,
                 keepSuggestionsOnLoading: false,
                 getImmediateSuggestions: false,
                 hideOnEmpty: true,
                 animationDuration: Duration(seconds: 0),
                 textFieldConfiguration: TextFieldConfiguration(
                     focusNode: _typeHeadeFocusNode,
                     textInputAction: TextInputAction.search,
                     decoration: InputDecoration(
                         isDense: true,
                         hintText: 'Search Location...',
                         contentPadding: EdgeInsets.only(left:5,bottom: 0),
                         border: OutlineInputBorder(
                             borderSide: BorderSide.none
                         )
                     ),
                     controller: this._typeAheadController
                 ),
                 suggestionsCallback: (pattern) async {
                   // Prediction p = await PlacesAutocomplete.show(context: context, apiKey: Common.googleMapKey);
                   // displayPrediction(p);
                   Completer<List<String>> completer = new Completer();
                   _autocompletePlace(_typeAheadController.text);
                   List<Place> _plList  = [];
                   if (_placePredictions.length > 0)
                     for (var prediction in _placePredictions)
                     {
                       _plList.add(Place.fromJSON(prediction));
                     }

                   List<String> lst = [];
                   for(var i in _plList){
                     lst.add(i.description);
                   }
                   completer.complete(lst);
                   return completer.future;
                 },
                 itemBuilder: (context, suggestion){
                   if(_typeAheadController.text == null){
                     return null;
                   }
                   return Container(
                     width: MediaQuery.of(context).size.width * 0.8,
                     child: ListTile(
                       title: Text(suggestion),
                     ),
                   );
                 },
                 hideSuggestionsOnKeyboardHide: true,
                 onSuggestionSelected: (suggestion) {
                   this._typeAheadController.text = suggestion;
                   setState(() {
                     Common.lookingForParkingHeading = suggestion;
                   });

                   setState(() {
                     Common.lookingForParkingHeading = _typeAheadController.text;
                     searchAddress = _typeAheadController.text;
                     search=false;
                     faverColor = false;
                     _markers.clear();
                     _circleLocation.clear();
                     latLngList.clear();
                     _customMarkers.clear();
                     faverColor = false;
                   });
                   searchAddressNavigate();
                   this._typeAheadController.clear();
                 },
                 suggestionsBoxDecoration: SuggestionsBoxDecoration(
                   color: Colors.white,
                 ),
               )
           ),
           IconButton(
             icon: Image(
               image: AssetImage(
                   "assets/icn_arrow_next.png"
               ),
             ),
             splashColor: Colors.transparent,
             highlightColor: Colors.transparent,
             hoverColor: Colors.transparent,
             onPressed: (){
               if(_typeAheadController.text.trim().length != 0){
                 setState(() {
                   setState(() {
                     Common.lookingForParkingHeading = _typeAheadController.text;
                     searchAddress = _typeAheadController.text;
                     search=false;
                     faverColor = false;
                     _markers.clear();
                     _circleLocation.clear();
                     latLngList.clear();
                     _customMarkers.clear();
                     faverColor = false;
                   });
                   searchAddressNavigate();
                   _typeAheadController.clear();
                 });
               }
               else{
                 setState(() {
                   Common.lookingForParkingHeading = "Looking for Parking";
                   //searchAddress = _typeAheadController.text;
                   search=false;
                   faverColor = false;
                   _markers.clear();
                   _circleLocation.clear();
                   latLngList.clear();
                   _customMarkers.clear();
                   faverColor = false;
                 });
                 // searchAddressNavigate();
                 _typeAheadController.clear();
               }
             },
           )
         ],
       ),
     ),
     actions: <Widget>[
       IconButton(
         icon: Image(
           image: AssetImage(
               "assets/icn_close.png"
           ),
         ),
         splashColor: Colors.transparent,
         highlightColor: Colors.transparent,
         hoverColor: Colors.transparent,
         onPressed: (){
           setState(() {
             search = false;
           });
         },
       )
     ],
   )
       : apbar.AppBar(
     elevation: Common.elevation,
     backgroundColor: Colors.white,
     leading: IconButton(
       icon:Image(
         image: AssetImage(
             "assets/icn_back.png"
         ),
       ),
       onPressed: (){
         Navigator.of(context).pop();
       },
     ),
     centerTitle: true,
     title: Text( '${Common.lookingForParkingHeading}',style: appbarHeading,),
     actions: <Widget>[
       IconButton(
         splashColor: Colors.transparent,
         highlightColor: Colors.transparent,
         hoverColor: Colors.transparent,
         icon: Image(
           image: AssetImage(
               "assets/icn_search_home.png"
           ),
         ),
         onPressed: (){
           setState(() {
             search = true;
           });
         },
       ),
       IconButton(

         icon: faverColor
             ? Icon( Icons.star, color: Colors.green[300], size: 35,)
             : Image(  image: AssetImage(  "assets/icn_favorite.png" ),
           color: Colors.black, ),
         onPressed: (){
           faverColor
               ? widget.bookmarkId != null
               ? deleteFaveriouteLocationMthod(bookmarkId: widget.bookmarkId)
               : deleteFaveriouteLocationMthod(bookmarkId: tempBookmarkId)
               : addFaveriouteLocationMethod();
         },
       ),
     ],
   );
 }

  Widget _android_View(){
    return MaterialApp(
      routes: <String, WidgetBuilder> {
        '/Home': (BuildContext context) =>  Home(widget.status),
        '/History': (BuildContext context) =>  History(widget.status),
      },

      debugShowCheckedModeBanner: false,
      home: WillPopScope(
        onWillPop: () async{
          Navigator.pushReplacementNamed(context,'/Home');
          return;
        },
        child: SafeArea(
          top: false,
          bottom: false,
          child: Scaffold(
            //backgroundColor: Colors.white,
              key: _scaffoldKey,

              appBar: _appBar(),

              body: _curPosition == null
                  ? Center(
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      AnimatingLocationPin(),
                      Text('Searching your location...')
                    ],
                  ),
                ),
              )
                  : Container(
                child: Stack(
                  children: <Widget>[
                    _googleMap(),
                    // _searchcurrentLocationwidget()

                    _searchcurrentLocationwidget2()
                  ],
                ),
              )
          ),
        ),
      ),
    );

  }


  /*bool myInterceptor(bool stopDefaultButtonEvent) {
    print("BACK BUTTON!"); // Do some stuff.
    return true;
  }*/


  @override
  void initState() {
    super.initState();
    connectionCheck();
    connectivitySubscription();
   // gpsAccess();
    setIniTialCameraPosition();
    permissiobMethod();
    widget.savedLocationDestination != null
        ? getCoordinateOfAddress(qry: widget.savedLocationDestination) :resetfever();
    setUserInfo();

    _locationSubscription = location.onLocationChanged()
        .listen((loc.LocationData currentLocation) async {

        _serviceEnabled = await location.serviceEnabled();
      if(_serviceEnabled){
        _locationData = currentLocation;
        print('${_locationData.toString()}');
      }else{
       // permissiobMethod();

        showDialog(
            context: context,
            builder: (context){
              return AlertDialog(
                title: Text(
                    '${Common.locationMandatory}'
                ),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Enable'),
                    onPressed: (){
                     /* Navigator.pushReplacement(context, MaterialPageRoute(
                          builder: (context)=> Location(true)
                      ));*/
                     Navigator.pop(context);
                    },
                  )
                ],
              );
            }
        );

      }
    });
  }

  Widget _googleMap(){
    return  Container(
      child:
      onMapCreated != null
          ? GoogleMap(
        markers: searchParking
            ? Set<Marker>.of(_customMarkers) : Set<Marker>.of(_markers),
        circles: Set.from(_circleLocation),
        polylines: _polyLines,
        mapType: MapType.normal,
        zoomGesturesEnabled: true,
        scrollGesturesEnabled: true,
        compassEnabled: true,
        myLocationButtonEnabled: false,
        rotateGesturesEnabled: true,
        tiltGesturesEnabled: false,
        initialCameraPosition:CameraPosition(
            target: _latLng,
            zoom: Common.zoomCamera
        ),
        onMapCreated: onMapCreated,
        onCameraMove: (value){
          setState(() {
            Common.zoomCamera = value.zoom;
          });
        },
        // onTap: (LatLng location) async {  },
        onLongPress: (LatLng location) async{
          if(isFirstTap)
          {
            setAppbarTitle(latLng: location);

            if(_markers.length>=2 && _circleLocation.isNotEmpty)
            {
              _markers.clear();
              _circleLocation.clear();
              latLngList.clear();
              _customMarkers.clear();
            }
            setState(() {
              lattitudeStore = location.latitude;
              longitudeStore = location.longitude;
              searchParking = false;
              faverColor = false;
            });

            animateCamera( latLng: location);
            _onAddCircle(latlang: location);
            _onAddMarkerButtonPressed(latlang: location );
          }
          else{
            setState(() {
              isFirstTap = true;
            });
          }
        }
      )
          :CircularProgressIndicator(),
    );
  }

  Widget _searchcurrentLocationwidget(){
    return Align(
      alignment: Alignment.bottomCenter,
      child: Padding(
          padding: const EdgeInsets.only(bottom: 20),
          child:
          platform.Platform.isIOS
              ?  GestureDetector(
            onTap: (){
              animateCamera(latLng: LatLng(_curPosition.latitude,_curPosition.longitude));
            },
            child: Container(
              width: 215,
              height: 50,
              //padding: EdgeInsets.all(12),
              decoration: BoxDecoration(
                  color: Color.fromRGBO(28, 129, 247, 1),
                  borderRadius: BorderRadius.all(Radius.circular(30))
              ),
              child: CupertinoActionSheetAction(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: Image(
                        image: AssetImage(
                            "assets/icn_search_location.png"
                        ),
                      ),
                    ),
                    Text(
                      "${Common.searchCurrentLocation}",
                      style: locationSearchLocationButton,
                    ),
                  ],
                ),
                onPressed: (){
                  animateCamera(latLng: LatLng(_curPosition.latitude,_curPosition.longitude));
                },
              ),
            ),
          )
              :  GestureDetector(
            behavior: HitTestBehavior.deferToChild,
            onTap: (){
              animateCamera(latLng: LatLng(_curPosition.latitude,_curPosition.longitude));
            },
            child: Container(
              width: 205,
              padding: EdgeInsets.all(12),
              decoration: BoxDecoration(
                  color: Color.fromRGBO(28, 129, 247, 1),
                  borderRadius: BorderRadius.all(Radius.circular(30))
              ),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 5),
                    child: Image(
                      image: AssetImage(
                          "assets/icn_search_location.png"
                      ),
                    ),
                  ),
                  Text(
                    "Search Current Location",
                    style: locationSearchLocationButton,
                  ),
                ],
              ),
            ),
          )
      ),
    );
  }

  Widget _searchcurrentLocationwidget2(){
    return Align(
      alignment: Alignment.bottomCenter,
      child: Padding(
          padding: const EdgeInsets.only(bottom: 20),
          child:
          platform.Platform.isIOS
              ?  FlatButton(
            child: Container(
              width: 215,
              height: 50,
              //padding: EdgeInsets.all(12),
              decoration: BoxDecoration(
                  color: Color.fromRGBO(28, 129, 247, 1),
                  borderRadius: BorderRadius.all(Radius.circular(30))
              ),
              child: CupertinoActionSheetAction(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: Image(
                        image: AssetImage(
                            "assets/icn_search_location.png"
                        ),
                      ),
                    ),
                    Text(
                      "${Common.searchCurrentLocation}",
                      style: locationSearchLocationButton,
                    ),
                  ],
                ),
                onPressed: (){
                  animateCamera(latLng: LatLng(_curPosition.latitude,_curPosition.longitude));
                },
              ),
            ),
            onPressed:  (){
              animateCamera(latLng: LatLng(_curPosition.latitude,_curPosition.longitude));
            },
          )
              :  FlatButton(
            child: Container(
              width: 205,
              padding: EdgeInsets.all(12),
              decoration: BoxDecoration(
                  color: Color.fromRGBO(28, 129, 247, 1),
                  borderRadius: BorderRadius.all(Radius.circular(30))
              ),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 5),
                    child: Image(
                      image: AssetImage(
                          "assets/icn_search_location.png"
                      ),
                    ),
                  ),
                  Text(
                    "Search Current Location",
                    style: locationSearchLocationButton,
                  ),
                ],
              ),
            ),
            onPressed: (){
              animateCamera(
                  latLng: LatLng(_curPosition.latitude,_curPosition.longitude)
              );
            },
          )
      )
    );
  }


  @override
  Widget build(BuildContext context) {

    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarIconBrightness:Brightness.dark,
        statusBarColor: Colors.black,
      ),
    );
    return
    //  platform.Platform.isIOS
    //  ?   _iOS_View()
        _android_View();
  }

  void _autocompletePlace(String input) async {
    /// Will be called everytime the input changes. Making callbacks to the Places
    /// Api and giving the user Place options

    if (input.length > 0) {
      String url =
          "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=$input&key=${Common.googleMapKey}&language=en";
        url += "&location=${_curPosition.latitude},${_curPosition.longitude}&radius=20000";
          url += "&strictbounds";
      final response = await http.get(url);
      final json = jsonDecode(response.body);

      if (json["error_message"] != null) {
        var error = json["error_message"];
        if (error == "This API project is not authorized to use this API.")
          error += " Make sure the Places API is activated on your Google Cloud Platform";
        throw Exception(error);
      } else {
        final predictions = json["predictions"];
        setState(() => _placePredictions = predictions);
      }
    } else {
      //await _animationController.animateTo(0.5);
      setState(() => _placePredictions = []);
      //await _animationController.reverse();
    }
  }

  _flushPopUp({String message ,Color bgColor, Duration duration}){
    Flushbar(
      message:  "${message}",
      flushbarPosition: FlushbarPosition.TOP,
      flushbarStyle: FlushbarStyle.FLOATING,
      borderRadius: 10,
      backgroundColor: bgColor,
      margin: EdgeInsets.all(10),
      duration: duration,
    ).show(context);

  }

  searchAddressNavigate(){
    if(_markers.length>=2 && _circleLocation.length>=1)
    {
      _markers.clear();
      _circleLocation.clear();
      latLngList.clear();
      _customMarkers.clear();
      faverColor = false;
      searchParking = false;
    }

    Geolocator().placemarkFromAddress(searchAddress).then(
            (result) async{
          animateCamera( latLng:LatLng(result[0].position.latitude, result[0].position.longitude));
          _onAddCircle(
              latlang: LatLng(result[0].position.latitude, result[0].position.longitude));
          _onAddMarkerButtonPressed(
              latlang:LatLng(
                  result[0].position.latitude,
                  result[0].position.longitude));
          setState(() {
            lattitudeStore = result[0].position.latitude;
            longitudeStore = result[0].position.longitude;
          });
        }
    );
  }

 /* upadateRangeValues({var rad}){
    setState(() {
      if(rad == 100){
        setState(() {
          Common.zoomCamera = 19.327394485473633;
        });
      }else if(rad == 200){
        setState(() {
          Common.zoomCamera = 18.265888214111328;
        });
      }else if(rad == 300){
        setState(() {
          Common.zoomCamera = 17.760255813598633;
        });
      }else if(rad == 400){
        setState(() {
          Common.zoomCamera = 17.386709213256836;
        });
      }else if(rad == 500){
        setState(() {
          Common.zoomCamera = 16.99898338317871;
        });
      }else{
        // if radius is 600
        setState(() {
          Common.zoomCamera = 16.87555694580078;
        });
      }
    });
  }*/

  changeIcon(){
    setState(() {
      if(_showSecond){
        setState(()=> _showSecond = false);
      }else{
        {
          setState(()=> _showSecond = true);
        }
      }
    });
  }

  Widget _bottomAnim({LatLng latLng}){
    return BottomAnim(
      latLng: latLng,
      historyCallback: (){
        Navigator.of(context).push(
            MaterialPageRoute(
                builder: (context)=>History(widget.status)
            )
        );
      },
      savedLocationCallback: (fever) async{

        faverColor
            ? widget.bookmarkId != null
            ? deleteFaveriouteLocationMthod(bookmarkId: widget.bookmarkId)
            : deleteFaveriouteLocationMthod(bookmarkId: tempBookmarkId)
            : addFaveriouteLocationMethod();
      },
    );
  }

  _bottomPersistentSheet({LatLng latLng}){
    _scaffoldKey.currentState.showBottomSheet<void>((BuildContext context) {
      return
        GestureDetector(
          onTap: (){  Navigator.pop(context); },
          child: SafeArea(
            child: Scaffold(
                backgroundColor: Colors.transparent,
                bottomNavigationBar:Container(
                  //height: 410,
                  decoration: BoxDecoration(
                    //color: Color.fromRGBO(28, 129, 245, 1),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(25),
                          topRight: Radius.circular(25)
                      )
                  ),
                  child: GestureDetector(
                    onTap: (){
                      Navigator.pop(context);
                    },
                    child: Container(
                      child: Stack(
                        children: <Widget>[
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: GestureDetector(
                              onTap: (){},
                              child: Container(
                                height:210,
                                color: Color.fromRGBO(28, 129, 245, 1),
                                child: Align(
                                  alignment: Alignment.bottomCenter,
                                  child: Container(
                                    padding: EdgeInsets.only(bottom: 20),
                                    width: MediaQuery.of(context).size.width,
                                    //height: MediaQuery.of(context).size.height/2,
                                    height:200,
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(25),
                                            topRight: Radius.circular(25)
                                        )
                                    ),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.all(20),
                                              child: Text("Select Radius",style: listViewLabelText,),
                                            ),
                                          ],
                                        ),

                                        Padding(
                                          padding: const EdgeInsets.symmetric(horizontal: 12),
                                          child: Column(
                                            children: <Widget>[
                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: <Widget>[
                                                  Container(
                                                    child: Text("Block"),
                                                    margin: EdgeInsets.only(left: 10),
                                                  ),
                                                  Flexible(
                                                      child:
                                                      RadiusSelection(onChanged: (val){
                                                        print('Radio value >>>>> $val');
                                                        //  upadateRangeValues(rad: val *100);
                                                        changeRadiusMethod(val);
                                                        _onAddCircle(latlang: LatLng(lattitudeStore,longitudeStore));
                                                      },)
                                                  )
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),

                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            Container(
                                              decoration: BoxDecoration(
                                                  color: Color.fromRGBO(255, 165, 0, 1),
                                                  borderRadius: BorderRadius.all(Radius.circular(50))
                                              ),
                                              child: FlatButton(
                                                child: Padding(
                                                  padding: const EdgeInsets.only(left:8.0,right: 8.0),
                                                  child: Row(
                                                    children: <Widget>[
                                                      Text("Search Parking ",style: bottomSheetMyLocationRadiusButton,),
                                                      Icon(Icons.arrow_forward,color: Colors.white,)
                                                    ],
                                                  ),
                                                ),
                                                onPressed: (){
                                                  setState(() {
                                                    searchParking = true;
                                                  });
                                                  _addMarkingAvailableParking();
                                                  loadingScreen();
                                                  Future.delayed(Duration(seconds: 3)).whenComplete((){
                                                    Navigator.pop(context);
                                                  });
                                                },
                                              ),
                                            )
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          _bottomAnim(latLng: latLng)
                        ],
                      ),
                    ),
                  ),
                )
            ),
          ),
        );
    },
        backgroundColor: Colors.transparent
    );

  }

  _bottomPersistentSheet2(){
    _scaffoldKey.currentState.showBottomSheet<void>((BuildContext context) {
      return
        GestureDetector(
          onTap: (){
            Navigator.pop(context);
          },
          child: Scaffold(
            backgroundColor: Colors.transparent,
            bottomNavigationBar: Container(
              height: platform.Platform.isIOS ? 90:80,
              child: GestureDetector(
                onTap: (){  },
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      color: Color.fromRGBO(28, 129, 245, 1),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(25),
                          topRight: Radius.circular(25)
                      )
                  ),
                  child: Column(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(left: 15,right: 15,top: 15,bottom: 10),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(2),
                                  child: Container(
                                    padding: EdgeInsets.only(left: 5),
                                    child: Text(
                                      '${Common.myLocationString}',
                                      style: bottomSheetMyLocation,
                                    ),
                                  ),
                                ),
                                Container(
                                  //width: MediaQuery.of(context).size.width * 0.4,
                                  //padding: EdgeInsets.all(),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      IconButton(
                                        icon: faverColor
                                            ? Icon( Icons.star, color: Colors.green[300], size: 35,)
                                            : Image(  image: AssetImage(  "assets/icn_favorite.png" ),
                                          color: Colors.white,  ),
                                        onPressed: () async{

                                          faverColor
                                              ? widget.bookmarkId != null
                                              ? deleteFaveriouteLocationMthod(bookmarkId: widget.bookmarkId)
                                              : deleteFaveriouteLocationMthod(bookmarkId: tempBookmarkId)
                                              : addFaveriouteLocationMethod();
                                         /*
                                          if(faverColor){
                                            _flushPopUp(message: Common.underDevelopment,bgColor: Colors.black,duration: Duration(milliseconds: 2500));
                                          }else{
                                          var chkFuture = await bookmarkAddApi(latLng: LatLng(lattitudeStore,longitudeStore));
                                          if(chkFuture){
                                            setState(() {
                                              faverColor= true;
                                            });
                                              _savedLocation();
                                          }
                                        }*/
                                        }
                                      ),

                                      IconButton(
                                        icon: Image(
                                          image: AssetImage("assets/icn_history_BottomSheet.png"),
                                        ),
                                        onPressed: (){

                                        },
                                      ),

                                      IconButton(
                                          icon: Image(
                                            image:
                                            AssetImage("assets/icn_arrow_up.png"),
                                          ),
                                          onPressed: () {
                                            _bottomPersistentSheet3();
                                          }
                                      ),

                                    ],
                                  ),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(Radius.circular(50)),
                                    color: Color.fromRGBO(0, 99, 216, 1),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
    },
        backgroundColor: Colors.transparent
    );
  }

  _bottomPersistentSheet3(){
    _scaffoldKey.currentState.showBottomSheet<void>((BuildContext context) {
      return

        GestureDetector(
          onTap: (){
            Navigator.pop(context);
          },
          child: Scaffold(
            backgroundColor: Colors.transparent,
            bottomNavigationBar: Container(
              height: platform.Platform.isIOS ? 100:95,
              child: GestureDetector(
                onTap: (){  },
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      color: Color.fromRGBO(28, 129, 245, 1),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(25),
                          topRight: Radius.circular(25)
                      )
                  ),
                  child: Column(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 20,vertical: 8),
                        //margin: EdgeInsets.symmetric(horizontal: 20,vertical: 15),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[

                                _carnowidget(),
                                IconButton(
                                    icon: Image(
                                      image:
                                      AssetImage("assets/icn_arrow_up.png"),
                                    ),
                                    onPressed: () {
                                      _cardetailBottom();
                                    }
                                ),
                              ],
                            ),
                            Divider(color: Colors.white, thickness: 0.5,)
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
    },
        backgroundColor: Colors.transparent
    );
  }

  loadingScreen(){
    showwDialog(
        context: context,
        builder: (context)=>ForLookingMatchLoading()
    );
  }

  _cardetailBottom(){
    _scaffoldKey.currentState.showBottomSheet<void>((BuildContext context) {
      return GestureDetector(
        onTap: (){
          Navigator.pop(context);
        },
        child: SingleChildScrollView(
          child: GestureDetector(
            onTap: (){},
            child: _cardetailwidget()
          ),
        ),
      );
    },
        backgroundColor: Colors.transparent
    );
  }
  Widget _cardetailwidget(){
    return CarDetailScreen(
      swapRequestSent: (){
        swapRequestSent(context);
      },
      bottomPersistentSheet3: (){
        _bottomPersistentSheet3();
      },
    );

  }

  Widget _carnowidget(){
    return CarNoText();
  }

  swapRequestSent(context) async{

    _scaffoldKey.currentState.showBottomSheet<void>((BuildContext context) {
      return GestureDetector(
        onTap: (){
          Navigator.pop(context);
        },
        child: Scaffold(
          backgroundColor: Color.fromRGBO(28, 129, 245, 0.8),
          bottomNavigationBar: SingleChildScrollView(
            child: GestureDetector(
              onTap: (){
                Navigator.pop(context);
              },
              child: Container(
                //height: MediaQuery.of(context).size.height * 0.6,
                //height: MediaQuery.of(context).size.height *0.7,
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.only(top: 25),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30)
                    )
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            child: Text(
                              '${Common.swapRequestSent}',
                              style: signInBottomThankYou,
                              textAlign: TextAlign.center,
                              softWrap: true,
                              maxLines: 3,
                              //textScaleFactor: 1.5,
                            ),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            child: Text(
                              '${Common.waitingForConfirm}',
                              style: signInBottomThankYouSubHeading,
                              textAlign: TextAlign.center,
                              softWrap: true,
                              maxLines: 3,
                              //textScaleFactor: 1.5,
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 25),
                      width: 200,
                      height: 100,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage("assets/img_other_user.png"),
                          )
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  "${Common.selectedCarNumber}",
                                  style: successFullCarNo,
                                  textAlign: TextAlign.center,
                                  softWrap: true,
                                  maxLines: 3,
                                  //textScaleFactor: 1.5,
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),

                    Padding(
                      padding: const EdgeInsets.all(5),
                      child: Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              LoaderAnimation()
                            ],
                          ),
                        ],
                      ),
                    ),

                    Container(
                      padding: EdgeInsets.only(top: 25),
                      width: 200,
                      height: 100,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage("assets/img_self_user.png"),
                          )
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  //'$userCarNo',
                                  '${Common.profileCarNo}',
                                  style: successFullCarNo,
                                  textAlign: TextAlign.center,
                                  softWrap: true,
                                  maxLines: 3,
                                  //textScaleFactor: 1.5,
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),

                    Padding(
                      padding:
                      platform.Platform.isIOS
                          ? EdgeInsets.only(  top: 5, left: 5, right: 5, bottom: 25)
                          :EdgeInsets.only( top: 5, left: 5, right: 5, bottom: 5),
                      child: Container(
                        decoration: BoxDecoration(
                            color: Color.fromRGBO(255, 165, 0, 1),
                            borderRadius: BorderRadius.all(Radius.circular(50))
                        ),
                        width: 200,
                        height: 50,

                        child: FlatButton(
                          shape: OutlineInputBorder(
                              borderSide: BorderSide.none,
                              borderRadius: BorderRadius.all(Radius.circular(50))
                          ),
                          child: Text(
                            'Cancel',
                            textAlign: TextAlign.center,
                            style: signInBottomThankYouButtonOk,),
                          onPressed: (){
                            Navigator.pop(context);
                          },
                        ),
                      ),
                    )
                  ],
                ),

              ),
            ),

          ),
        ),
      );
    },
      backgroundColor: Colors.transparent,
    );

    Future.delayed(Duration(seconds: 3)).whenComplete((){
      successfullMatch(context);
    });

  }

  successfullMatch(context) async{
    _scaffoldKey.currentState.showBottomSheet<void>((BuildContext context) {
      return GestureDetector(
        onTap: (){
          Navigator.pop(context);
        },
        child: Scaffold(
          backgroundColor: Colors.transparent,
          bottomNavigationBar: GestureDetector(
            onTap: (){
              Navigator.of(context).pop();
            },
            child: Container(
              height: MediaQuery.of(context).size.height * 0.6,
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.only(top: 20),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30),
                      topRight: Radius.circular(30)
                  )
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Successfully Matched',
                          style: signInBottomThankYou,
                          textAlign: TextAlign.center,
                          softWrap: true,
                          maxLines: 3,
                          //textScaleFactor: 1.5,
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 25),
                    width: 200,
                    height: 100,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage("assets/img_other_user.png"),
                        )
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              child: Text(
                                "${Common.selectedCarNumber}",
                                style: successFullCarNo,
                                textAlign: TextAlign.center,
                                softWrap: true,
                                maxLines: 3,
                                //textScaleFactor: 1.5,
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 25),
                    width: 200,
                    height: 100,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage("assets/img_self_user.png"),
                        )
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              child: Text(
                                //'$userCarNo',
                                "${Common.profileCarNo}",
                                style: successFullCarNo,
                                textAlign: TextAlign.center,
                                softWrap: true,
                                maxLines: 3,
                                //textScaleFactor: 1.5,
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(5),
                    child: Container(
                      decoration: BoxDecoration(
                          color: Color.fromRGBO(255, 165, 0, 1),
                          borderRadius: BorderRadius.all(Radius.circular(50))
                      ),
                      width: 200,
                      height: 50,

                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          GestureDetector(
                            child: Container(
                              width: 180,
                              decoration: BoxDecoration(
                                // border: Border.all(color: Color.fromRGBO(255,255,255,0.1)),
                                  borderRadius: BorderRadius.all(Radius.circular(30))
                              ),
                              //padding: EdgeInsets.only(right: 15,left: 20,top: 10,bottom: 10),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Expanded(
                                    child: Text(
                                      "Swap Now",
                                      style: loginSignInButton,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                  Icon(Icons.arrow_forward,color: Colors.white)
                                ],
                              ),
                            ),
                            onTap: (){
                              successfullSwap(context);
                            },
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      );
    },
        backgroundColor: Colors.transparent
    );

  }

  successfullSwap(context) {
    _scaffoldKey.currentState.showBottomSheet<void>((BuildContext context) {
      return Scaffold(
        backgroundColor: Colors.transparent,
        bottomNavigationBar: GestureDetector(
          onTap: (){
            Navigator.of(context).pop();
          },
          child: Container(
            height: MediaQuery.of(context).size.height * 0.5,
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.only(top: 20),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30)
                )
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Successfully Swap?',
                        style: signInBottomThankYou,
                        textAlign: TextAlign.center,
                        softWrap: true,
                        maxLines: 3,
                        //textScaleFactor: 1.5,
                      )
                    ],
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: 300,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              child: Text(
                                '${Common.successsfullySwapWith} \"${Common.selectedCarNumber}\"',
                                style: signInBottomThankYouSubHeading,
                                textAlign: TextAlign.center,
                                softWrap: true,
                                maxLines: 3,
                                //textScaleFactor: 1.5,
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(5),
                  child: Container(
                    decoration: BoxDecoration(
                      //color: Color.fromRGBO(255, 165, 0, 1),
                        borderRadius: BorderRadius.all(Radius.circular(50)),
                        border: Border.all(
                          color: Color.fromRGBO(255, 165, 0, 1),
                          width: 0.5,
                        )
                    ),
                    width: 200,
                    height: 50,

                    child: FlatButton(
                      shape: OutlineInputBorder(
                          borderSide: BorderSide.none,
                          borderRadius: BorderRadius.all(Radius.circular(50))
                      ),
                      child: Text(
                        'NO',
                        textAlign: TextAlign.center,
                        style: signInBottomThankYouButtonNo,),
                      onPressed: (){
                        Navigator.pop(context);
                      },
                    ),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(5),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Color.fromRGBO(255, 165, 0, 1),
                        borderRadius: BorderRadius.all(Radius.circular(50))
                    ),
                    width: 200,
                    height: 50,

                    child: FlatButton(
                      shape: OutlineInputBorder(
                          borderSide: BorderSide.none,
                          borderRadius: BorderRadius.all(Radius.circular(50))
                      ),
                      child: Text(
                        'Yes',
                        textAlign: TextAlign.center,
                        style: signInBottomThankYouButtonOk,),
                      onPressed: () async {
                        var chkReq = await parkingRequest();
                        if(chkReq){
                          rateus();
                        }else{
                          CommonMethod(errrorMessage: Common.messageRequestNotSent );
                        }
                      },
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      );
    },
        backgroundColor: Colors.transparent
    );
  }

  rateus(){
    _scaffoldKey.currentState.showBottomSheet<void>((BuildContext context) {
      return SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          backgroundColor: Colors.transparent,
          bottomNavigationBar:  RateUs(),
        ),
      );
    },
        backgroundColor: Colors.transparent
    );
  }

  _savedLocation(){
    _scaffoldKey.currentState.showBottomSheet<void>(
          (BuildContext context){
        return Scaffold(
          backgroundColor: Color.fromRGBO(24, 121, 232, 0.8),
          bottomNavigationBar: GestureDetector(
            onTap: (){
              Navigator.of(context).pop();
            },
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height/2,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30),
                      topRight: Radius.circular(30)
                  )
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Flexible(
                        child: Image.asset(
                          'assets/check_registration_popup.png',
                          width: 100,
                          height: 100,
                        ),
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          '${Common.locationHasSaved}',
                          style: saveLocationBottom,
                          textAlign: TextAlign.center,
                          softWrap: true,
                          maxLines: 3,
                          //textScaleFactor: 1.5,
                        ),
                      )
                    ],
                  ),
                  Container(
                    decoration: BoxDecoration(
                        color: Color.fromRGBO(255, 165, 0, 1),
                        borderRadius: BorderRadius.all(Radius.circular(50))
                    ),
                    width: 250,
                    height: 50,
                    child: FlatButton(
                      shape: OutlineInputBorder(
                          borderSide: BorderSide.none,
                          borderRadius: BorderRadius.all(Radius.circular(50))
                      ),
                      child: Text(
                        'View Saved Location',
                        softWrap: true,
                        maxLines: 3,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 16,
                          fontFamily: Common.fontAdelleRegular,
                          color: Colors.white,
                        ),
                      ),
                      onPressed: (){
                        Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context) => SavedLocation(widget.status)
                            )
                        );
                      },
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(50)),
                        border: Border.all(
                            color: Colors.black87,
                            width: 0.4
                        )
                    ),
                    height: 50,
                    width: 250,
                    child: FlatButton(
                      shape: OutlineInputBorder(
                          borderSide: BorderSide.none,
                          borderRadius: BorderRadius.all(Radius.circular(50))
                      ),
                      child: Text(
                        'Cancel',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 16,
                          color: Color.fromRGBO(28, 129, 245, 1),
                        ),
                      ),
                      onPressed: (){
                        Navigator.of(context).pop();
                      },
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
      backgroundColor: Color.fromRGBO(24, 121, 232, 0.3),
    );
  }

  void curPosition() async{
    _curPosition = await Geolocator().getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);

    await helper.setLastMapLocation(lastmapLocation: LatLng(_curPosition.latitude,_curPosition.longitude));

    animateCamera(latLng:LatLng(_curPosition.latitude,_curPosition.longitude) );
    setState(() {
      _latLng =LatLng(_curPosition.latitude,_curPosition.longitude);
    });

    BitmapDescriptor userMarker = await _userAssetIcon(context);
    setState(() {
      _markers.add(Marker(
          markerId: MarkerId(_curPosition.toString()),
          position: LatLng(_curPosition.latitude,_curPosition.longitude),
          /*infoWindow: InfoWindow(
              title: '$userName',
              snippet: '\t $userCarNo'
          ),*/
          icon: userMarker
      ));
    });
  }

  animateCamera({LatLng latLng})async{
    mapController.animateCamera(
        CameraUpdate.newCameraPosition(CameraPosition(
            target: latLng,
            zoom: Common.zoomCamera
        )
        )
    );
  }

  onMapCreated(GoogleMapController controller) async{
    _mapController.complete(controller);

    setState(() {
      mapController = controller;
    });
  }

  void _onAddCircle({LatLng latlang}) {

    if(_circleLocation.isNotEmpty){
      _circleLocation.clear();
    }

    setState(
            () {
              _circleLocation.add(
                  Circle(
                    circleId : CircleId(latlang.toString()),
                    center: _createCenter(latlang: latlang),
                    radius: Common.radiusCircle ,
                    fillColor: Color.fromRGBO(128, 142, 253, 0.3),
                    strokeWidth: 2,
                    visible: true,
                    strokeColor: Color.fromRGBO(128, 142, 253, 1),
                  ) );
            } );


  }

  LatLng _createCenter({LatLng latlang}) {
    return latlang;
  }

  changeRadiusMethod(var i){
    setState(() {
      Common.radiusCircle = i * 30.48;
      radiusValue = i * 30.48;
    });
  }

  getDistance({LatLng latLng}) async {
    ltlg.Distance distance = new ltlg.Distance();
    var m = await distance.as(
        ltlg.LengthUnit.Kilometer,
        new ltlg.LatLng(lattitudeStore,longitudeStore),
        new ltlg.LatLng(latLng.latitude,latLng.longitude)).toDouble();

    return (m.toDouble());
  }

  setAppbarTitle({LatLng latLng,String title}) async{
    var heading_temp = await helper.getLocality(latLng: latLng);
    setState(() {
      if(latLng != null){
        setState(() {
          Common.lookingForParkingHeading = heading_temp;
        });
      }
      else if(title != null) {
        setState(() {
          Common.lookingForParkingHeading = title;
        });
      }
    });
  }

  void _onAddMarkerButtonPressed({LatLng latlang}) async{

    BitmapDescriptor userMarker = await _userAssetIcon(context);
    setState(() {
      _markers.add(Marker(
          markerId: MarkerId(_curPosition.toString()),
          position: LatLng(_curPosition.latitude,_curPosition.longitude),
          /*infoWindow: InfoWindow(
              title: '$userName',
              snippet: '\t $userCarNo'
          ),*/
          icon: userMarker
      ));
    });

    setState(() { _markers.add(Marker(
      markerId: MarkerId(latlang.toString()),
      position: latlang,
      onTap: (){
        _bottomPersistentSheet(latLng: latlang);
      },
      icon: BitmapDescriptor.defaultMarker,
    )); });

    _bottomPersistentSheet(latLng: latlang);
  }

  void _addMarkingAvailableParking() async {

    var list = await searchNearMethod(
        latLng: LatLng(lattitudeStore, longitudeStore),
        radius: Common.radiusCircle);

    BitmapDescriptor userMarkericon =   await _userAssetIcon(context);
    setState(() {
      _customMarkers.add(Marker(
        markerId: MarkerId("Selected Location"),
        position: LatLng(lattitudeStore, longitudeStore),
        consumeTapEvents: true,
        onTap: () {
          _bottomPersistentSheet();
        },
        icon: BitmapDescriptor.defaultMarker,
      ));
      _customMarkers.add(Marker(
          markerId: MarkerId("User Location"),
          position: LatLng(_curPosition.latitude, _curPosition.longitude),
          /*infoWindow: InfoWindow(
              title: '$userName',
              snippet: '\t $userCarNo'
          ),*/
          icon: userMarkericon
      ));
    });

    for(var i in list){
      addMarkingWithList(latLng: LatLng(i.latitude,i.longitude) );
    }

  }

  addMarkingWithList({LatLng latLng}) async {

    var distanceFoot = await getDistance(latLng: LatLng(latLng.latitude, latLng.longitude));

    if ((radiusValue ) > (distanceFoot * 3280.84)) {
      setmark(latLng: LatLng(latLng.latitude, latLng.longitude));
      setState(() {
        latLngList.add(latLng);
      });
    }
  }

  setmark({LatLng latLng}) async {

    BitmapDescriptor markericon = await _getAssetIcon(context);
    setState(() {
      _customMarkers.add(Marker(
          markerId: MarkerId(latLng.toString()),
          position: latLng,
          consumeTapEvents: true,
          onTap: () async{
            if(selectedCarLocation != null){
              desetmark(latLng: selectedCarLocation);
              setState(() {
                selectedCarLocation = null;
              });
            }
            setState(() {
              selectedCarLocation = latLng;
            });
            await selectCarmark(latLng: selectedCarLocation);
          },
          icon: markericon
      ));
    });
  }

  selectCarmark( {LatLng latLng}) async {

    BitmapDescriptor selectCaricon = await _selectedAssetIcon(context);
    BitmapDescriptor userMarkericon =   await _userAssetIcon(context);

    animateCamera( latLng: latLng);
    _bottomPersistentSheet2();
    await sendRequest(latLng);
    await selectedCarInfo(latLng: selectedCarLocation);

    for (var ltl in latLngList){
      if( ltl.latitude != latLng.latitude && ltl.longitude != latLng.longitude  ){
        setmark(latLng:  ltl);
      }else{
        setState(() {
          _customMarkers.add(Marker(
              markerId: MarkerId(latLng.toString()),
              position: latLng,  //LatLng(i.latitude,i.longitude),

              onTap: () async{
                await selectedCarInfo(latLng: latLng);
                _bottomPersistentSheet2();
                if(selectedCarLocation != null){
                  await desetmark(latLng: selectedCarLocation);
                  if(selectedCarLocation == latLng){
                    await selectCarmark(latLng: latLng);
                    if(_customMarkers != null){
                      setState(() {
                        _customMarkers.clear();
                      });
                    }
                  }else{
                    setState(() {
                      selectedCarLocation = latLng;
                    });
                    await selectCarmark(latLng: latLng);

                    if(_customMarkers != null){
                      setState(() {
                        _customMarkers.clear();
                      });
                    }
                  }
                }
              },
              icon: selectCaricon
          ));
        });
      }
    }

    setState(() {
      _customMarkers.add(Marker(
        markerId: MarkerId("Selected Location"),
        position: LatLng(lattitudeStore, longitudeStore),
        consumeTapEvents: true,
        onTap: () {},
        icon: BitmapDescriptor.defaultMarker,
      ));
      _customMarkers.add(Marker(
          markerId: MarkerId("User Location"),
          position: LatLng(_curPosition.latitude, _curPosition.longitude),
          consumeTapEvents: true,
          onTap: () { },
          icon: userMarkericon
      ));
    });


  }

  desetmark( {LatLng latLng}) async {
    BitmapDescriptor markericon = await _getAssetIcon(context);
    setState(() {
      _customMarkers.add(Marker(
          markerId: MarkerId(latLng.toString()),
          position: latLng,  //LatLng(i.latitude,i.longitude),
          onTap: (){
            if(selectedCarLocation != null){
              desetmark(latLng: selectedCarLocation);
              setState(() {
                selectedCarLocation = null;
              });
            }
            setState(() {
              selectedCarLocation = latLng;
            });
            selectCarmark(latLng: selectedCarLocation);
            _bottomPersistentSheet2();
            selectedCarInfo();
          },
          icon: markericon
      ));
    });

  }

  userLocationMarking({LatLng latLng}) async{
    BitmapDescriptor userMarkericon = await _userAssetIcon(context);
    setState(() {
      _customMarkers.add(Marker(
          markerId: MarkerId("User Location"),
          position: latLng,
          onTap: (){},
          icon: userMarkericon
      ));
    });
  }

  selectCarMark({LatLng latLng}) async{
    BitmapDescriptor selectedParkinMarkericon = await _selectedAssetIcon(context);

    setState(() {
      _customMarkers.add(Marker(
          markerId: MarkerId("Selected Car"),
          position: selectedCarLocation,
          onTap: () async{},
          icon: selectedParkinMarkericon
      ));
    });
  }

  Future<BitmapDescriptor> _getAssetIcon(BuildContext context) async {
    return await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(size: Size(100,100)),
        'assets/other_car_normal.png')
        .then((onValue) {
      return onValue;
    });


    /* final Completer<BitmapDescriptor> bitmapIcon = Completer<BitmapDescriptor>();
    final ImageConfiguration config = createLocalImageConfiguration(context);
    const AssetImage("assets/other_car_normal.png")
        .resolve(config)
        .addListener( ImageStreamListener(
            (ImageInfo image, bool sync) async {
          final ByteData bytes =
          await image.image.toByteData(format: ui.ImageByteFormat.png);
          final BitmapDescriptor bitmap =
          BitmapDescriptor.fromBytes(bytes.buffer.asUint8List());
          bitmapIcon.complete(bitmap);
        }
    )
    );
    return await bitmapIcon.future;*/
  }

  Future<BitmapDescriptor> _userAssetIcon(BuildContext context) async {

    return await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(size: Size(100,100)),
        'assets/df.png')
        .then((onValue) {
      return onValue;
    });

    /*final Completer<BitmapDescriptor> bitmapIcon = Completer<BitmapDescriptor>();
    final ImageConfiguration config = createLocalImageConfiguration(context);
    const AssetImage("assets/df.png")
        .resolve(config)
        .addListener(ImageStreamListener((ImageInfo image, bool sync) async {
      final ByteData bytes =
      await image.image.toByteData(format: ui.ImageByteFormat.png);
      final BitmapDescriptor bitmap =
      BitmapDescriptor.fromBytes(bytes.buffer.asUint8List());
      bitmapIcon.complete(bitmap);
    }
    )
    );
    return await bitmapIcon.future;*/
  }

  Future<BitmapDescriptor> _selectedAssetIcon(BuildContext context) async {


    return await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(size: Size(100,100)),
        'assets/other_car_selected.png')
        .then((onValue) {
      return onValue;
    });

   /* final Completer<BitmapDescriptor> bitmapIcon = Completer<BitmapDescriptor>();
    final ImageConfiguration config = createLocalImageConfiguration(context);
    const AssetImage("assets/other_car_selected.png")
        .resolve(config)
        .addListener(ImageStreamListener((ImageInfo image, bool sync) async {
      final ByteData bytes =
      await image.image.toByteData(format: ui.ImageByteFormat.png);
      final BitmapDescriptor bitmap =
      BitmapDescriptor.fromBytes(bytes.buffer.asUint8List());
      bitmapIcon.complete(bitmap);
    }
    )
    );
    return await bitmapIcon.future;*/
  }

  selectedCarInfo({LatLng latLng}) async{
    var addrLine;
    var future = await searchNearMethod(latLng: LatLng(lattitudeStore, longitudeStore),radius: Common.radiusCircle);
    for(var i in future){
      if(i.latitude == latLng.latitude && i.longitude == latLng.longitude){
        addrLine = await helper.getAddressLocation(latLng: LatLng(i.latitude,i.longitude));
        CardetailModel json = await carInfoApi(parkingId:  i.userId);

        if(json.status == 1){
          setState(() {
            Common.selectedCarNumber = json.carNumber;
            Common.selectedAddressline = addrLine;
            Common.selectedCarMake = json.make;
            Common.selectedCarModel = json.model;
            Common.selectedCarColor = json.color;
            Common.selectedCarUsername = "${json.fname} ${json.lname}";
            Common.selectedCarUserParkingId = json.parkingId;
          });
        }

      }
    }


  }

  addFaveriouteLocationMethod()async{
    if(lattitudeStore == null && longitudeStore == null){
      return;
    }
    else{
      var chkFuture = await bookmarkAddApi(latLng: LatLng(lattitudeStore,longitudeStore));
      if(chkFuture.status == 1 ){
        setState(() {
          faverColor= true;
          tempBookmarkId = chkFuture.bookmarkId;
        });
        _savedLocation();
      }else{

      }
    }
  }

  deleteFaveriouteLocationMthod({String bookmarkId})async{
      var chkFuture = await bookmarkDeleteApi( bookmarkId: bookmarkId );
      if(chkFuture.status == 1){
        _flushPopUp(
            message: Common.bookmarkDeletesuccessfully,
            bgColor: Colors.green,
            duration: Duration(milliseconds: 2500));

        setState(() {
          faverColor= false;
        });
    }
  }

  parkingRequest() async{
    var reqPark = await parkingRequestApi( parkingId: Common.selectedCarUserParkingId  );

    if(reqPark.status == 1){
      return true;
    }else{
      return false;
    }
  }



  connectionCheck() async {
    var cnChk = await _internetConnectivity.checkConnectivity();
    if(cnChk){
      return ;
    }else{
      showDialog(
          context: context,
          builder: (context){
            return AlertDialog(
              title: Text(
                  '${Common.internetErrorMessage}'
              ),
              actions: <Widget>[
                FlatButton(
                  child: Text('Retry'),
                  onPressed: (){
                    Navigator.pushReplacement(context, MaterialPageRoute(
                        builder: (context)=> Location(true)
                    ));
                  },
                )
              ],
            );
          }
      );
    }
  }

  connectivitySubscription() async{
    connectivitySubscriptionVariable =
    await _connectivity.onConnectivityChanged.listen(
            (ConnectivityResult result){
        print(" connectivity reulsts  $result");
        if (result == ConnectivityResult.mobile) {
        } else if (result == ConnectivityResult.wifi) {
        }  else if (result == ConnectivityResult.none) {
          showDialog(
              context: context,
              builder: (context){
                return AlertDialog(
                  title: Text(
                      '${Common.internetErrorMessage}'
                  ),
                  actions: <Widget>[
                    FlatButton(
                      child: Text('Retry'),
                      onPressed: (){
                        Navigator.pushReplacement(context, MaterialPageRoute(
                            builder: (context)=> Location(true)
                        ));
                      },
                    )
                  ],
                );
              }
          );
        }else{
          showDialog(
              context: context,
              builder: (context){
                return AlertDialog(
                  title: Text(
                      '${Common.internetErrorMessage}'
                  ),
                  actions: <Widget>[
                    FlatButton(
                      child: Text('Retry'),
                      onPressed: (){
                        Navigator.pushReplacement(context, MaterialPageRoute(
                            builder: (context)=> Location(true)
                        ));
                      },
                    )
                  ],
                );
              }
          );
        }

    });
    /*connectivitySubscriptionVariable = await _connectivity.onConnectivityChanged.listen(
            (ConnectivityResult result){
          print(" connectivity reulsts  $result");
          if (result == ConnectivityResult.mobile) {
            return true;
          } else if (result == ConnectivityResult.wifi) {
            return true;
          }else{
            return false;
          }
        });*/
  }

  connectivitySubscriptionCancel(){
    connectivitySubscriptionVariable.cancel();  }

  /*Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        return true;
        break;
      case ConnectivityResult.mobile:
        return true;
        break;
      case ConnectivityResult.none:
        showDialog(
            context: context,
            builder: (context){
              return AlertDialog(
                title: Text(
                    '${Common.internetErrorMessage}'
                ),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Retry'),
                    onPressed: (){
                      Navigator.pushReplacement(context, MaterialPageRoute(
                          builder: (context)=> Location(true)
                      ));
                    },
                  )
                ],
              );
            }
        );
        break;
      default:
        break;
    }
  }*/






  //TODO: api calling methods

  Future<List<SearchNearModel>> searchNearMethod({LatLng latLng, double radius}) async {

    try {
      String oauth = await helper.getOauth();
      HttpService _httpService = HttpService();

      var nearSearchData = await _httpService.searchNearMethod(
          token:  oauth,
          latLng: latLng,
          radius: radius
      );

      List<SearchNearModel> searchNearModellist =[];

      if(nearSearchData.statusCode == 200){
        var jsonData = await jsonDecode(nearSearchData.body);

        for(var i in jsonData["data"]){
          SearchNearModel serlist = SearchNearModel(
              status: jsonData["status"],
              message: jsonData["message"],
              address: i["address"],
              userId: i["_id"],
              longitude: i["longitude"],
              latitude: i["latitude"],
              carNumber: i["car"]["car_number"],
              model: i["car"]["model"],
              make: i["car"]["make"],
              color: i["car"]["color"]
          );
          searchNearModellist.add(serlist);
        }

      }else if(nearSearchData.statusCode == 401){
       // _messageBottomSheet(errrorMessage: Common.authInvalid);

          Common.authvalidatestatus = true;
        Navigator.pushReplacement(context,
          MaterialPageRoute(
            builder: (context)=> Login()
          )
        );
      }else{
        _messageBottomSheet(errrorMessage: Common.serverError);
      }


      return searchNearModellist;

    }catch(e){
      print("Exception : >>>>> " + e.toString());
    }
  }

  Future<CardetailModel> carInfoApi({String parkingId}) async {
    String oauth = await helper.getOauth();
    try {
      HttpService _httpService = HttpService();

      var carInfoData = await _httpService.carDetail( token:  oauth, parkingId: "$parkingId");

      if(carInfoData.statusCode == 200){
        var jsonData = CardetailModel.fromJson(jsonDecode(carInfoData.body));
        return jsonData;
      }else if(carInfoData.statusCode == 401){
        //_messageBottomSheet(errrorMessage: Common.authInvalid);
        Navigator.pushReplacement(context,
            MaterialPageRoute(
                builder: (context)=> Login()));
      }else{
        _messageBottomSheet(errrorMessage: Common.serverError);
      }

    }catch(e){
      print("Exception : >>>>> " + e.toString());
    }
  }

  Future<ParkingRequestModel> parkingRequestApi({String parkingId}) async {
    String oauth = await helper.getOauth();
    try {
      HttpService _httpService = HttpService();

      var parkingRequestData = await _httpService.parkingRequest( token:  oauth, parkingId: "$parkingId");

      if(parkingRequestData.statusCode == 200){
        var jsonData = ParkingRequestModel.fromJson(jsonDecode(parkingRequestData.body));
        return jsonData;
      }else if(parkingRequestData.statusCode == 401){
        //_messageBottomSheet(errrorMessage: Common.authInvalid);
        Navigator.pushReplacement(context,
            MaterialPageRoute(
                builder: (context)=> Login()));
      }else{
        _messageBottomSheet(errrorMessage: Common.serverError);
      }
    }catch(e){
      print("Exception : >>>>> " + e.toString());
    }
  }

  Future<BookmarkAddModel> bookmarkAddApi({LatLng latLng}) async {
    String oauth = await helper.getOauth();
    var addrline = await helper.getAddressLocation(latLng: latLng);
    try {
      HttpService _httpService = HttpService();

      var addBookmarkData = await _httpService.bookmarkAdd(
          token:  oauth,
          latLng: latLng,
          address: "$addrline"
      );

      if(addBookmarkData.statusCode == 200){
        var jsonData = BookmarkAddModel.fromJson(jsonDecode(addBookmarkData.body));
        setState(() {
          tempBookmarkId = jsonData.bookmarkId;
        });
        return jsonData;

      }else  if(addBookmarkData.statusCode == 401){
        //_messageBottomSheet(errrorMessage: Common.authInvalid);
        Navigator.pushReplacement(context,
            MaterialPageRoute(
                builder: (context)=> Login()));
      }else{
        _messageBottomSheet(errrorMessage: Common.serverError);
      }
    }catch(e){
      print("Exception : >>>>> " + e.toString());
    }
  }

  Future<BookmarkDeleteModel> bookmarkDeleteApi({String bookmarkId}) async {
    String oauth = await helper.getOauth();
    try {
      HttpService _httpService = HttpService();

      var deleteBookmarkData = await _httpService.bookmarkDelete( token:  oauth, bookmarkId: bookmarkId );

      print("delete bookmark response  ${deleteBookmarkData.body}");

      if(deleteBookmarkData.statusCode == 200){
        var jsonData = BookmarkDeleteModel.fromJson(jsonDecode(deleteBookmarkData.body));
        return jsonData;
      }else  if(deleteBookmarkData.statusCode == 401){
       // _messageBottomSheet(errrorMessage: Common.authInvalid);
        Navigator.pushReplacement(context,
            MaterialPageRoute(
                builder: (context)=> Login()));
      }else{
        _messageBottomSheet(errrorMessage: Common.serverError);
      }
    }catch(e){
      print("Exception : >>>>> " + e.toString());
    }
  }

  // show message  method

  _messageBottomSheet({String errrorMessage}){

    Flushbar(
      message:  "$errrorMessage",
      flushbarPosition: FlushbarPosition.TOP,
      flushbarStyle: FlushbarStyle.FLOATING,
      borderRadius: 10,
      margin: EdgeInsets.all(10),
      duration:  Duration(milliseconds: 1000),
    )..show(context);

  }


  // for map route in flutterr

  // ! CREATE LAGLNG LIST
  List<LatLng> _convertToLatLng(List points) {
    List<LatLng> result = <LatLng>[];
    for (int i = 0; i < points.length; i++) {
      if (i % 2 != 0) {
        result.add(LatLng(points[i - 1], points[i]));
      }
    }
    return result;
  }

  // !DECODE POLY
  List _decodePoly(String poly) {
    var list = poly.codeUnits;
    var lList = new List();
    int index = 0;
    int len = poly.length;
    int c = 0;
// repeating until all attributes are decoded
    do {
      var shift = 0;
      int result = 0;

      // for decoding value of one attribute
      do {
        c = list[index] - 63;
        result |= (c & 0x1F) << (shift * 5);
        index++;
        shift++;
      } while (c >= 32);
      /* if value is negetive then bitwise not the value */
      if (result & 1 == 1) {
        result = ~result;
      }
      var result1 = (result >> 1) * 0.00001;
      lList.add(result1);
    } while (index < len);

/*adding to previous value as done in encoding */
    for (var i = 2; i < lList.length; i++) lList[i] += lList[i - 2];

    print(lList.toString());

    return lList;
  }

  // ! SEND REQUEST
  void sendRequest(LatLng destination) async {

    GoogleMapsServices _googleMapsServices = GoogleMapsServices();

   /* List<Placemark> placemark =
    await Geolocator().placemarkFromAddress(intendedLocation);
    double latitude = placemark[0].position.latitude;
    double longitude = placemark[0].position.longitude;
    LatLng destination = LatLng(latitude, longitude);*/
    //_addMarker(destination, intendedLocation);
    String route = await _googleMapsServices.getRouteCoordinates(
        LatLng(_curPosition.latitude,_curPosition.longitude), destination);
    createRoute(route);
  }

  // ! TO CREATE ROUTE
  void createRoute(String encondedPoly) {
    _polyLines.add(Polyline(
        polylineId: PolylineId("Map route"),
        startCap: Cap.roundCap,
        endCap: Cap.buttCap,
        width: 5,
        points: _convertToLatLng(_decodePoly(encondedPoly)),
        color: Colors.blue));

  }

  @override
  void dispose() {

    _locationSubscription.cancel();
     Common.lookingForParkingHeading = "Looking for Parking";
     faverColor = false;
    super.dispose();
  }

}


class BS extends StatefulWidget {

  final VoidCallback callbackChange;
  final VoidCallback historyCallback;
  final Function(bool) savedLocationCallback;
  final LatLng latLng;
  BS({this.callbackChange,this.latLng,this.historyCallback,this.savedLocationCallback});
  _BS createState() => _BS();
}

class _BS extends State<BS> {

  var address;
  var city;

  double  height =235.0;
  setHeight(){
    if(height == 235.0){
      setState(()=>height =435);
    }else{
      setState(()=>height =235);
    }
  }
  @override
  void initState() {
    getAddressLocation();
    super.initState();
  }

  Widget _starIconButton(){
    return
      faverColor
    ? Icon( Icons.star, color: Colors.green[300], size: 35,)
        : Image(  image: AssetImage(  "assets/icn_favorite.png" ),
    color: Colors.white);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      child: GestureDetector(
        onTap: (){},
        child: Column(
          children: <Widget>[
            Container(
              height: 235.0,
              decoration: BoxDecoration(
                  color: Color.fromRGBO(28, 129, 245, 1),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30),
                  )
              ),
              padding: EdgeInsets.only(left: 15,right: 15,top: 15,bottom: 10),
              child: Column(
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(left:8.0,top: 8.0, bottom: 8.0 ,right: 8.0),
                                child: Container(
                                  padding: EdgeInsets.only(left: 5),
                                  child: Text(
                                    '${Common.myLocationString}',
                                    style: bottomSheetMyLocation,
                                  ),
                                ),
                              ),

                              Container(
                                width: MediaQuery.of(context).size.width * 0.4 + 16,
                                //padding: EdgeInsets.all(),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[

                                IconButton(
                                      icon: _starIconButton(),
                                      onPressed: (){
                                        widget.savedLocationCallback(faverColor);
                                        setState(() {
                                          faverColor = !faverColor;
                                        });
                                      },
                                ),


                                    IconButton(
                                      icon: Image(
                                        image: AssetImage("assets/icn_history_BottomSheet.png"),
                                      ),
                                      onPressed: (){
                                        widget.historyCallback();
                                      },
                                    ),

                                    IconWidget(
                                      callbackChange: (){
                                        setHeight();
                                      },
                                    )

                                  ],
                                ),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(50)),
                                  color: Color.fromRGBO(0, 99, 216, 1),
                                ),
                              ),



                            ],
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(50)),
                            color: Color.fromRGBO(0, 99, 216, 1),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 10,right: 15,top: 15,bottom: 10),
                    child: Row(
                      children: <Widget>[
                        city == null
                            ? CircularProgressIndicator()
                            : Expanded(
                          child: Text('${city??"${Common.unknown}"}',
                              style:bottomSheetMyLocationHeading,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                              softWrap: true),
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 10,right: 15,top: 10,bottom: 10),
                    child: Row(
                      children: <Widget>[
                        address == null
                            ? Container()
                            : Expanded(
                          child: Text('$address',
                              style:bottomSheetMyLocationAddress,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 2,
                              softWrap: true),
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 10,right: 15,top: 10,bottom: 10),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                              '${Common.freeParkingAvailableString}',
                              style:TextStyle(
                                  color: Color.fromRGBO(232, 240, 34, 1),
                                  fontFamily: Common.fontAdelleLight,
                                  fontSize: 16
                              ),
                              softWrap: true),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
  
  getAddressLocation() async {
    try{
      final coordinates = new Coordinates( widget.latLng.latitude, widget.latLng.longitude );
      var addresses = await Geocoder.local.findAddressesFromCoordinates(
          coordinates);
      var first = addresses.first;
      setState(() {
        city = first.locality;
        address = "${first.addressLine}";
      });
    }catch(e){
      print(e.toString());
    }
  }

}


class RadiusSelection extends StatefulWidget {

  final Function(double) onChanged;

  RadiusSelection({this.onChanged});

  @override
  _RadiusSelectionState createState() => _RadiusSelectionState();
}

class _RadiusSelectionState extends State<RadiusSelection> {

  static double initvalue = 0;
  var min =0.0;
  var max = 5.0;


  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SliderTheme(
          data: SliderTheme.of(context).copyWith(
            tickMarkShape: MyRadioSliderTickMarkShape(
                radius: 15,
                outerRadius: 15,
                index: 6,
                initvalue : initvalue.toInt()
            ),
            thumbShape: MyRadioSliderThumbShape(
                activeColor: Colors.blue,
                outerCircle:  true,
                radius: 15,
                radValue: initvalue.toInt()
            ),

            trackHeight: 3,
            activeTrackColor: Colors.blue,
            inactiveTickMarkColor: Colors.white,
            activeTickMarkColor: Colors.blue,
            trackShape: RoundSliderTrackShape2(),
            thumbColor: Colors.green,
          ),
          child: Slider(
            label: "${((initvalue+1)).toInt() * 100} ft.",
            value: initvalue,
            min: min,
            max: max,
            divisions: 5,
            onChanged: (val){
              setState(() {
                initvalue = val;
              });
              widget.onChanged(initvalue + 1);
            },
          )
      ),
    );
  }
}


class BottomAnim extends StatefulWidget {
  final LatLng latLng;
  final VoidCallback historyCallback;
  final Function(bool) savedLocationCallback;

  BottomAnim({this.latLng,  this.historyCallback, this.savedLocationCallback});

  @override
  _BottomAnimState createState() => _BottomAnimState();
}

class _BottomAnimState extends State<BottomAnim> {

  bool _showSecond = false;

  changeIcon(){
    setState(() {
      if(_showSecond){
        setState(()=> _showSecond = false);
      }else{
        {
          setState(()=> _showSecond = true);
        }
      }
    });
  }

  Widget _firstWidget(){
    return Align(
      alignment: Alignment.bottomCenter,
      child: BS(
        latLng: widget.latLng,
        callbackChange: (){ changeIcon(); },
        historyCallback: (){ widget.historyCallback(); },
        savedLocationCallback: (fevercolor){
          widget.savedLocationCallback(fevercolor);
        },
      ),
    );
  }
  Widget _secondWidget(){
    return BS(latLng: widget.latLng,
        callbackChange: (){
          changeIcon();
        }
    );
  }
  @override
  Widget build(BuildContext context) {
    return _showSecond
        ? _secondWidget()
        : _firstWidget();
  }
}


class IconWidget extends StatefulWidget {

  final VoidCallback callbackChange;

  IconWidget({this.callbackChange});
  @override
  _IconWidgetState createState() => _IconWidgetState();
}

class _IconWidgetState extends State<IconWidget> {

  bool iconStatus = false;
  changeIcon(){
    setState(() {
      if(iconStatus){
        setState(()=> iconStatus = false);
      }else{
        {
          setState(()=> iconStatus = true);
        }
      }
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return iconStatus
        ? IconButton(
      icon: Image(image: AssetImage("assets/icn_arrow_down.png")),
      onPressed: (){
        //setState(()=> iconStatus = false);
        changeIcon();
        widget.callbackChange();
      },
    )
        : IconButton(
      icon:Image(image: AssetImage("assets/icn_arrow_up.png")),
      onPressed: (){
        widget.callbackChange();
        changeIcon();
      },
    );
  }
}


class CarDetailScreen extends StatefulWidget {

  VoidCallback bottomPersistentSheet3;
  VoidCallback swapRequestSent;

  CarDetailScreen({this.bottomPersistentSheet3,this.swapRequestSent});

  @override
  _CarDetailScreenState createState() => _CarDetailScreenState();
}

class _CarDetailScreenState extends State<CarDetailScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15),
      width: MediaQuery.of(context).size.width,
      // height: MediaQuery.of(context).size.height/2,
      decoration: BoxDecoration(
          color: Color.fromRGBO(28, 129, 245, 1),
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(25),
              topRight: Radius.circular(25)
          )
      ),
      child:
      Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(1),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "Car Number",
                      style: bottomSheetCarDetailLabel,
                    ),
                    IconButton(
                      iconSize: 18,
                      icon: Image(
                        image: AssetImage("assets/icn_arrow_down.png"),
                      ),
                      onPressed: (){
                        widget.bottomPersistentSheet3();
                       // _bottomPersistentSheet3();
                        //Navigator.of(context).pop();
                      },
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "${Common.selectedCarNumber}",
                      style: TextStyle(
                          fontFamily: Common.fontAdelleRegular,
                          fontSize: 25,
                          color: Color.fromRGBO(247, 255, 0, 1)
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Divider(height: 0.1,color: Colors.white,thickness: 0.2,),
          Container(
            padding: EdgeInsets.only(top: 5,bottom: 5),
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        "Location",
                        style: bottomSheetCarDetailLabel,
                      ),
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                        child: Text(
                          "${Common.selectedAddressline}",
                          style: bottomSheetCarDetailText,
                        )
                    ),
                  ],
                ),
              ],
            ),
          ),
          Divider(height: 0.1,color: Colors.white,thickness: 0.2,),
          Container(
            padding: EdgeInsets.only(top: 5,bottom: 5),
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        "Name",
                        style: bottomSheetCarDetailLabel,
                      ),
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "${Common.selectedCarUsername}",
                      style: bottomSheetCarDetailText,
                    ),
                  ],
                ),
              ],
            ),
          ),
          Divider(height: 0.1,color: Colors.white,thickness: 0.2,),
          Container(
            padding: EdgeInsets.only(top: 5,bottom: 5),
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        "Make",
                        style: bottomSheetCarDetailLabel,
                      ),
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "${Common.selectedCarMake}",
                      //"Hyundai",
                      style: bottomSheetCarDetailText,
                    ),
                  ],
                ),
              ],
            ),
          ),
          Divider(height: 0.1,color: Colors.white,thickness: 0.2,),
          Container(
            padding: EdgeInsets.only(top: 5,bottom: 5),
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        "Model",
                        style: bottomSheetCarDetailLabel,
                      ),
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "${Common.selectedCarModel}",
                      //"i20",
                      style: bottomSheetCarDetailText,
                    ),
                  ],
                ),
              ],
            ),
          ),
          Divider(height: 0.1,color: Colors.white,thickness: 0.2,),
          Container(
            padding: EdgeInsets.only(top: 5,bottom: 5),
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,

                    children: <Widget>[
                      Text(
                        "Color",
                        style: bottomSheetCarDetailLabel,
                      ),
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "${Common.selectedCarColor}",
                      //"Red",
                      style: bottomSheetCarDetailText,
                    ),
                  ],
                ),
              ],
            ),
          ),
          Divider(height: 0.1,color: Colors.white,thickness: 0.1,),
          Container(
            height: MediaQuery.of(context).size.height *0.09,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(50))
                      ),
                      child: FlatButton(
                        child: Row(
                          children: <Widget>[
                            Text(
                              "Swap Parking ",
                              style: bottomSheetCarDetailSwapParkingButton,
                            ),
                            Icon(Icons.arrow_forward,color: Color.fromRGBO(28, 129, 245, 1),)
                          ],
                        ),
                        onPressed: (){
                          //swapRequestSent(context);
                          widget.swapRequestSent();
                        },
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class CarNoText extends StatefulWidget {
  @override
  _CarNoTextState createState() => _CarNoTextState();
}

class _CarNoTextState extends State<CarNoText> {
  @override
  Widget build(BuildContext context) {
    return  Container(
      child:
      Common.selectedCarNumber == null
          ? Row(
            children: <Widget>[
              CircularProgressIndicator(),
            ],
          )
          :  Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top:9,bottom: 3),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text("Car Number",style: swaplistViewLabel,)
              ],
            ),
          ),

          Padding(
            padding: const EdgeInsets.only(bottom: 3,),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  "${Common.selectedCarNumber}",
                  //"BGY-3891",
                  style: TextStyle(
                      color: Color.fromRGBO(247, 255, 0, 1),
                      fontSize: 18
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}


double rippleStartX, rippleStartY;
AnimationController _controller;
Animation _animation;
bool isInSearchMode = false;

void onSearchTapUp(TapUpDetails details) {
 // setState(() {
    rippleStartX = details.globalPosition.dx;
    rippleStartY = details.globalPosition.dy;
 // });
  _controller.forward();
}

cancelSearch() {
 // setState(() {
    isInSearchMode = false;
  //});
  _controller.reverse();
}

class AppbarAnimation extends StatefulWidget implements PreferredSizeWidget{

  Widget appbar;
  VoidCallback callback;

  AppbarAnimation({this.appbar,this.callback});

  @override
  Size get preferredSize => Size.fromHeight(56.0);

  @override
  _AppbarAnimationState createState() => _AppbarAnimationState();
}

class _AppbarAnimationState extends State<AppbarAnimation> with SingleTickerProviderStateMixin {


  animationStatusListener(AnimationStatus animationStatus) {
    if (animationStatus == AnimationStatus.completed) {
      setState(() {
        isInSearchMode = true;
      });
    }
  }



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controller = AnimationController(vsync: this, duration: Duration(milliseconds: 1000));
    _animation = Tween(begin: 0.0, end: 1.0).animate(_controller);
    _controller.addStatusListener(animationStatusListener);
    _controller.forward();

  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return
      AnimatedBuilder(
      animation: _animation,
      builder: (context, child) {
        return CustomPaint(
          painter: MyPainter(
            containerHeight: widget.preferredSize.height,
            center: Offset(rippleStartX ?? 0, rippleStartY ?? 0),
            radius: _animation.value * screenWidth,
            context: context,
          ),
          child: widget.appbar,
        );
      },
    );
  }
}
