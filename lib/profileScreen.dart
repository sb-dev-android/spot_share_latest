import 'package:flutter/material.dart';
import 'designModelClass/podoclasses/common.dart';
import 'stylesheet.dart';
import 'designModelClass/customAppbar.dart' as CustomAppbar;
import 'package:back_button_interceptor/back_button_interceptor.dart';


class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {

  EdgeInsets margin = EdgeInsets.only(top: 15,bottom: 10);
  EdgeInsets padding = EdgeInsets.only(top: 10,bottom: 10);
  double labHeight = 30;


  bool myInterceptor(bool stopDefaultButtonEvent) {
      Navigator.of(context).pop();
    return true;
  }

  @override
  void initState() {
    BackButtonInterceptor.add(myInterceptor);
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          appBar: CustomAppbar.AppBar(
            backgroundColor: Colors.white,
            title: Text("Profile",style: appbarHeading,),
            leading: IconButton(
              icon: Icon(Icons.arrow_back,color: Colors.black,),
              onPressed: (){
                Navigator.pop(context);
              },
            ),
            centerTitle: true,
            elevation: 1,
          ),
          body: Container(
            padding: EdgeInsets.only(right: 20,left: 20,top: 20,bottom: 20),
            child: Column(
              children: <Widget>[
                Container(
                  margin: margin,
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: labHeight,
                        child: Row(
                          children: <Widget>[
                            Text(
                                "Name:"
                            ),
                          ],
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Text(
                            "${Common.profileCarUser}"
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: margin,
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: labHeight,
                        child: Row(
                          children: <Widget>[
                            Text(
                                "Email:"
                            ),
                          ],
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Text(
                              "${Common.profileEmail}"
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: margin,
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: labHeight,
                        child: Row(
                          children: <Widget>[
                            Text(
                                "Car No.:"
                            ),
                          ],
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Text(
                              "${Common.profileCarNo}"
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: margin,
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: labHeight,
                        child: Row(
                          children: <Widget>[
                            Text(
                                "Model:"
                            ),
                          ],
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Text(
                              "${Common.profileCarModel}"
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: margin,
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: labHeight,
                        child: Row(
                          children: <Widget>[
                            Text(
                                "Make:"
                            ),
                          ],
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Text(
                              "${Common.profileCarMake}"
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: margin,
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: labHeight,
                        child: Row(
                          children: <Widget>[
                            Text(
                                "Color:"
                            ),
                          ],
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Text(
                              "${Common.profileCarColor}"
                          ),
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    BackButtonInterceptor.remove(myInterceptor);
    // TODO: implement dispose
    super.dispose();
  }
}
